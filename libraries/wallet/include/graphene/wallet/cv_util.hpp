#pragma once

#include <graphene/chain/protocol/pk_update.hpp>
#include <graphene/chain/pk_update_calc.hpp>
#include <graphene/chain/asset_object.hpp>

namespace graphene::chain
{
    template<typename T>
    asset_object get_asset(T& db_api, asset_id_type id)
    {
        auto assets = db_api.get_assets({id});
        FC_ASSERT(!assets.empty(), "asset was not found");

        auto asset_obj = assets.front();
        FC_ASSERT(asset_obj.valid(), "asset was not found");

        return *asset_obj;
    }
}

namespace cv::impl
{

inline graphene::chain::asset core_amount_from_string(
        const graphene::chain::asset_object& aobj,
        const std::string& amount)
{
    return (graphene::chain::asset_id_type() != aobj.id)
                ? aobj.amount_from_string(amount) * aobj.options.core_exchange_rate
                : aobj.amount_from_string(amount);
}

template<typename T>
fc::time_point_sec calc_pk_expiration(
        T& db_api,
        graphene::chain::account_id_type account_id,
        graphene::chain::asset amount,
        int64_t subscription_pack_id)
{
     auto monthly = graphene::chain::get_monthly(db_api, subscription_pack_id);
     auto now = db_api.get_dynamic_global_properties().time;
     auto prev_pk = db_api.get_account_pk(account_id);

     return calc_expiration_by_amount(prev_pk.expiration, now, amount, monthly);
}

} // namespace cv::impl
