/*
 * Copyright (c) 2015 Cryptonomex, Inc., and contributors.
 *
 * The MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <graphene/chain/protocol/block.hpp>
#include <fc/io/raw.hpp>
#include <fc/bitutil.hpp>
#include <algorithm>

namespace graphene::chain
{

digest_type block_header::digest()const
{
   return digest_type::hash(*this);
}

uint32_t block_header::num_from_id(const block_id_type& id)
{
   return fc::endian_reverse_u32(id._hash[0]);
}

block_id_type signed_block_header::id()const
{
   auto tmp = fc::sha224::hash( *this );
   
   // store the block num in the ID, 160 bits is plenty for the hash
   tmp._hash[0] = fc::endian_reverse_u32(block_num()); 
   static_assert( sizeof(tmp._hash[0]) == 4, "should be 4 bytes" );
   block_id_type result;
   memcpy(result._hash, tmp._hash, std::min(sizeof(result), sizeof(tmp)));
   return result;
}

fc::ecc::public_key signed_block_header::signee()const
{
   return fc::ecc::public_key( witness_signature, digest(), true/*enforce canonical*/ );
}

void signed_block_header::sign( const fc::ecc::private_key& signer )
{
   witness_signature = signer.sign_compact( digest() );
}

bool signed_block_header::validate_signee( const fc::ecc::public_key& expected_signee )const
{
   return signee() == expected_signee;
}

static void digest_transactions(const vector<processed_transaction>& tx, vector<digest_type>& digest)
{
   assert(digest.empty());
   digest.reserve(tx.size());

   for (const auto& t : tx)
   {
      digest.push_back(t.merkle_digest());
   }
}

static auto digest_transactions(const vector<processed_transaction>& tx)
{
   vector<digest_type> ret;
   digest_transactions(tx, ret);
   return ret;
}

template <typename T>
inline bool is_odd(T t)
{
   return (t & 1);
}

template <typename T>
inline T closest_even(T t)
{
   return t - (t & 1);
}

auto concat_hashes(const digest_type& lhs, const digest_type& rhs)
{
   if (lhs < rhs)
   {
      return std::make_pair(lhs, rhs);
   }
   else
   {
      return std::make_pair(rhs, lhs);
   }
}

digest_type digest_pair(const digest_type& lhs, const digest_type& rhs)
{
   const auto hash_pair = concat_hashes(lhs, rhs);
   return digest_type::hash(hash_pair);
}

checksum_type signed_block::calculate_merkle_root()const
{
   if (transactions.empty())
   {
      return checksum_type();
   }

   vector<digest_type> ids;
   digest_transactions(transactions, ids);

   auto current_number_of_hashes = ids.size();
   while (current_number_of_hashes > 1)
   {
      auto i_max = closest_even(current_number_of_hashes);
      auto k = 0u;

      for(auto i = 0u; i < i_max; i += 2)
      {
         ids[k++] = digest_pair(ids[i], ids[i + 1]);
      }

      if(is_odd(current_number_of_hashes))
      {
         ids[k++] = ids[i_max];
      }

      current_number_of_hashes = k;
   }

   return checksum_type::hash( ids[0] );
}

merkle_path signed_block::get_merkle_path(size_t tx_index) const
{
   auto path = merkle_path{};

   auto ids = digest_transactions(transactions);
   auto current_number_of_hashes = ids.size();

   while (current_number_of_hashes > 1)
   {
      auto i_max = closest_even(current_number_of_hashes);
      auto k = 0u;

      for (auto i = 0u; i < i_max; i += 2)
      {
         if (tx_index == i)
         {
            tx_index = k;
            path.push_back(ids[i + 1]);
         }
         else if (tx_index == i + 1)
         {
            tx_index = k;
            path.push_back(ids[i]);
         }
         
         ids[k++] = digest_pair(ids[i], ids[i + 1]);
      }

      if (is_odd(current_number_of_hashes))
      {
         if (i_max == tx_index)
         {
            tx_index = k;
         }

         ids[k++] = ids[i_max];
      }

      current_number_of_hashes = k;
   }

   return path;
}

bool verify_merkle(const processed_transaction& tx, const merkle_path& path, const checksum_type& root)
{
   auto digest = tx.merkle_digest();
   for (const auto& p : path)
   {
      digest = digest_pair(digest, p);
   }

   return root == checksum_type::hash(digest);
}

} // namespace graphene::chain
