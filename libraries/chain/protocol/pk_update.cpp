#include <graphene/chain/protocol/pk_update.hpp>

namespace graphene
{
namespace chain
{ 

account_id_type pk_update_operation::fee_payer() const 
{
    return account; 
}

void pk_update_operation::validate() const
{
   FC_ASSERT( fee.amount >= 0 );
   FC_ASSERT( amount.amount >= 0 );

   if (extensions.value.subscription_pack.valid())
   {
        FC_ASSERT(*extensions.value.subscription_pack >= 0);
   }
}

share_type pk_update_operation::calculate_fee(const fee_parameters_type& schedule) const
{
   auto fee = schedule.fee;
   const auto per_kbyte = schedule.price_per_kbyte;
   
   fee += calculate_data_fee(fc::raw::pack_size(key), per_kbyte);
   fee += calculate_data_fee(fc::raw::pack_size(permissions), per_kbyte);

   return fee;
}

}
} // namespace graphene::chain

