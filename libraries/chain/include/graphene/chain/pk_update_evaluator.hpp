#pragma once
#include <graphene/chain/protocol/operations.hpp>
#include <graphene/chain/evaluator.hpp>

namespace graphene { namespace chain {

class pk_update_evaluator : public evaluator<pk_update_evaluator>
{
   public:
      using operation_type = pk_update_operation;

      void_result do_evaluate(const operation_type& o);
      void_result do_apply(const operation_type& o);
};

} } // graphene::chain
