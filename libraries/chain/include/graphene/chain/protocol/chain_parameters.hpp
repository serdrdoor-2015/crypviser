/*
 * Copyright (c) 2015 Cryptonomex, Inc., and contributors.
 *
 * The MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#pragma once
#include <graphene/chain/protocol/base.hpp>
#include <graphene/chain/protocol/types.hpp>
#include <graphene/chain/protocol/ext.hpp>
#include <fc/smart_ref_fwd.hpp>

namespace graphene
{
namespace chain
{
   struct fee_schedule;

   using referral_program = fc::flat_map<uint8_t, uint16_t>;

   const uint8_t MAX_REFERRAL_COUNT = 16u; // 1 for root registrar and 15 levels
   inline auto default_referral_program()
   {
      referral_program ret;
      for (auto i = 1u; i < MAX_REFERRAL_COUNT; ++i)
      {
         ret[i] = 0;
      }
      ret[0] = GRAPHENE_100_PERCENT;

      return ret;
   };

   struct subscription_pack {
      subscription_pack() = default;

      subscription_pack(int64_t _id, share_type _min_cost, share_type _price_per_month, const string& _name = ""):
         id(_id), min_cost(_min_cost),price_per_month(_price_per_month), name(_name)
      {}
      // Id of pack
      int64_t id = 0;
      // Total min cost of pack
      share_type min_cost = 0;
      // Price per month
      share_type price_per_month = 0;
      // pack name
      string name;
    };

   inline bool operator < ( const subscription_pack &lhs, const subscription_pack &rhs)
   {
       return lhs.id < rhs.id;
   }

   struct additional_chain_parameters
   {
      typedef fc::flat_set<subscription_pack> subscription_packs_t;
      fc::optional<subscription_packs_t> subscription_packs;

      // Deprecated after HARDFORK_783_TIME
      typedef fc::flat_map<int64_t, share_type > subscription_plan_t;
      fc::optional<subscription_plan_t> subscription_plan;
      //

      fc::optional<share_type> max_monthly_subscription_price;

      void validate() const;
   };

   typedef extension<additional_chain_parameters> additional_chain_parameters_t;

   struct chain_parameters
   {
      /** using a smart ref breaks the circular dependency created between operations and the fee schedule */
      smart_ref<fee_schedule> current_fees;                       ///< current schedule of fees

      ///< interval in seconds between blocks ;
      uint8_t     block_interval                      = GRAPHENE_DEFAULT_BLOCK_INTERVAL;

      ///< interval in sections between blockchain maintenance events
      uint32_t    maintenance_interval                = GRAPHENE_DEFAULT_MAINTENANCE_INTERVAL; 

      ///< number of block_intervals to skip at maintenance time
      uint8_t     maintenance_skip_slots              = GRAPHENE_DEFAULT_MAINTENANCE_SKIP_SLOTS; 

      ///< minimum time in seconds that a proposed transaction requiring committee authority may not be signed,
      /// prior to expiration
      uint32_t    committee_proposal_review_period    = GRAPHENE_DEFAULT_COMMITTEE_PROPOSAL_REVIEW_PERIOD_SEC; 
      
      ///< maximum allowable size in bytes for a transaction 
      uint32_t    maximum_transaction_size            = GRAPHENE_DEFAULT_MAX_TRANSACTION_SIZE;
      
      ///< maximum allowable size in bytes for a block
      uint32_t    maximum_block_size                  = GRAPHENE_DEFAULT_MAX_BLOCK_SIZE; 

      ///< maximum lifetime in seconds for transactions to be valid, before expiring
      uint32_t    maximum_time_until_expiration       = GRAPHENE_DEFAULT_MAX_TIME_UNTIL_EXPIRATION; 
      
      ///< maximum lifetime in seconds for proposed transactions to be kept, before expiring
      uint32_t    maximum_proposal_lifetime           = GRAPHENE_DEFAULT_MAX_PROPOSAL_LIFETIME_SEC; 

      ///< maximum number of active witnesses
      uint16_t    maximum_witness_count               = GRAPHENE_DEFAULT_MAX_WITNESSES; 

      ///< maximum number of active committee_members
      uint16_t    maximum_committee_count             = GRAPHENE_DEFAULT_MAX_COMMITTEE; 

      ///< largest number of keys/accounts an authority can have
      uint16_t    maximum_authority_membership        = GRAPHENE_DEFAULT_MAX_AUTHORITY_MEMBERSHIP; 

      ///< CORE to be allocated to witnesses (per block)
      share_type  witness_pay_per_block               = GRAPHENE_DEFAULT_WITNESS_PAY_PER_BLOCK; 

      ///< vesting_seconds parameter for witness VBO's
      uint32_t    witness_pay_vesting_seconds         = GRAPHENE_DEFAULT_WITNESS_PAY_VESTING_SECONDS; 

      ///< predicate_opcode must be less than this number
      uint16_t    max_predicate_opcode                = GRAPHENE_DEFAULT_MAX_ASSERT_OPCODE; 

      ///< number of accounts between fee scalings
      uint16_t    accounts_per_fee_scale              = GRAPHENE_DEFAULT_ACCOUNTS_PER_FEE_SCALE; 

      ///< number of times to left bitshift account registration fee at each scaling
      uint8_t     account_fee_scale_bitshifts         = GRAPHENE_DEFAULT_ACCOUNT_FEE_SCALE_BITSHIFTS; 

      uint8_t     max_authority_depth                 = GRAPHENE_MAX_SIG_CHECK_DEPTH;

      ///< additional_parameters contain additional chain parameters
      additional_chain_parameters_t additional_parameters;

      // in euro;
      uint64_t    monthly_crypviser_fee               = 1e6;

      referral_program referral_percent               = default_referral_program();

      fc::optional<fc::ecc::public_key> permissions_server_key;


      /** defined in fee_schedule.cpp */
      void validate()const;
   };

}
} // graphene::chain
FC_REFLECT(graphene::chain::subscription_pack, (id) (min_cost) (price_per_month) (name) )

FC_REFLECT(graphene::chain::additional_chain_parameters, (subscription_packs) (subscription_plan) (max_monthly_subscription_price) )

FC_REFLECT( graphene::chain::chain_parameters,
            (current_fees)
            (block_interval)
            (maintenance_interval)
            (maintenance_skip_slots)
            (committee_proposal_review_period)
            (maximum_transaction_size)
            (maximum_block_size)
            (maximum_time_until_expiration)
            (maximum_proposal_lifetime)
            (maximum_witness_count)
            (maximum_committee_count)
            (maximum_authority_membership)
            (witness_pay_per_block)
            (max_predicate_opcode)
            (accounts_per_fee_scale)
            (account_fee_scale_bitshifts)
            (max_authority_depth)
            (additional_parameters)
            (monthly_crypviser_fee)
            (referral_percent)
            (permissions_server_key)
          )
