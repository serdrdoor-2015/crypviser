#pragma once
#include <graphene/chain/protocol/base.hpp>
#include <graphene/chain/protocol/ext.hpp>
#include <chrono>

namespace cv
{
      using public_key = std::string;
      const int64_t default_subscription_pack_id = 0;
      constexpr auto update_operation_ttl_ms = std::chrono::microseconds{std::chrono::minutes{2}};

      struct expiring_key
      {
            public_key key;
            fc::time_point_sec expiration;
            std::vector<char> permissions;
            int64_t subscription_pack_id = default_subscription_pack_id;
      };

} // namespace cv

namespace graphene
{
namespace chain
{

class database;

/**
* @brief transfer balance from account and split pay between all registrars.
* @param account_id Account which is paying the crypviser fee.
* @param amount Amount to send.
*/
void transfer_to_referrals(database& db, account_id_type account_id, asset amount);


struct pk_update_operation : public base_operation
{
      struct fee_parameters_type
      {
         uint32_t fee = GRAPHENE_BLOCKCHAIN_PRECISION;
         uint32_t price_per_kbyte = 10 * GRAPHENE_BLOCKCHAIN_PRECISION; 
      };

      struct ext
      {
          fc::optional<int64_t> subscription_pack;
      };

      using key_type = cv::public_key;

      asset fee;
      account_id_type account;
      extension <ext> extensions;
      key_type key;
      asset amount;
      std::vector<char> permissions;
      fc::time_point_sec expiration;

      account_id_type fee_payer() const;
      void validate() const;
      share_type calculate_fee(const fee_parameters_type& schedule) const;
};

}
} // namespace graphene::chain

FC_REFLECT(cv::expiring_key, (key)(expiration)(permissions)(subscription_pack_id))
FC_REFLECT(graphene::chain::pk_update_operation::fee_parameters_type, (fee)(price_per_kbyte))
FC_REFLECT(graphene::chain::pk_update_operation::ext, (subscription_pack))
FC_REFLECT(graphene::chain::pk_update_operation, 
            (fee)(account)(extensions)(key)(amount)(permissions)(expiration))
