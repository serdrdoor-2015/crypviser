#pragma once
#include <graphene/chain/protocol/operations.hpp>
#include <graphene/chain/protocol/ext.hpp>
#include <graphene/db/generic_index.hpp>

namespace graphene { namespace chain {
   class database;

   class pk_object : public graphene::db::abstract_object<pk_object>
   {
   public:
      static const uint8_t space_id = protocol_ids;
      static const uint8_t type_id  = pk_object_type;

      using key_type = cv::public_key;

      account_id_type  account;
      key_type key;
      fc::time_point_sec expiration;
      vector<char> permissions;
      int64_t subscription_pack_id = cv::default_subscription_pack_id;
      extensions_type extensions;
      trx_location reference_transaction;
   };

   struct by_account;
   struct by_key;

   using pk_id_index = ordered_unique< tag<by_id>,
      member<object, object_id_type, &object::id>>;

   using pk_account_index = ordered_unique< tag<by_account>,
      member<pk_object, account_id_type, &pk_object::account>>;

    using pk_key_index = ordered_unique<tag<by_key>,
      member<pk_object, pk_object::key_type, &pk_object::key>>;

   using pk_multi_index_type = multi_index_container<
      pk_object,
      indexed_by<pk_id_index, pk_account_index, pk_key_index>
   >;

   using pk_index = generic_index<pk_object, pk_multi_index_type>;
}} // namespace graphene::chain


FC_REFLECT_DERIVED( graphene::chain::pk_object,
                    (graphene::db::object),
                    (account)
                    (key)
                    (expiration)
                    (permissions)
                    (subscription_pack_id)
                    (extensions)
                    (reference_transaction)
                    )

