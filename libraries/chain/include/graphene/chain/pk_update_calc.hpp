#pragma once
#include <graphene/chain/protocol/chain_parameters.hpp>
#include <graphene/chain/database.hpp>
#include <graphene/chain/hardfork.hpp>
#include <chrono>
#include <algorithm>

namespace graphene
{
namespace chain
{

extern const asset_object& get_asset(const database& db, asset_id_type id);

constexpr auto year()
{
    return std::chrono::hours{24} * 365;
}

constexpr auto month()
{
    return std::chrono::hours{24} * 30;
}

template<typename T>
asset convert_to_core(T& db, asset amount)
{
    if (asset_id_type() != amount.asset_id)
    {
        const auto& asset_obj = get_asset(db, amount.asset_id);
        auto core = amount * asset_obj.options.core_exchange_rate;
        return core;
    }

    return amount;
}

template<typename T>
auto get_subscription_pack(T& db, int64_t subscription_pack_id)
{
    const auto global_props = db.get_global_properties();

    if (subscription_pack_id)
    {
        if (db.get_dynamic_global_properties().time < HARDFORK_783_TIME)
        {
            const auto plans = global_props.parameters.additional_parameters.value.subscription_plan;
            FC_ASSERT(plans.valid(), "No subscription plan available");

            const auto it = plans->find(subscription_pack_id);
            FC_ASSERT(it != plans->end(), "Subscription package not found ${p}", ("p", subscription_pack_id));
            return subscription_pack{subscription_pack_id, it->second, it->second};
        }
        else
        {
            const auto plans = global_props.parameters.additional_parameters.value.subscription_packs;
            FC_ASSERT(plans.valid(), "No subscription plan available");

            auto it = std::lower_bound(std::begin(*plans), std::end(*plans), subscription_pack_id,
                [subscription_pack_id](const auto& pack, auto)
                {
                    return pack.id < subscription_pack_id;
                }
            );
            FC_ASSERT( (it != std::end(*plans) && (it->id == subscription_pack_id) ),
                "Subscription package not found ${p}", ("p", subscription_pack_id));
            return *it;
        }
    }
    const auto& monthly_fee = global_props.parameters.monthly_crypviser_fee;
    return subscription_pack{cv::default_subscription_pack_id, monthly_fee, monthly_fee};
}

// Returns monthly price in CVT
template<typename T>
auto get_monthly(T& db, const subscription_pack& pack)
{
    const auto monthly = pack.price_per_month;

    const auto eur = asset_id_type{2};
    auto core = convert_to_core(db, asset{monthly, eur});

    const auto now = db.get_dynamic_global_properties().time;
    if (now >= HARDFORK_783_TIME)
    {
        const auto& global_properties = db.get_global_properties();
        const auto& additional_parameters = global_properties.parameters.additional_parameters.value;

        // max_monthly_price is in CVT!
        const auto max_monthly_price = *additional_parameters.max_monthly_subscription_price;
        core = std::min(core, asset(max_monthly_price));
    }
    return core.amount;
}

template<typename T>
auto get_monthly(T& db, uint64_t subscription_pack_id)
{
    const auto pack = get_subscription_pack(db, subscription_pack_id);
    return get_monthly(db, pack);
}

template<typename T>
auto calc_duration(asset monthly, asset amount, T max_duration)
{
    FC_ASSERT(monthly.asset_id == amount.asset_id, "Assets must be the same type");

    using return_type = std::pair<fc::microseconds, asset>;

    if (monthly.amount > 0)
    {
        using namespace std::chrono;

        auto ms_price = double(monthly.amount.value) / microseconds{ month() }.count();
        auto max_duration_ms = microseconds{max_duration}.count();
        auto max_pay = ms_price * max_duration_ms;

        if (amount.amount > max_pay)
        {
            auto duration = fc::microseconds{max_duration_ms};
            auto pay = asset{max_pay, amount.asset_id};
            return return_type{duration, pay};
        }
        else
        {
            auto duration_ms = amount.amount.value / ms_price;
            auto duration = fc::microseconds{int64_t(duration_ms)};
            return return_type{duration, amount};
        }
    }
    return return_type{fc::microseconds{0}, asset()};
}

template<typename Duration>
auto calc_to_pay(asset monthly, Duration duration)
{
    using namespace std::chrono;

    if (monthly.amount > 0)
    {
        auto ms_price = double(monthly.amount.value) / microseconds{month()}.count(); // ms_price in core asset
        auto duration_ms = microseconds{duration}.count();
        auto pay = ms_price * duration_ms;

        return asset{pay, asset_id_type()};
    }

    return asset();
}

fc::time_point_sec calc_expiration_by_amount(fc::time_point_sec last,
                                             fc::time_point_sec current,
                                             asset amount,
                                             share_type monthly_fee);

inline int64_t get_subscription_pack_id(const fc::time_point_sec& now, const pk_update_operation& op)
{
    if (now < HARDFORK_346_TIME ||
        !op.extensions.value.subscription_pack.valid())
    {
        return cv::default_subscription_pack_id;
    }

    return *op.extensions.value.subscription_pack;
}

}
} // namespace graphene::chain
