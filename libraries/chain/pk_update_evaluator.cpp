#include <graphene/chain/pk_update_evaluator.hpp>
#include <graphene/chain/pk_object.hpp>
#include <graphene/chain/pk_update_calc.hpp>
#include <graphene/chain/database.hpp>

#include <boost/core/ignore_unused.hpp>
#include <chrono>

namespace graphene
{
namespace chain
{

fc::time_point_sec calc_expiration_by_amount(fc::time_point_sec last,
                                             fc::time_point_sec current,
                                             asset amount,
                                             share_type monthly_fee)
{
    using namespace std::chrono;

    auto from = std::max(current, last);
    auto max_duration = microseconds{year()};

    if (current < from)
    {
        const auto left_until_expiration = microseconds{(from - current).count()};
        max_duration -= left_until_expiration;
    }

    const auto calculated = calc_duration(monthly_fee, amount, max_duration);
    const auto duration = std::get<0>(calculated);
    const auto to_pay = std::get<1>(calculated);
    boost::ignore_unused(to_pay);
    
    return from + duration;
}

const asset_object& get_asset(const database& db, asset_id_type id)
{
    return db.get(id);
}

void check_core_balance(database& db, account_id_type account, asset amount)
{
    assert(asset_id_type() == amount.asset_id);

    auto balance = db.get_balance(account, asset_id_type()).amount;
    bool insufficient_balance = balance >= amount.amount;

    FC_ASSERT( insufficient_balance,
            "Insufficient Balance: ${balance}, unable to spend '${amount}' from account '${a}''", 
            ("a", db.get(account).name)
            ("amount", db.to_pretty_string(amount))
            ("balance", db.to_pretty_string(balance)));
}

void check_balance(database& db, account_id_type account, asset amount)
{
    auto core = convert_to_core(db, amount);
    check_core_balance(db, account, core);
}

bool non_registrar(account_id_type account)
{
    // we should ignore all special-purpose accounts
    // the last of which to the day is GRAPHENE_PROXY_TO_SELF_ACCOUNT
    // with instance number 5
    return account.instance.value <= GRAPHENE_PROXY_TO_SELF_ACCOUNT.instance.value;
}

bool real_registrar(account_id_type account)
{
    return !non_registrar(account);
}

auto get_registrars(database& db, account_id_type immediate_registrar)
{
    std::vector<account_id_type> ret;

    for (auto r = immediate_registrar; real_registrar(r);)
    {
        ret.push_back(r);
        const auto& account = db.get(r);
        r = account.registrar;
    }

    return ret;
}

share_type cut_pay(share_type a, uint16_t p)
{
    assert(p <= GRAPHENE_100_PERCENT); 

    if( a == 0 || p == 0 )
    {
        return 0;
    }

    if( p == GRAPHENE_100_PERCENT )
    {
        return a;
    }

    auto r = fc::uint128{a.value};
    r *= p;
    r /= GRAPHENE_100_PERCENT;
    return r.to_uint64();
}

bool check_subscription(database& db, account_id_type account)
{
    const auto& keys = db.get_index_type<pk_index>().indices().get<by_account>();
    auto it = keys.find(account);

    if (it != keys.end())
    {
        const auto expiration = it->expiration;
        const auto now = db.head_block_time();

        return expiration > now;
    }

    return false;
}

uint64_t referral_program_cap(const referral_program& prog)
{
    uint64_t ret = 0;
    for (const auto& p: prog)
    {
        ret += p.second;
    }

    return ret;
}

void split_pay_between_registrars(database& db, const std::vector<account_id_type>& registrars, asset amount)
{
    assert(!registrars.empty());
    // in (registrars): 
    // the first one - is the immediate registrar (level 1)
    // the second - level 2 registrar
    // the third - level 3 registrar
    // ...
    // the last one - root registrar

    const auto& referral_percent = db.get_global_properties().parameters.referral_percent;
    auto referral_count = std::min<size_t>(MAX_REFERRAL_COUNT, registrars.size());
    // in (referrals):
    // the first one is the root registrar
    // the second - level 1 registrar
    // the third  - level 3 registrar
    // ...
    // the last one - level 15 registrar

    auto remainder = amount;
    auto total_ref_percent = referral_program_cap(referral_percent);
    auto remainder_percent = total_ref_percent;

    auto ref_percent = [&](auto idx)
    {
        auto f = referral_percent.find(idx + 1);
        return f != referral_percent.end() ? f->second : 0;
    };

    auto ref_account = [&](auto idx)
    {
        return registrars[idx];
    };

    auto root_account = [&]
    {
        return registrars.back();
    };

    auto calc_pay = [&](size_t idx)
    {
        auto percent = ref_percent(idx); 
        auto pay_amount = cut_pay(amount.amount, percent);
        return asset{pay_amount, amount.asset_id};
    };

    auto pay_referral = [&](size_t idx)
    {
        auto account = ref_account(idx);
        if (check_subscription(db, account))
        {
            auto pay = calc_pay(idx);
            remainder -= pay;
            remainder_percent -= ref_percent(idx);
            db.adjust_balance(account, pay);
        }
    };

    auto pay_root_referral = [&]
    {
        auto pay = remainder;
        if (total_ref_percent < GRAPHENE_100_PERCENT)
        {
            auto pay_amount = cut_pay(amount.amount, remainder_percent);
            pay = asset{pay_amount, amount.asset_id};
            pay = std::min(remainder, pay);
        }

        db.adjust_balance(root_account(), pay);
        remainder -= pay;
    };

    auto update_reserve_fund = [&]
    {
        if (remainder.amount > 0)
        {
            asset_id_type core;
            db.modify(db.get(core).dynamic_asset_data_id(db), [&](asset_dynamic_data_object& d) 
            {
                d.current_supply -= remainder.amount;
            });
        }
    };

    for (auto i = 0u; i < referral_count - 1; ++i) // everybody except root referral
    {
        pay_referral(i);
    }

    pay_root_referral();
    update_reserve_fund();
}


void transfer_to_referrals(database& db, account_id_type account_id, asset amount)
{
    const auto& account = db.get(account_id);
    auto registrars = get_registrars(db, account.registrar);

    if (registrars.empty())
    {
        registrars.push_back(account_id);
    }

    db.adjust_balance(account_id, -amount);
    split_pay_between_registrars(db, registrars, amount);
}

auto has_public_key(database& db, account_id_type account)
{
    const auto& keys = db.get_index_type<pk_index>().indices().get<by_account>();
    auto it = keys.find(account);
    return std::make_pair(it != keys.end(), it);
}

auto get_monthly(const database& db, const pk_update_evaluator::operation_type& op)
{
    const auto now = db.head_block_time();
    const auto subscription_pack_id = get_subscription_pack_id(now, op);

    return get_monthly(db, subscription_pack_id);
}

using namespace std::chrono;

auto create_public_key_object(database& db, const pk_update_evaluator::operation_type& op)
{
    const auto monthly = get_monthly(db, op);
    const auto now = db.head_block_time();
    const auto pay = calc_to_pay(monthly, microseconds{(op.expiration - now).count()});
    const auto subscription_pack_id = get_subscription_pack_id(now, op);

    db.create<pk_object>([&](auto& object)
    {
        object.account = op.account;
        object.key = op.key;
        object.expiration = op.expiration;
        object.permissions = op.permissions;
        object.subscription_pack_id = subscription_pack_id;
        object.reference_transaction = db.locate_current_transaction();
    });

    return pay;
}

template <typename T>
auto update_existing_public_key(database& db, const pk_update_evaluator::operation_type& op,
                                T object_it)
{
    const auto monthly = get_monthly(db, op);
    const auto now = db.head_block_time();
    auto pay = asset{};
    const auto subscription_pack_id = get_subscription_pack_id(now, op);

    db.modify<pk_object>(*object_it, [&](pk_object& object)
    {
        object.key = op.key;
        object.permissions = op.permissions;
        object.subscription_pack_id = subscription_pack_id;
        object.reference_transaction = db.locate_current_transaction();

        const auto prev_expiration = (now >= object.expiration) ? now : object.expiration;
        pay = calc_to_pay(monthly, microseconds{(op.expiration - prev_expiration).count()});
        object.expiration = op.expiration;
    });

    return pay;
}

void_result pk_update_evaluator::do_evaluate(const operation_type& op)
{ 
    try 
    {
        // do fast checks first
        FC_ASSERT(!op.key.empty(), "Public key can't be empty");

        const auto now = db().head_block_time();

        if (now < HARDFORK_346_TIME)
        {
            FC_ASSERT(!op.extensions.value.subscription_pack.valid());
        }

        FC_ASSERT(op.expiration > now, "Expiration must be in future");

        const auto max_expiration = now + fc::microseconds{microseconds{year()}.count()};
        FC_ASSERT(op.expiration <= max_expiration, "Maximum duration limit exceeded");

        const auto subscription_pack_id = get_subscription_pack_id(now, op);
        const auto pack = get_subscription_pack(db(), subscription_pack_id);

        if (now >= HARDFORK_783_TIME)
        {
            auto min_cost_in_core = convert_to_core(db(), asset{pack.min_cost, asset_id_type{2}});
            FC_ASSERT(min_cost_in_core <= op.amount,
            "Insufficient amount. User pays ${amount}, but required at least ${min_cost} core assets",
            ("amount", op.amount)("min_cost", min_cost_in_core));
        }

        const auto monthly = get_monthly(db(), pack);

        // do slow (indexed) checks here
        const auto& index = db().get_index_type<pk_index>().indices().get<by_key>();
        auto entry = index.find(op.key);
        
        FC_ASSERT(entry == index.end(),
            "Public key must be unique: ${key} belongs to ${account}",
            ("key", entry->key)("account", entry->account));

        auto pubkey_iter = has_public_key(db(), op.account);
        auto exists = std::get<0>(pubkey_iter);
        auto it = std::get<1>(pubkey_iter);

        auto prev_expiration = exists ? it->expiration : now;

        if (now >= HARDFORK_346_TIME)
        {
            if (prev_expiration > now)
            {
                FC_ASSERT(subscription_pack_id == it->subscription_pack_id, 
                          "User can't change existing subscription");
            }
            else
            {
                prev_expiration = now;
            }

            const auto min_duration_ms = microseconds{month()} - cv::update_operation_ttl_ms;
            const auto min_expiration = prev_expiration + fc::microseconds{min_duration_ms.count()};

            FC_ASSERT(op.expiration >= min_expiration,
                      "Update operation must extend subscription (month minimum)");
        }

        const auto duration = microseconds{(op.expiration - prev_expiration).count()};
        const auto to_pay = calc_to_pay(monthly, duration);
        const auto core = convert_to_core(db(), op.amount);

        FC_ASSERT(to_pay <= core, "Insufficient amount. User pays ${a}, but required ${p}",
                                  ("a", core)("p", to_pay));

        check_core_balance(db(), op.account, to_pay);
        
        return void_result();

    }  
    FC_CAPTURE_AND_RETHROW( (op) ) 
}

void_result pk_update_evaluator::do_apply(const operation_type& op)
{ 
    try 
    {
        auto pay = asset{};

        auto pubkey_iter = has_public_key(db(), op.account);
        auto exists = std::get<0>(pubkey_iter);
        auto it = std::get<1>(pubkey_iter);

        if (exists)
        {
            pay = update_existing_public_key(db(), op, it);
        }
        else
        {
            pay = create_public_key_object(db(), op);
        }

        transfer_to_referrals(db(), op.account, pay);

        return void_result();

    }  
    FC_CAPTURE_AND_RETHROW( (op) ) 
}

} 
} // namespace graphene::chain
