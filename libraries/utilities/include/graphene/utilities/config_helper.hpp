#pragma once

#include <fc/filesystem.hpp>

#include <string>

namespace graphene { namespace config_helper {

void save_extra_option(const std::string& full_path, const std::string& tag, const std::string& value);
uint64_t get_salt(const fc::path &data_dir);

} } //graphene::config_helper
