#include <graphene/utilities/config_helper.hpp>

#include <regex>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <fc/exception/exception.hpp>
#include <fc/crypto/rand.hpp>

namespace bpo = boost::program_options;

namespace graphene { namespace config_helper {

void save_extra_option(const std::string& full_path, const std::string& tag, const std::string& value)
{
    //backup current config file
    std::string bak_file = full_path + ".bak";
    try
    {
        boost::filesystem::copy_file(full_path, bak_file,
            boost::filesystem::copy_option::overwrite_if_exists);
    } 
    catch (const boost::filesystem::filesystem_error& e)
    {
        FC_THROW("Cannot create backup copy of configuration file: ${e}",
                 ("e", e.what()));
    }

    std::string error_message;
    //rewrite config with new tag=value
    try
    {
        std::ifstream in_cfg(bak_file);
        std::ofstream out_cfg(full_path);

        // write option at the beginning
        out_cfg << tag << "=" << value << "\n";

        std::regex tag_regex(tag);
        std::smatch pieces;

        for (std::string line; std::getline(in_cfg, line);)
        {
            // skip option if exists
            if (std::regex_search(line, pieces, tag_regex)) 
            {
                continue;
            }

            out_cfg << line << "\n";
        }

        in_cfg.close();
        out_cfg.close();

        bpo::options_description cfg_options("Verify");
        cfg_options.add_options()(tag.c_str(), bpo::value<std::string>(), "Verify option.");
        
        bpo::variables_map vm;
        bpo::store(bpo::parse_config_file<char>(full_path.c_str(), cfg_options, true), vm);

        FC_ASSERT(vm.count(tag) > 0, "Failed to store option '${t}'", ("t", tag));
        FC_ASSERT(value == vm[tag].as<std::string>(), 
                  "Failed to verify stored option '${t}'", ("t", tag));

        return;
    }
    catch (const fc::exception &e)
    {
        error_message = e.to_string();
    }
    catch (const std::exception &e)
    {
        error_message = e.what();
    }

    boost::filesystem::copy_file(bak_file, full_path,
        boost::filesystem::copy_option::overwrite_if_exists);

    FC_THROW("Failed to save extra option in configuration file: ${e}",
             ("e", error_message));
}

uint64_t get_salt(const fc::path &data_dir)
{
    uint64_t salt(0);
    fc::path salt_path = data_dir / ".salt";

    if (!fc::exists(salt_path))
    {
        fc::rand_bytes((char*) &salt, sizeof(salt));                  
        std::ofstream out_salt(salt_path.preferred_string());
        out_salt << std::hex << salt;
    }
    else
    {
        std::ifstream in_salt(salt_path.preferred_string());
        in_salt >> std::hex >> salt;
    }

    return salt;         
};

} } //graphene::config_helper
