#pragma once

#include <string>
#include <vector>

namespace graphene::app::cli {

std::string get_passphrase();
std::pair<std::string, bool> get_password_interactively();
std::string get_private_key_interactively();

/**
 * Asks an user to input password and private key. Then encrypt private_key using secret that
 * was generated via KDF + input password + salt.
 * @params salt
 * @return pair of public_key and encripted private_key
 * can throw fc::aes_exception  and fc::exception
 */
std::pair<std::string, std::string> encrypt_private_key_interactively(uint64_t salt);

/**
 * Asks user to input password. Then decrypt private_key using secret that
 * was generated via KDF + input password + salt.
 * @params salt
 * @params wif_keys - list of pairs {public key, encrypted private key}
 * @return list of pairs {public key, decrypted private_key}
  */
std::vector<std::string> decrypt_private_keys_interactively(uint64_t salt, const std::vector<std::string>& wif_keys);

} // namespace graphene::app::cli
