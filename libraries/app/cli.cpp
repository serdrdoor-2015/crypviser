#include <unistd.h>
#include <termios.h>

#include <cerrno>
#include <cstring>

#include <string>
#include <iostream>

#include <fc/exception/exception.hpp>
#include <fc/crypto/aes.hpp>
#include <fc/crypto/kdf.hpp>
#include <fc/crypto/base58.hpp>

#include <graphene/chain/protocol/types.hpp>
#include <graphene/utilities/key_conversion.hpp>
#include <graphene/app/plugin.hpp>

namespace graphene::app::cli {

std::string get_passphrase()
{
    termios attr_old;

    // Turn echoing off and fail if we cannot
    if (::tcgetattr(STDIN_FILENO, &attr_old) != 0)
    {
        FC_THROW("tcgetattr failed with error '${e}'.", ("e", std::strerror(errno)));
    }

    termios attr_new = attr_old;
    attr_new.c_lflag &= ~(ECHO);

    if (::tcsetattr(STDIN_FILENO, TCSAFLUSH, &attr_new) != 0)
    {
        FC_THROW("tcsetattr failed with error '${e}'.", ("e", std::strerror(errno)));
    }

    // Read the password
    std::string line;
    std::getline(std::cin, line);

    // Restore terminal
    ::tcsetattr(STDIN_FILENO, TCSAFLUSH, &attr_old);
    return line;
}

std::pair<std::string, bool> get_password_interactively()
{
    std::cout << "Enter password: ";
    const std::string password = get_passphrase();
    std::cout << "\nConfirm password: ";
    const std::string retyped_password = get_passphrase();
    std::cout << '\n';

    if (password != retyped_password)
    {
        std::cout << "Sorry, passwords do not match\n";
        return std::make_pair(std::string(), false);
    }

    return std::make_pair(password, true);
}

std::string get_private_key_interactively()
{
    std::cout << "Enter private key: ";
    const std::string private_key = get_passphrase();
    std::cout << '\n';
    return private_key;
}

using cipher_t = std::vector<char> (*)(const fc::sha512&, const std::vector<char>&);

std::pair<std::string, std::string> encrypt_private_key_interactively(uint64_t salt)
{
    std::cout << "Private key encryption.\n";

    std::string password;
    bool success;
    std::tie(password, success) = get_password_interactively();

    if ( !success )
    {
        FC_THROW("cannot match password");
    }

    const std::string private_key_data = get_private_key_interactively();

    const fc::optional<fc::ecc::private_key> private_key = graphene::utilities::wif_to_key(private_key_data);
    if ( !private_key )
    {
        FC_THROW("bad private key");
    }
    const auto secret = fc::derive_aes_key(password, salt);
    const std::vector<char> to_encrypt(private_key_data.begin(), private_key_data.end());
    
    const auto encrypted_private_key = fc::aes_encrypt(secret, to_encrypt);

    const auto encrypted = fc::to_base58(encrypted_private_key);

    const std::string public_key_data(chain::public_key_type( (*private_key).get_public_key() ));
    return std::make_pair(public_key_data, encrypted);
}
    
std::vector<std::string> decrypt_private_keys_interactively(uint64_t salt, const std::vector<std::string>& wif_keys)
{
    std::vector<std::string> decrypted_wif_keys;

    // check pair [public, private] key
    for (const auto& wif_key: wif_keys)
    {
        auto key_id_to_wif_pair = graphene::app::dejsonify<std::pair<chain::public_key_type, std::string> >(wif_key);

        std::string private_key_data = key_id_to_wif_pair.second;
        while (true)
        {
            auto private_key = graphene::utilities::wif_to_key(private_key_data);

            if ( private_key )
            {
                if ( chain::public_key_type((*private_key).get_public_key()) != key_id_to_wif_pair.first )
                {
                    // TODO: should be added to log?
                    std::cout << "cannot match public key from pivate\n";
                }
                // in any case add wif_key to the decrypted list
                key_id_to_wif_pair.second = private_key_data;

                decrypted_wif_keys.push_back(fc::json::to_string(key_id_to_wif_pair));
                break;
            }
            else // cannot get private key from wif. So try to decrypt private key_data and try again
            {
                std::cout << "Enter password: ";
                const std::string password = get_passphrase();
                std::cout << '\n';
                try {
                    const std::vector<char> to_decrypt(fc::from_base58(key_id_to_wif_pair.second));

                    const auto secret = fc::derive_aes_key(password, salt);

                    const auto decrypted = fc::aes_decrypt(secret, to_decrypt);

                    private_key_data.assign(decrypted.begin(), decrypted.end());
                }
                catch (const fc::aes_exception& )
                {
                    std::cout << "Incorrect password. Please try again.\n";
                }
            }
        }
    }

    return decrypted_wif_keys;
}

} // namespace graphene::app::cli
