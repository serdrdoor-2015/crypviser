import pytest
import time
import subprocess
import json
import random
import string

CVCOIN_SYM = "CVT"

ACTOR   = "test_account"
WIF_KEY = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"

def generate_random_string(len):
    gen = lambda n: ''.join([random.choice(string.lowercase) for i in xrange(n)])
    return gen(len)

def generate_random_name():
    return generate_random_string(10)

@pytest.fixture
def client():
    class Client(object):
        def __init__(self):
            self.driver_cmd = "./driver -i ws://localhost:11010".split()


    	def add_initial_peer(self, val):
            self.__exec("add_initial_peer", val)

    	def set_chain_id(self, val):
            self.__exec("set_chain_id", val)

    	def get_remote_chain_id(self):
            output = self.__exec("get_remote_chain_id")
            return Client.__get_string(output)
            
        def get_key(self, val):
            output = self.__exec("get_key", val)
            return Client.__get_string(output)

        def get_expiration(self, val):
            output = self.__exec("get_expiration", val)
            return Client.__get_string(output)

        def update_pk(self, account_name, key, amount, asset_symbol, wif_key):
            self.__exec("update_pk", account_name, key, amount, asset_symbol, wif_key)

        def register_account(self, name, key, registrar, wif_key):
            self.__exec("register_account", name, key, registrar, wif_key)

        def transfer(self, from_account, to_account, amount, wif_key):
            self.__exec("transfer", from_account, to_account, amount, wif_key)

        def get_number_head_block(self):
            output = self.__exec("get_number_head_block")
            return Client.__get_int(output)

        def get_last_error(self):
            output = self.__exec("get_last_error")

            data = Client.__get_json(output)
            return data["code"]

        def __driver(self):
            return subprocess.Popen(self.driver_cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE)

        def __exec(self, *args):
            p = self.__driver()
            p.stdin.write(" ".join(args) + "\n")

            time.sleep(0.05)

            return p.communicate()[0]

        def wait_blocks(self, num_blocks):
            block = self.get_number_head_block()
            while self.get_number_head_block() - block < num_blocks:
                assert 0 == self.get_last_error()
                time.sleep(1)
        
        @staticmethod
        def __get_string(output):
            output = output.splitlines()[-2]
            return output[1:-1]

        @staticmethod
        def __get_int(output):
            output = output.splitlines()[-2]
            return int(output)

        @staticmethod
        def __get_json(output):
            lines = output.splitlines()[1:-1]
            return json.loads(" ".join(lines))

    client = Client()

    global ACTOR
    ACTOR = generate_random_name()
    account_key = "CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV"

    client.register_account(ACTOR, account_key, "nathan", WIF_KEY)
    client.transfer("nathan", ACTOR, "10000", WIF_KEY)

    return client


@pytest.mark.parametrize('amount', ["15", "25", "35"])
def test_update_pk(client, amount):
    new_key = generate_random_string(10)
    
    client.update_pk(ACTOR, new_key, amount, CVCOIN_SYM, WIF_KEY)
    client.wait_blocks(1)

    key = client.get_key(ACTOR)
    assert key == new_key

    ex = client.get_expiration(ACTOR)
    assert ex != ""


def test_get_number_head_block(client):
    head = client.get_number_head_block()
    assert head > 0


def test_get_last_error(client):
    error = client.get_last_error()
    assert 0 == error
    