#pragma once

#include <graphene/chain/protocol/block.hpp>
#include <graphene/chain/protocol/types.hpp>
#include <graphene/wallet/brain_key.hpp>
#include <fc/api.hpp>

namespace light {

using graphene::chain::chain_id_type;
using graphene::chain::block_header;
using graphene::chain::signed_transaction;
using graphene::chain::public_key_type;
using graphene::wallet::brain_key_info;

enum error_code : int {
    SUCCESS = 0,
    CONNECTION_ERROR,
    BROADCAST_ERROR,
    INVALID_PARAM,
    CREATE_TRANSACTION_ERROR,
    ACCOUNT_NOT_REGISTERED,
    PUBLIC_KEY_NOT_REGISTERED,
    VERIFICATION_FAILURE,
    INTERNAL_ERROR,
    EXCEPTION_OCCURED
};

std::string strerror(error_code code);

struct error
{
    int code = error_code::SUCCESS;
    std::string message;

    operator bool() const
    {
        return code != error_code::SUCCESS;
    }
};

struct balance
{
    int64_t satoshis;
    int64_t precision;
};

struct key_info
{
    uint32_t block_num;
    uint32_t trx_num;
    std::string witness_name;
    std::string timestamp;
};

class client_protocol
{
public:
    client_protocol(const client_protocol&) = delete;
    client_protocol& operator=(const client_protocol&) = delete;

    client_protocol();
    ~client_protocol();

    //public api
public:
    /** Returns version info such as core protocol version, git revision, build date, etc.
     * 
     * @returns version info
     */
    fc::variant version() const;

    /** Suggests a safe brain key to use for creating your account.
     * @returns a suggested brain_key
     */
    brain_key_info suggest_brain_key();

    /** Adds initial blockchain peer, this peer will be later used to send requests to
     *  and also to automatically discover new peers, so we never use the same address
     *  for network communication
     *
     * @param address the peer address
     */
    void add_initial_peer(const std::string& address);

    /** Set initial blockchain peers, these peers will be later used to send requests to
     *  and also to automatically discover new peers. This call will reset previously 
     *  defined initial peers
     *
     * @param peers the vector of peers addresses
     */
    void set_initial_peers(const std::vector<std::string>& peers);

    /**
     * Initiate peer discovery process, will use the peer addresses added in add_initial_peer
     */
    void discover_peers();


    /** Returns the Crypviser public key belonging to the account
     *
     * @param account_name the name of the account
     * @returns the Crypviser public key used for key exchange and traffic encryption
     */
    std::string get_key(const std::string& account_name);

    /** Returns the Crypviser public key expiration datetime
     *
     * @param account_name the name of the account
     * @returns the Crypviser public key expiration datetime
     */
    std::string get_expiration(const std::string& account_name);

    /** Returns the Crypviser public key belonging to the account, 
     * does it in the most unsecure way by issuing a single RPC request.
     * It's not advised to use the function, we keep it just for testing purposes.
     *
     * @param account_name the name of the account
     * @returns the Crypviser public key 
     */
    std::string get_key_unsecure(const std::string& account_name);

    /** Returns the Crypviser public key expiration datetime.
     * Does it in the most unsecure way by issuing a single RPC request.
     * It's not advised to use the function, we keep it just for testing purposes.
     *
     * @param account_name the name of the account
     * @returns the Crypviser public key expiration datetime
     */
    std::string get_expiration_unsecure(const std::string& account_name);

    /** Updates the Crypviser public key associated with the given account
     *
     * @param account_name the name of the account
     * @param key the Crypviser public key, the one used for key exchange and further traffic encryption
     * @param amount the amount of CVT (or other token) the user is willing to pay for the service
     * @param asset_symbol the asset the user is willing to pay its fees in (typically it's CVT)
     * @param wif_key the blockchain private key used to sign a transaction, should belong to account_name
     */
    void update_pk(const std::string& account_name, const std::string& key,
                const std::string& amount, const std::string& asset_symbol,
                const std::string& wif_key);

    /** Updates the Crypviser public key associated with the given account
     *
     * @param account_name the name of the account
     * @param key the Crypviser public key, the one used for key exchange and further traffic encryption
     * @param subscription_pack_id the id of subscription package
     * @param amount the amount of CVT (or other token) the user is willing to pay for the service
     * @param asset_symbol the asset the user is willing to pay its fees in (typically it's CVT)
     * @param wif_key the blockchain private key used to sign a transaction, should belong to account_name
     */
    void subscribe_account(const std::string& account_name, const std::string& key,
                const long subscription_pack_id, const std::string& amount,
                const std::string& asset_symbol, const std::string& wif_key);

    /** Registers a new account on the blockchain
     *
     * @param account_name the name of the account
     * @param account_public_key the public key associated with the account
     * @param registrar the registrar account name, the one which pays for the registration
     * @param registrar_private_key the private key belonging to the registrar, used to sign a transaction
     */
    void register_account(const std::string& account_name, public_key_type account_public_key,
            const std::string& registrar, const std::string& registrar_private_key);

    /** Registers a new account using a registrar server
     *
     * @param registrar_server_address the registrar server address
     * @param account_name the name of the account
     * @param account_public_key the public key associated with the account
     */
    void register_account_with_registrar_server(
            const std::string& registrar_server_address,
            const std::string& account_name,
            const std::string& account_public_key);

    /** Transfers the specified amount of CVT from one account to another.
     *
     * @param from the account name to transfer CVT from
     * @param to the account name to transfer CVT to
     * @param amount the amount of CVT to transfer
     * @param wif_key the private key of the account which want to transfer its CVT, used to sign a transaction
     */
    void transfer(std::string from, std::string to, std::string amount, std::string wif_key);

    /** Same as \c add_initial_peer()
     *
     * @param address the initial peer address
     */
    void set_remote_endpoint_from_address(const std::string& address);

    /** Returns the last error code and its description. See \c error_code
     * 
     * @returns the last error code and its description
     */
    error get_last_error() const { return last_error; };

    /** Retrieves the block headers from the blockchain
     *
     * @first_block the first block number to synchronize
     * @last_block the last block number to synchronize
     */
    void synchronize_block_headers(std::uint32_t first_block, std::uint32_t last_block);


    /** Retrieves all the block headers produced by the blockchain so far
     *
     */
    void synchronize_all_block_headers();

    /** Returns the head block number, meaning the most recent block produced by the blockchain
     *
     * @returns the head block number
     */
    uint32_t get_number_head_block();

    /** Loads the block headers from a buffer, 
     * saves the user from synchronizing the blocks all over thus incurring higher network overhead
     *
     * @param buffer the buffer pointer
     * @param n the buffer size
     */
    void load_from_buffer(const char* buffer, size_t n);

    /** Serializes the block headers into a binary buffer
     *
     * @param buffer the buffer pointer
     * @param n the buffer size
     * @returns the required buffer size when passed null pointer or the number of bytes used in the buffer
     */
    size_t save_to_buffer(char* buffer, size_t n);

    /** Sets the level of response data validation.
     *
     * @param n validation level
     */
    void set_validation_level(std::uint16_t n);

    /** Set timeout for operations
     *
     * @param timeout_in_seconds the timeout value in seconds
     */
    void set_operation_timeout(uint64_t timeout_in_seconds);

    /** Set chain-id, if empty, identification and verification will not be performed
     *
     * @param chain_id the id of chain to connect to
     */
    void set_chain_id(chain_id_type chain_id);

    /** Returns the CVT balance of an account in satoshis, includes token presicion such as
     *  (balance.satoshis / balance.precision) is the amount of CVT on the balance
     *  (balance.satoshis % balance.precision) is the fractional part of the balance
     *
     * @param username the account name
     * @returns the account balance
     */
    balance get_balance(const std::string& username);

    /** Set of potential network addresses. 
     *  New peers discovered by discover_peers or add_initial_peer
     * 
     * @returns the array of potential discovered peers
     */
    std::vector<std::string> get_potential_addresses();

    /** Peer falls into blacklist if returns fake data different from other peers.
     *  
     * @returns the array of blacklisted peers
     */
    std::vector<std::string> get_blacklisted_addresses();

    /** A peer falls into inaccessible list with every unsuccessful connection retry.
     *  After 1-hour timeout the peer is deleted from inaccessible list
     *  
     * @returns the array of inaccessible peers
     */
    std::vector<std::string> get_inaccessible_peers();

    /** A peer goes falls unsynchronized list if it is suspected to be unsynchronized.
     *  After 1-hour timeout the peer is deleted from unsynchronized list
     *
     * @returns the array of unsynchronized peers
     */
    std::vector<std::string> get_unsynchronized_peers();

    /** Get chain-id of connected blockchain
     *
     * @returns the id of connected blockchain
     */
    chain_id_type get_chain_id();


    /** Return the global properties of the blockchain
     *
     * @returns blockchain global properties
     */
    std::string get_global_properties();

    /** Return total supply of CVT
     *
     * @returns total supply of CVT
     */
    balance get_total_supply();

    /** Return curent supply of CVT
     *
     * @returns current supply of CVT
     */
    balance get_current_supply();

    /** Returns additional information about the public key and where in the blockchain it resides, 
     * like block number, transaction number,
     * block signing witness name, block timestapm
     *
     * @returns public key information
     */
    key_info get_key_info(const std::string& account);

    std::string get_account_history(std::string account_name);
    
private:
    class client;

    struct key_data
    {
        cv::expiring_key key;
        uint32_t block_num;
        uint32_t trx_num;
    };

    using account_id_type = graphene::chain::account_id_type;

    void sign_transaction(signed_transaction tx, const std::string& wif_key);

    key_data get_account_pk(const std::string& account_name);
    key_data get_account_pk_unsecure(const std::string& account_name);

    account_id_type get_verified_account_id(const std::string& account_name);
    fc::optional<key_data> get_verified_pk(account_id_type account_id);

    std::vector<char> encrypt_permissions(const std::string& perm);

    template<typename Callable>
    auto safe_execute(Callable f);

    void signal_error(error_code code);
    void signal_error(error_code code, const std::string& message);
    void signal_success();

private:
    std::unique_ptr<client> pclient;
    error last_error;
};

}//light

FC_REFLECT( light::error, (code)(message) )
FC_REFLECT( light::balance, (satoshis)(precision) )
FC_REFLECT( light::key_info, (block_num)(trx_num)(witness_name)(timestamp))

FC_API( light::client_protocol,
        (set_initial_peers)
        (add_initial_peer)
        (discover_peers)
        (set_operation_timeout)
        (get_potential_addresses)
        (get_blacklisted_addresses)
        (get_inaccessible_peers)
        (get_unsynchronized_peers)
        (set_validation_level)
        (get_key)
        (get_expiration)
        (update_pk)
        (subscribe_account)
        (get_last_error)
        (synchronize_block_headers)
        (synchronize_all_block_headers)
        (get_number_head_block)
        (get_key_unsecure)
        (get_expiration_unsecure)
        (register_account)
        (register_account_with_registrar_server)
        (transfer)
        (suggest_brain_key)
        (version)
        (set_chain_id)
        (get_chain_id)
        (get_balance)
        (get_global_properties)
        (get_total_supply)
        (get_current_supply)
        (get_key_info)
        (get_account_history)
      )
