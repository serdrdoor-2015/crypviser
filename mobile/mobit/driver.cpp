#include <iostream>
#include <boost/program_options.hpp>

#include <mobit/mobit.hpp>

#include <fc/rpc/cli.hpp>
#include <fc/rpc/websocket_api.hpp>
#include <fc/rpc/http_api.hpp>


int main(int argc, char** argv)
{
    namespace po = boost::program_options;

    try
    {
        po::options_description opts;
        opts.add_options()
            ("help,h", "Print this help message and exit.")
            ("rpc-endpoint,r", po::value<std::string>()->implicit_value("127.0.0.1:9171"), 
             "Endpoint for driver websocket RPC to listen on")
            ("rpc-http-endpoint,H", po::value<std::string>()->implicit_value("127.0.0.1:9172"), 
             "Endpoint for driver HTTP RPC to listen on")
            ("initial-peer,i", po::value<std::string>()->implicit_value("ws://localhost:11010"), 
             "Initial peer of blockchain");

        po::variables_map options;
        po::store(po::parse_command_line(argc, argv, opts), options);

        if (options.count("help"))
        {
            std::cout << opts << '\n';
            return EXIT_SUCCESS;
        }

        auto client = std::make_shared<light::client_protocol>();

        if (options.count("initial-peer"))
        {
           const std::string endpoint = options.at("initial-peer").as<std::string>();
           ilog("Set initial-peer as ${p}", ("p", endpoint));
           client->add_initial_peer(endpoint);
        }

        fc::api<light::client_protocol> api(client);

        auto driver_cli = std::make_shared<fc::rpc::cli>();

        auto websocket_server = std::make_shared<fc::http::websocket_server>();
        if (options.count("rpc-endpoint"))
        {
           websocket_server->on_connection([&api](const fc::http::websocket_connection_ptr& c){
              std::cout << "here... \n";
              wlog(".");
              auto wsc = std::make_shared<fc::rpc::websocket_api_connection>(*c);
              wsc->register_api(api);
              c->set_session_data(wsc);
           });

           const std::string endpoint = options.at("rpc-endpoint").as<std::string>();
           ilog("Listening for incoming RPC requests on ${p}", ("p", endpoint));

           websocket_server->listen(fc::ip::endpoint::from_string(endpoint));
           websocket_server->start_accept();
        }

        auto http_server = std::make_shared<fc::http::server>();
        if (options.count("rpc-http-endpoint"))
        {            
            const std::string endpoint = options.at("rpc-http-endpoint").as<std::string>();
            ilog("Listening for incoming HTTP RPC requests on ${p}", ("p", endpoint));

            http_server->listen(fc::ip::endpoint::from_string(endpoint));

            //
            // due to implementation, on_request() must come AFTER listen()
            //
            http_server->on_request(
                [&api]( const fc::http::request& req, const fc::http::server::response& resp )
                {
                    auto conn = std::make_shared<fc::rpc::http_api_connection>();

                    conn->register_api( api );
                    conn->on_request( req, resp );
                } );
        }

        driver_cli->register_api(api);
        driver_cli->start();
        driver_cli->wait();
    }
    catch (const fc::exception& e)
    {
        std::cout << e.to_detail_string() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
