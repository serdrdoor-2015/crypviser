#include <iostream>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <mobit/mobit.hpp>

#include <graphene/app/cli.hpp>
#include <graphene/app/plugin.hpp>
#include <graphene/utilities/key_conversion.hpp>
#include <graphene/utilities/config_helper.hpp>

#include <fc/rpc/cli.hpp>
#include <fc/filesystem.hpp>
#include <fc/rpc/websocket_api.hpp>

class registrar
{
public:
    registrar(
            const std::vector<std::string>& initial_peers,
            const std::string& registrar_account,
            const std::string& registrar_pk) :
        registrar_account_(registrar_account),
        registrar_pk_(registrar_pk)
    {
        clinet_protocol_ = std::make_unique<light::client_protocol>();

        for (const std::string& address : initial_peers)
        {
            clinet_protocol_->add_initial_peer(address);
        }
    }

    void register_account(const std::string& name, graphene::chain::public_key_type key)
    {
        clinet_protocol_->register_account(name, key, registrar_account_, registrar_pk_);
    }

private:
    std::unique_ptr<light::client_protocol> clinet_protocol_;
    std::string registrar_account_;
    std::string registrar_pk_;
};

namespace detail
{
struct config
{
    std::string                 registrar_pk;
    std::string                 registrar_account;
    std::vector<std::string>    initial_peers;
};

config parse_config(const std::string& config_path)
{
    namespace po = boost::program_options;
    using keys_t = std::vector<std::string>;

    po::options_description cfg_options;
    cfg_options.add_options()
        ("initial-peers", po::value<std::string>()->required())
        ("registrar", po::value<std::string>()->required())
        ("private-key", po::value<keys_t>()->required());

    po::variables_map options;
    po::store(po::parse_config_file<char>(config_path.c_str(), cfg_options, false), options);

    config cfg;
    try{

        cfg.registrar_account = options.at("registrar").as<std::string>();

        // get private key
        const auto wif_keys = options.at("private-key").as<keys_t>();                
        // check private_key list and if it needed to decrypt them
        const auto salt = graphene::config_helper::get_salt(fc::path(config_path).parent_path());
        const auto wif_private_keys = graphene::app::cli::decrypt_private_keys_interactively(salt, wif_keys);

        // registrar should use at least one key, so let's use first one
        const auto wif_pair = graphene::app::dejsonify<std::pair<graphene::chain::public_key_type, std::string> >(wif_private_keys[0]);
        cfg.registrar_pk = wif_pair.second;

        // get initial-peers
        const auto peers = options.at("initial-peers").as<std::string>();
        cfg.initial_peers = graphene::app::dejsonify<std::vector<std::string> > (peers);
    }
    catch (const std::out_of_range& )
    {
        FC_THROW("missing config arguments");
    }
    catch (const fc::eof_exception& )
    {
        FC_THROW("wrong config parameters");
    }

    return cfg;
}

std::unique_ptr<registrar> make_registrar(const std::string& config_path)
{
    const auto cfg = parse_config(config_path);
    return std::make_unique<registrar>(cfg.initial_peers, cfg.registrar_account, cfg.registrar_pk);
}

std::shared_ptr<fc::http::websocket_server> make_websocket_server(
        const std::string& endpoint,
        const fc::api<registrar>& api)
{
    auto websocket_server = std::make_shared<fc::http::websocket_server>();
    websocket_server->on_connection([&api](const fc::http::websocket_connection_ptr& c){
       std::cout << "here... \n";
       wlog(".");
       auto wsc = std::make_shared<fc::rpc::websocket_api_connection>(*c);
       wsc->register_api(api);
       c->set_session_data(wsc);
    });

    ilog("Listening for incoming RPC requests on ${p}", ("p", endpoint));

    websocket_server->listen(fc::ip::endpoint::from_string(endpoint));
    websocket_server->start_accept();

    return websocket_server;
}

}

int main(int argc, char** argv)
{
    namespace po = boost::program_options;

    try
    {
        po::options_description opts;
        opts.add_options()
            ("help,h", "Print this help message and exit.")
            ("config,c", po::value<std::string>()->default_value("config.ini"), "Configuration file path.")
            ("rpc-endpoint,r", po::value<std::string>()->default_value("0.0.0.0:9180"), "Endpoint for server websocket RPC to listen on.")
            ("encrypt-private-key", "Encrypt private key in interactive mode.");

        po::variables_map options;
        po::store(po::parse_command_line(argc, argv, opts), options);

        if (options.count("help"))
        {
            std::cout << opts << '\n';
            return EXIT_SUCCESS;
        }

        const auto endpoint = options.at("rpc-endpoint").as<std::string>();

        fc::path config_path(options.at("config").as<std::string>());
        if( config_path.is_relative() )
        {
            config_path = fc::current_path() / config_path;
        }

        if (options.count("encrypt-private-key"))
        {
            const auto salt = graphene::config_helper::get_salt(config_path.parent_path());
            const auto key_pair  = graphene::app::cli::encrypt_private_key_interactively(salt);
            graphene::config_helper::save_extra_option(config_path.preferred_string(),
                                                        "private-key", 
                                                        fc::json::to_string(key_pair));
            return EXIT_SUCCESS;
        }

        std::shared_ptr<registrar> reg = detail::make_registrar(config_path.preferred_string());
        fc::api<registrar> api(reg);

        auto websocket_server = detail::make_websocket_server(endpoint, api);

        auto registrar_cli = std::make_shared<fc::rpc::cli>();
        registrar_cli->register_api(api);
        registrar_cli->start();
        registrar_cli->wait();
    }
    catch (const fc::exception& e)
    {
        std::cout << e.to_detail_string() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

FC_API( registrar,
        (register_account)
      )
