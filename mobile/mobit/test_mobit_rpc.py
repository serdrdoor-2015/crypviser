# dependencies: pip install pytest requests dateutil

import pytest
import time
import string
import json
import random
import requests
from datetime import datetime
import subprocess

TESTNET_HOST = "localhost"
INIT_PEER_1 = "ws://%s:11010" % TESTNET_HOST
INIT_PEER_A = "ws://%s:12020" % TESTNET_HOST
REGISTRAR_SERVER = "ws://%s:9180" % TESTNET_HOST

CVCOIN_SYM = "CVT"
BROADCAST_ERROR = 2
REQ_ID = 0

def get_timestamp():
    now = datetime.now()
    return str(int(now.strftime("%s")) * 1000)


def generate_random_string(len):
    gen = lambda n: ''.join([random.choice(string.lowercase) for i in xrange(n)])
    return gen(len)


def generate_random_key():
    ts = get_timestamp()
    key = ts + generate_random_string(10)
    return key


def generate_random_name():
    ts = get_timestamp()
    name = generate_random_string(10) + ts
    return name


class Account(object):
    def __init__(self, name = generate_random_name()):
        self.name = name
        self.pub_key = "CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV"
        self.priv_key = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"


class Client(object):
    def __init__(self):
        endpoint = '127.0.0.1:9172'
        self.uri = 'http://%s' % endpoint
        drv_cmd = './driver --rpc-http-endpoint=%s' % endpoint
        self.driver = subprocess.Popen(drv_cmd.split(), stdout=subprocess.PIPE, stdin=subprocess.PIPE)

        time.sleep(0.1)

    def close(self):
        self.driver.communicate()


    # utils
    def push_request(self, method, *arguments):
        global REQ_ID

        response = requests.post(self.uri, json={
            "jsonrpc": "2.0",
            "method": method,
            "params": list(*arguments),
            "id": REQ_ID
        })

        result = json.loads(response.content)
        status_code = response.status_code
        response.close()

        assert result["id"] == -1 or result["id"] == REQ_ID

        del result["id"]

        REQ_ID += 1
        return (status_code, result)

    def send_request(self, method, *arguments):
        status_code, result = self.push_request(method, *arguments)
        assert status_code == 200
        return result["result"]

    # api
    def set_operation_timeout(self, val):
        self.send_request("set_operation_timeout", [val])

    def get_last_error(self):
        return self.send_request("get_last_error")

    def get_last_error_code(self):
        res = self.get_last_error()
        return res["code"]

    def get_last_error_message(self):
        res = self.get_last_error()
        return res["code"]

    def add_initial_peer(self, val):
        self.send_request("add_initial_peer", [val])
        assert 0 == self.get_last_error_code()

    def discover_peers(self):
        self.send_request("discover_peers")
        assert 0 == self.get_last_error_code()

    def get_potential_addresses(self):
        return self.send_request("get_potential_addresses")

    def get_inaccessible_peers(self):
        return self.send_request("get_inaccessible_peers")

    def get_blacklisted_addresses(self):
        return self.send_request("get_blacklisted_addresses")

    def synchronize_block_headers(self, begin, end):
        self.send_request("synchronize_block_headers", [begin, end])

    def synchronize_all_block_headers(self):
        self.send_request("synchronize_all_block_headers")

    def get_key(self, val):
        return self.send_request("get_key", [val])

    def get_key_unsecure(self, val):
        return self.send_request("get_key_unsecure", [val])

    def get_expiration(self, val):
        return self.send_request("get_expiration", [val])

    def get_expiration_unsecure(self, val):
        return self.send_request("get_expiration_unsecure", [val])

    def update_pk(self, account_name, key, amount, asset_symbol, wif_key):
        self.send_request("update_pk", [account_name, key, amount, asset_symbol, wif_key])

    def register_account(self, name, key, registrar, wif_key):
        self.send_request("register_account", [name, key, registrar, wif_key])

    def register_account_with_registrar_server(self, server, name, pub_key):
        self.send_request("register_account_with_registrar_server", [server, name, pub_key])

    def transfer(self, from_account, to_account, amount, wif_key):
        self.send_request("transfer", [from_account, to_account, amount, wif_key])

    def get_number_head_block(self):
        return self.send_request("get_number_head_block")

    def set_chain_id(self, val):
        self.send_request("set_chain_id", [val])

    def get_chain_id(self):
        return self.send_request("get_chain_id")

    def get_account_history(self, account_name):
        return self.send_request("get_account_history", [account_name])

    #
    def generate_actor(self):
        acc = Account(generate_random_name())
        self.register_account(acc.name, acc.pub_key, "nathan", acc.priv_key)
        self.transfer("nathan", acc.name, "10000", acc.priv_key)
        self.actor = acc
        self.wait_blocks(1)
        return self.actor

    def get_actor(self):
        return self.actor

    def wait_blocks(self, num_blocks):
        block = self.get_number_head_block()
        while self.get_number_head_block() - block < num_blocks:
            assert 0 == self.get_last_error_code()
            time.sleep(1)
        

@pytest.yield_fixture
def idle():
    idle = Client()
    yield idle
    idle.close()

@pytest.yield_fixture
def engaged():
    engaged = Client()
    engaged.add_initial_peer(INIT_PEER_1)
    engaged.generate_actor()
    yield engaged
    engaged.close()


def test_disabled_peers(idle):
    peer = "ws://unreachable:1111"
    idle.set_operation_timeout(1)
    idle.add_initial_peer(peer)
    disabled_after = idle.get_inaccessible_peers()
    assert peer in disabled_after


def test_discover_peers(idle):
    idle.add_initial_peer(INIT_PEER_A)
    peers = idle.get_potential_addresses()

    assert 1 < len(peers)


@pytest.mark.parametrize('amount', ["15", "13177"])
def test_update_pk(engaged, amount):
    acc = engaged.generate_actor()
    new_key = generate_random_key()
    engaged.update_pk(acc.name, new_key, amount, CVCOIN_SYM, acc.priv_key)
    assert {'code': 0, 'message': ''} == engaged.get_last_error()
    
    engaged.wait_blocks(1)

    key = engaged.get_key(acc.name)
    assert 0 == engaged.get_last_error_code()
    assert key == new_key

    ex = engaged.get_expiration(acc.name)
    assert 0 == engaged.get_last_error_code()
    assert ex != ""


def test_update_pk_to_existing(engaged):
    acc = engaged.generate_actor()    
    new_key = generate_random_key()

    engaged.update_pk(acc.name, new_key, "10", CVCOIN_SYM, acc.priv_key)
    assert {'code': 0, 'message': ''} == engaged.get_last_error()

    engaged.wait_blocks(1)

    engaged.update_pk(acc.name, new_key, "20", CVCOIN_SYM, acc.priv_key)
    assert BROADCAST_ERROR == engaged.get_last_error_code()


def test_register_account_twice(engaged):
    acc = engaged.get_actor()
    engaged.register_account(acc.name, acc.pub_key, "nathan", acc.priv_key)
    assert BROADCAST_ERROR == engaged.get_last_error_code()


def test_register_account_via_server(engaged):
    new_acc = generate_random_name()
    engaged.register_account_with_registrar_server(
                        REGISTRAR_SERVER,
                        new_acc,
                        "CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV")
    assert {'code': 0, 'message': ''} == engaged.get_last_error()

    engaged.wait_blocks(3)

    # register with server, check with blockchain
    history = engaged.get_account_history(new_acc)
    assert 0 == engaged.get_last_error_code()
    assert new_acc == json.loads(history)[0]["op"][1]["name"]


def test_producing_blocks(engaged):
    head = engaged.get_number_head_block()
    assert 0 == engaged.get_last_error_code()
    assert head > 0

    engaged.wait_blocks(1)


def test_reset_chain_id_to_valid(idle):
    idle.set_operation_timeout(1)
    idle.add_initial_peer("ws://unreachable:1111")

    # disable unreachable host
    head = idle.get_number_head_block()
    assert 0 == head
    cid_before = idle.get_chain_id()
    disabled_before = idle.get_inaccessible_peers()
    assert 1 == len(disabled_before)

    # connect to live peer set chain-id automatically
    idle.add_initial_peer(INIT_PEER_1)
    cid_after = idle.get_chain_id()
    assert cid_before != cid_after
    
    disabled_after = idle.get_inaccessible_peers()
    assert 0 == len(disabled_after)


def test_reset_chain_id_to_foreign(engaged):
    engaged.set_chain_id("ca10c8c479d5dc81a91dcbed98f65e848ca10c8c479d5dc81a91dcbed98f65e8")
    assert 0 == engaged.get_last_error_code()
    
    head = engaged.get_number_head_block()
    assert head == 0