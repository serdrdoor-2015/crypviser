set(DIR_HEADERS "include/mobit")
file(GLOB HEADERS "${DIR_HEADERS}/*.hpp")
file(GLOB MOBIT_TEST "test_mobit.py" "test_mobit_rpc.py")

enable_testing()

set( MOBIT_FILES
     mobit.cpp
   )

add_library( mobit ${MOBIT_FILES} )

target_link_libraries( mobit PRIVATE graphene_app graphene_wallet fc )

target_include_directories( mobit
                            PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

execute_process( COMMAND ${CMAKE_COMMAND} -E make_directory 
                                            "${CMAKE_CURRENT_BINARY_DIR}/${DIR_HEADERS}" )
##copy public headers to current binary dir
foreach( PublicHeader ${HEADERS} )
  add_custom_command( TARGET mobit PRE_BUILD
                      COMMAND ${CMAKE_COMMAND} -E
                          copy ${PublicHeader} "${CMAKE_CURRENT_BINARY_DIR}/${DIR_HEADERS}/" )
endforeach()

##copy test files
foreach( TestFile ${MOBIT_TEST} )
add_custom_command( TARGET mobit POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E
                    copy ${TestFile} "${CMAKE_CURRENT_BINARY_DIR}/" )
endforeach()

include(FindPythonInterp)
add_test( NAME mobit_test
          COMMAND py.test -v ${CMAKE_CURRENT_BINARY_DIR}
          WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR} )


##https://gitlab.kitware.com/cmake/cmake/issues/8774
add_custom_target( check COMMAND ${CMAKE_CTEST_COMMAND}
                   DEPENDS mobit_test )

install( TARGETS
   mobit

   RUNTIME DESTINATION bin
   LIBRARY DESTINATION lib
   ARCHIVE DESTINATION lib
)

install( FILES ${HEADERS} DESTINATION "${DIR_HEADERS}" )

##driver application
add_executable( driver driver.cpp )

target_link_libraries( driver
                       PRIVATE mobit graphene_app graphene_net graphene_chain graphene_utilities graphene_wallet )

install( TARGETS
   driver

   RUNTIME DESTINATION bin
   LIBRARY DESTINATION lib
   ARCHIVE DESTINATION lib
)


##registrar application
add_executable( registrar registrar.cpp )

target_link_libraries( registrar
                       PRIVATE mobit graphene_app graphene_net graphene_chain graphene_utilities graphene_wallet )

install( TARGETS
   registrar

   RUNTIME DESTINATION bin
   LIBRARY DESTINATION lib
   ARCHIVE DESTINATION lib
)
