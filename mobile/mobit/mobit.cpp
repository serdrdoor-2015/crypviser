#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/websocket/ssl.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <mobit/mobit.hpp>

#include <graphene/wallet/cv_util.hpp>
#include <graphene/app/api.hpp>
#include <graphene/utilities/key_conversion.hpp>
#include <graphene/utilities/words.hpp>
#include <graphene/utilities/git_revision.hpp>

#include <fc/git_revision.hpp>
#include <fc/rpc/websocket_api.hpp>
#include <fc/variant_object.hpp>
#include <fc/smart_ref_impl.hpp>
#include <fc/rpc/bstate.hpp>

#include <iostream>
#include <sstream>
#include <random>
#include <queue>

namespace graphene::wallet {

fc::variant_object about();

} 

namespace light {

using namespace graphene::chain;
namespace ws = boost::beast::websocket;
namespace ssl = boost::asio::ssl;
using tcp = boost::asio::ip::tcp;


FC_DECLARE_EXCEPTION(run_out_of_connections_exception, 0, "Run out of connectiions");

std::string strerror(error_code code)
{
    switch (code)
    {
    case SUCCESS:
        return "SUCCESS";
    case CONNECTION_ERROR:
        return "CONNECTION_ERROR";
    case BROADCAST_ERROR:
        return "BROADCAST_ERROR";
    case INVALID_PARAM:
        return "INVALID_PARAM";
    case CREATE_TRANSACTION_ERROR:
        return "CREATE_TRANSACTION_ERROR";
    case ACCOUNT_NOT_REGISTERED:
        return "ACCOUNT_NOT_REGISTERED";
    case PUBLIC_KEY_NOT_REGISTERED:
        return "PUBLIC_KEY_NOT_REGISTERED";
    case VERIFICATION_FAILURE:
        return "VERIFICATION_FAILURE";
    case INTERNAL_ERROR:
        return "INTERNAL_ERROR";
    case EXCEPTION_OCCURED:
        return "EXCEPTION_OCCURED";
    }

    return std::string();
}

class websocket
{
public:
    websocket()
        : resolver_(context_)
        , wsock_(context_)
        , operation_timeout_(std::chrono::seconds(10))
    {
    }

    virtual ~websocket()
    {
        disconnect();
    }

    void wait_operation_complete(const std::chrono::seconds& timeout)
    {
        auto &ioc = wsock_.next_layer().get_io_context();

        ioc.restart();
        
        auto timepoint = std::chrono::steady_clock::now() + timeout;

        while (!last_operation_result_ && ioc.run_one_until(timepoint))
        {
        }

        if (!last_operation_result_)
        {
            // complete pending operation with status 'cancelled'
            // 
            wsock_.next_layer().cancel();
            while (!last_operation_result_ && ioc.run_one())
            {
            }

            FC_THROW_EXCEPTION(fc::timeout_exception, "Websocket operation");
        }
    }

    void connect(const std::string& host, const std::string& port)
    {
        auto handshake_handler = [&](const boost::system::error_code& ec)
        {
            last_operation_result_ = ec;
        };

        auto connect_handler = [&, handshake_handler]
                            (const boost::system::error_code& ec,
                             const tcp::resolver::iterator& it)
        {
            if (ec)
            {
                last_operation_result_ = ec;
            }
            else if (it == tcp::resolver::iterator())
            {
                last_operation_result_ = boost::asio::error::not_connected;
            }
            else
            {
                wsock_.async_handshake(host, "/", handshake_handler);
            }
        };

        auto resolve_handler = [&, connect_handler](const boost::system::error_code& ec,
                                                    const tcp::resolver::iterator& it)
        {
            if (ec)
            {
                last_operation_result_ = ec;
            }
            else
            {
                boost::asio::async_connect(
                    wsock_.next_layer(), it, tcp::resolver::iterator(), connect_handler);
            }
        };

        last_operation_result_.reset();
        resolver_.async_resolve(host, port, resolve_handler);
        wait_operation_complete(operation_timeout_);

        //
        // operation not complete or return error
        //
        if (!last_operation_result_ || *last_operation_result_)
        {
            FC_THROW_EXCEPTION(fc::unknown_host_exception, "Unknown host");
        }
    }

    void disconnect()
    {
        boost::beast::error_code ec;
        wsock_.close(ws::close_code::normal, ec);
    }

protected:
    boost::asio::io_context context_;
    tcp::resolver resolver_;
    ws::stream<tcp::socket> wsock_;
    fc::optional<boost::system::error_code> last_operation_result_;
    std::chrono::seconds operation_timeout_;
};

class websocket_ssl
{
public:
    explicit websocket_ssl(const std::string& ca_filename)
        : resolver_(context_)
        , ca_filename_(ca_filename)
        , ssl_ctx_(ssl::context::tlsv1)
        , wsock_(context_, ssl_ctx_)
        , operation_timeout_(std::chrono::seconds(10))
    {
    }

    virtual ~websocket_ssl()
    {
        disconnect();
    }

    void wait_operation_complete(const std::chrono::seconds& timeout)
    {
        auto &ioc = wsock_.next_layer().get_io_context();

        ioc.restart();

        auto timepoint = std::chrono::steady_clock::now() + timeout;

        while (!last_operation_result_ && ioc.run_one_until(timepoint))
        {
        }

        if (!last_operation_result_)
        {
            // complete pending operation with status 'cancelled'
            //
            wsock_.next_layer().next_layer().cancel();
            while (!last_operation_result_ && ioc.run_one())
            {
            }

            FC_THROW_EXCEPTION(fc::timeout_exception, "Websocket operation");
        }
    }

    void connect(const std::string& host, const std::string& port)
    {
        // See: fc::http::detail::websocket_tls_client_impl
        ssl_ctx_.set_options(
                    ssl::context::default_workarounds |
                    ssl::context::no_sslv2 |
                    ssl::context::no_sslv3 |
                    ssl::context::single_dh_use);
        setup_peer_verify(host);

        auto handshake_handler = [&](const boost::system::error_code& ec)
        {
            last_operation_result_ = ec;
        };

        auto handshake_ssl_handler = [&, handshake_handler]
                            (const boost::system::error_code& ec)
        {
            if (ec)
            {
                last_operation_result_ = ec;
            }
            else
            {
                wsock_.async_handshake(host, "/", handshake_handler);
            }
        };

        auto connect_handler = [&, handshake_ssl_handler]
                            (const boost::system::error_code& ec, const tcp::resolver::iterator& it)
        {
            if (ec)
            {
                last_operation_result_ = ec;
            }
            else if (it == tcp::resolver::iterator())
            {
                last_operation_result_ = boost::asio::error::not_connected;
            }
            else
            {
                // Perform the SSL handshake
                wsock_.next_layer().async_handshake(
                            ssl::stream_base::client,
                            handshake_ssl_handler);
            }
        };

        auto resolve_handler = [&, connect_handler](const boost::system::error_code& ec,
                                                    const tcp::resolver::results_type& results)
        {
            if (ec)
            {
                last_operation_result_ = ec;
            }
            else
            {
                boost::asio::async_connect(
                    wsock_.next_layer().next_layer(),
                    results.begin(),
                    results.end(),
                    connect_handler);
            }
        };

        last_operation_result_.reset();
        resolver_.async_resolve(host, port, resolve_handler);
        wait_operation_complete(operation_timeout_);

        //
        // operation not complete or return error
        //
        if (!last_operation_result_ || *last_operation_result_)
        {
            FC_THROW_EXCEPTION(fc::unknown_host_exception, "Unknown host");
        }
    }

    void disconnect()
    {
        boost::beast::error_code ec;
        wsock_.close(ws::close_code::normal, ec);
    }

protected:
    // See: websocket_tls_client_impl::setup_peer_verify
    void setup_peer_verify(const std::string& host)
    {
        if (ca_filename_ == "_none")
        {
            return;
        }
        ssl_ctx_.set_verify_mode(ssl::verify_peer);

        if (ca_filename_ == "_default")
        {
            ssl_ctx_.set_default_verify_paths();
        }
        else
        {
            ssl_ctx_.load_verify_file(ca_filename_);
        }

        ssl_ctx_.set_verify_depth(10);
        ssl_ctx_.set_verify_callback(ssl::rfc2818_verification(host));
    }

    boost::asio::io_context context_;
    tcp::resolver resolver_;
    std::string ca_filename_;
    ssl::context ssl_ctx_;
    ws::stream<ssl::stream<tcp::socket>> wsock_;
    fc::optional<boost::system::error_code> last_operation_result_;
    std::chrono::seconds operation_timeout_;
};

template<typename Session>
class websocket_session : public fc::http::websocket_connection, private Session
{
public:
    template<typename ...Args>
    explicit websocket_session(Args&&... args)
        : Session(std::forward<Args>(args)...)
    {
    }

    const std::chrono::seconds& operation_timeout() const
    {
        return this->operation_timeout_;
    }

    void set_operation_timeout(const std::chrono::seconds& timeout)
    {
        this->operation_timeout_ = timeout;
    }

    void connect(std::string addr)
    {
        auto scheme_pos = addr.find("://");
        if (scheme_pos != std::string::npos)
        {
            addr = addr.substr(scheme_pos + 3);
        }

        if (addr.find(':') == std::string::npos)
        {
            FC_THROW("Parsing address '${a}' error. Port not specified", ("a", addr));
        }

        auto host = addr.substr(0, addr.find(':'));
        auto port = addr.substr(addr.find(':') + 1);

        Session::connect(host, port);
    }

    template <typename Buffer>
    void write(Buffer&& buf)
    {
        this->last_operation_result_.reset();

        this->wsock_.async_write(boost::asio::buffer(std::forward<Buffer>(buf)),
                    [&](const boost::system::error_code& ec, std::size_t) {
                        this->last_operation_result_ = ec;
                    });

        this->wait_operation_complete(operation_timeout());
    }

    auto read()
    {
        this->last_operation_result_.reset();

        boost::beast::multi_buffer buf;
        this->wsock_.async_read(buf,
                    [&](const boost::system::error_code& ec, std::size_t) {
                        this->last_operation_result_ = ec;
                    });

        this->wait_operation_complete(operation_timeout());

        return buf;
    }

    virtual void send_message( const std::string& message )override
    {
        write(message);

        auto res = read();
        std::stringstream ss;
        ss << boost::beast::buffers(res.data()) << std::endl;
        on_message(ss.str());
    }

    virtual void close( int64_t code, const std::string& reason  )override
    {
        this->disconnect();
    }

    virtual std::string get_request_header(const std::string& key)override
    {
        return "";
    }
};

class websocket_api : public fc::rpc::websocket_api_connection
{
public:
    websocket_api(fc::http::websocket_connection& ws_session)
        : fc::rpc::websocket_api_connection(ws_session)
    {
    }

    ~websocket_api()
    {
    }

    virtual fc::variant send_call(
            fc::api_id_type api_id,
            std::string method_name,
            fc::variants args = fc::variants()) override
    {
        error_.reset();
        reply_ = fc::variant{};

        auto request = start_remote_call("call", { api_id, std::move(method_name), std::move(args) });
        _connection.send_message(fc::json::to_string(request));

        check_error();

        return reply_;
    }

    virtual fc::variant send_callback(
            uint64_t callback_id,
            fc::variants args = fc::variants()) override
    {
        error_.reset();
        reply_ = fc::variant{};

        auto request = start_remote_call("callback", { callback_id, std::move(args) });
        _connection.send_message(fc::json::to_string(request));

        check_error();

        return reply_;
    }

    virtual void send_notice(
            uint64_t callback_id,
            fc::variants args = fc::variants()) override
    {
        error_.reset();        
        reply_ = fc::variant{};

        fc::rpc::request req{ fc::optional<uint64_t>(), "notice", { callback_id, std::move(args) }};
        _connection.send_message(fc::json::to_string(req));

        check_error();
    }

protected:
    virtual void handle_reply(const fc::rpc::response& reply) override
    {
        if (reply.result)
        {
            reply_ = *reply.result;
        }
        else if (reply.error)
        {
            error_ = reply.error;
        }
        else
        {
            reply_ = fc::variant{};
        }
    }

    fc::rpc::request start_remote_call(const std::string& method_name, fc::variants args)
    {
        return { next_id_++, method_name, std::move(args) };
    }

    void check_error()
    {
        if (error_)
        {
            FC_THROW("${error}", ("error", error_->message));
        }
    }

protected:
    int64_t next_id_ = 1;
    fc::variant reply_;
    fc::optional<fc::rpc::error_object> error_;
};


//__________________________________________________________________________________________________
// if the user executes the same command twice in quick succession,
// we might generate the same transaction id, and cause the second
// transaction to be rejected.  This can be avoided by altering the
// second transaction slightly (bumping up the expiration time by
// a second).  Keep track of recent transaction ids we've generated
// so we can know if we need to do this
struct recently_generated_transaction_record
{
    fc::time_point_sec generation_time;
    graphene::chain::transaction_id_type transaction_id;
};

struct timestamp_index{};
typedef boost::multi_index_container<recently_generated_transaction_record,
        boost::multi_index::indexed_by<boost::multi_index::hashed_unique<boost::multi_index::member<recently_generated_transaction_record,
        graphene::chain::transaction_id_type,
        &recently_generated_transaction_record::transaction_id>,
        std::hash<graphene::chain::transaction_id_type> >,
        boost::multi_index::ordered_non_unique<boost::multi_index::tag<timestamp_index>,
        boost::multi_index::member<recently_generated_transaction_record, fc::time_point_sec,
        &recently_generated_transaction_record::generation_time> > > > recently_generated_transaction_set_type;

recently_generated_transaction_set_type _recently_generated_transactions;
//__________________________________________________________________________________________________

static void set_operation_fees(signed_transaction& tx, const fee_schedule& s) 
{
    for(auto& op : tx.operations)
    {
        s.set_fee(op);
    }
}

static fc::optional<std::string> get_rpc_endpoint(const graphene::net::peer_status& peer)
{
    auto port_it = peer.info.find("rpc_port");
    if (port_it != peer.info.end())
    {
        auto port = port_it->value().as<uint16_t>();
        if (port != 0)
        {
            auto ip = peer.host.get_address();
            return "ws://" + static_cast<std::string>(ip) + ":" + std::to_string(port);
        }
    }

    return {};
}

static fc::optional<std::string> get_rpc_tls_endpoint(const graphene::net::peer_status& peer)
{
    auto port_it = peer.info.find("rpc_tls_port");
    if (port_it != peer.info.end())
    {
        auto port = port_it->value().as<uint16_t>();
        if (port != 0)
        {
            auto ip = peer.host.get_address();
            return "wss://" + static_cast<std::string>(ip) + ":" + std::to_string(port);
        }
    }

    return {};
}

static cv::expiring_key key_from_operation(const operation& op)
{
    const auto& o = op.get<pk_update_operation>();
    return {o.key, o.expiration, o.permissions};
}

static bool contains_key(const operation& op, account_id_type account_id)
{
    auto o = op.get_if<pk_update_operation>();
    return (o != nullptr && o->account == account_id);
}

static std::vector<char> to_bytes(const std::string& str)
{
    return std::vector<char>(str.begin(), str.end());
}

static balance to_balance(std::int64_t balance)
{
    auto precision = asset::scaled_precision(GRAPHENE_BLOCKCHAIN_PRECISION_DIGITS).value;
    return {balance, precision};
}

struct session {
    using websocket_connection_ptr = std::unique_ptr<fc::http::websocket_connection>;

    session(const session&) = delete;
    session& operator=(const session&) = delete;

    explicit session(
            const std::string& address,
            const chain_id_type& chain_id_ = chain_id_type(),
            uint64_t operation_timeout_in_sec = 10):
        remote_endpoint(address),
        chain_id(chain_id_),
        operation_timeout(std::chrono::seconds(operation_timeout_in_sec)),
        connected(false)
    {
    }

    void connect()
    {
        if (connected) 
        {
            return;
        }

        ws = connect(remote_endpoint, operation_timeout);
    
        auto client_connection = std::make_shared<websocket_api>(*ws);

        database_api = client_connection->get_remote_api<graphene::app::database_api>(0);

        auto remote_chain_id = database_api->get_chain_id();

        if (chain_id_type() == chain_id)
        {
            chain_id = remote_chain_id;
        }

        if (remote_chain_id != chain_id)
        {
            FC_THROW( "Remote server gave us an unexpected chain_id",
                ("remote_chain_id", remote_chain_id)
                ("chain_id", chain_id) );
        }

        login_api = client_connection->get_remote_api<graphene::app::login_api>(1);
        remote_net_broadcast = login_api->network_broadcast();

        connected = true;
    }

    static websocket_connection_ptr connect(
            const std::string& endpoint,
            const std::chrono::seconds& operation_timeout)
    {
        const std::string wss_prefix = "wss:";
        if (endpoint.compare(0, wss_prefix.size(), wss_prefix) == 0)
        {
            auto ws = std::make_unique<websocket_session<websocket_ssl>>("_default");

            ws->connect(endpoint);
            ws->set_operation_timeout(operation_timeout);

            return std::move(ws);
        }

        auto ws = std::make_unique<websocket_session<websocket>>();

        ws->connect(endpoint);
        ws->set_operation_timeout(operation_timeout);

        return std::move(ws);
    }

    std::unique_ptr<fc::http::websocket_connection> ws;
    std::string remote_endpoint;
    chain_id_type chain_id;
    std::chrono::seconds operation_timeout;

    fc::api<graphene::app::database_api> database_api;
    fc::api<graphene::app::login_api> login_api;
    fc::api<graphene::app::network_broadcast_api> remote_net_broadcast;

    bool connected;
};

template<typename T>
class expiring_list
{
public:
    std::vector<T> values() const
    {
        std::vector<T> v;
        v.reserve(items.size());

        for (auto i : items)
        {
            v.push_back(i.first);
        }

        return v;
    }

    bool contains(const T& addr) const
    {
        return items.end() != items.find(addr);
    }

    template<typename Rep, typename Period>
    void insert(T value, const std::chrono::duration<Rep, Period>& period)
    {
        const auto now = std::chrono::system_clock::now();
        const auto until = now + period;

        typename decltype(items)::const_iterator it;
        std::tie(it, std::ignore) = items.emplace(std::move(value), until);
        expiration_queue.push_back(it);
    }

    void erase(const T& value)
    {
        auto it = items.find(value);

        if (items.end() != it)
        {
            expiration_queue.erase(
                std::remove(expiration_queue.begin(), expiration_queue.end(), it),
                expiration_queue.end());

            items.erase(it);
        }
    }

    void erase_expired()
    {
        const auto now = std::chrono::system_clock::now();
        while (!expiration_queue.empty())
        {
            const auto it = expiration_queue.front();

            const auto disabled_until = it->second;
            if (now >= disabled_until)
            {
                expiration_queue.pop_front();
                items.erase(it);
            }
            else
            {
                break;
            }
        }
    }

    void clear()
    {
        items.clear();
        expiration_queue.clear();
    }

private:
    using timestamp = std::chrono::time_point<std::chrono::system_clock>;

    std::map<T, timestamp> items;
    std::deque<typename decltype(items)::const_iterator> expiration_queue;
};


class network {
    using peers_set = std::set<std::string>;
    using peers_it = peers_set::const_iterator;

public:    
    network() : operation_timeout(10)
    {
    }

    network(const network&) = delete;
    network& operator=(const network&) = delete;

    void set_operation_timeout(uint64_t timeout_in_seconds)
    {
        operation_timeout = timeout_in_seconds;
    }

    void set_chain_id(chain_id_type chain_id_)
    {
        if (chain_id != chain_id_)
        {
            chain_id = chain_id_;
            reset_dynamic_peers();

            // re-discover all peers from initial
            //
            for (const std::string& address : initial_peers)
            {
                discover_peers_of_address(address);
            }                
        }
    }

    chain_id_type get_chain_id()
    {
        if (chain_id_type() == chain_id)
        {
            for (const std::string& address : initial_peers)
            {
                try
                {
                    // Force getting remote chain-id, if live connection available
                    //
                    connect(address);

                    return chain_id;
                }
                catch (const fc::exception&)
                {
                }
                catch (const std::exception&)
                {
                }

                disable_inaccessible_address(address);
            }                
        }

        return chain_id;
    }

    auto get_random_node_connection()
    {
        inaccessible_addresses.erase_expired();
        unsynchronized_addresses.erase_expired();

        auto random_pool = shuffle_peers();

        // If there are no nodes to connect to, reset inaccessible nodes
        if (random_pool.empty())
        {
            inaccessible_addresses.clear();
            random_pool = shuffle_peers();
        }

        for (peers_it address : random_pool)
        {
            try
            {
                return connect(*address);
            }
            catch (const fc::exception&)
            {
            }
            catch (const std::exception&)
            {
            }

            disable_inaccessible_address(*address);
        }

        FC_THROW_EXCEPTION(run_out_of_connections_exception, "no valid addresses were found to connect to");
    }

    //
    void set_initial_peers(const std::vector<std::string>& peers)
    {
        reset_dynamic_peers();

        initial_peers.clear();
        initial_peers.insert(peers.begin(), peers.end());

        for (const std::string& address : peers)
        {
            discover_peers_of_address(address);
        }
    }

    void add_initial_peer(const std::string& address)
    {
        initial_peers.insert(address);
        inaccessible_addresses.erase(address);
        unsynchronized_addresses.erase(address);
        permit_address(address);
        discover_peers_of_address(address);
    }

    bool address_is_initial(const std::string& addr) const
    {
        return initial_peers.end() != initial_peers.find(addr);
    }

    void reset_dynamic_peers()
    {
        discovered_peers.clear();
        inaccessible_addresses.clear();
        unsynchronized_addresses.clear();
        blacklisted_peers.clear();
    }

    void discover_peers()
    {
        std::vector<std::string> addresses(initial_peers.begin(), initial_peers.end());
        addresses.insert(addresses.end(), discovered_peers.begin(), discovered_peers.end());

        for (const std::string& address : addresses)
        {
            discover_peers_of_address(address);
        }
    }

    void discover_peers_of_address(const std::string& address)
    {
        try
        {
            auto conn = connect(address);
            
            // update potential peers        
            auto net_api = conn->login_api->network_peer();

            for (const auto& peer : net_api->get_connected_peers())
            {
                if (auto addr = get_rpc_endpoint(peer); addr)
                {
                    if (!address_is_initial(*addr) && !address_is_blacklisted(*addr))
                    {
                        discovered_peers.emplace(std::move(*addr));
                    }
                }

                if (auto addr = get_rpc_tls_endpoint(peer); addr)
                {
                    if (!address_is_initial(*addr) && !address_is_blacklisted(*addr))
                    {
                        discovered_peers.emplace(std::move(*addr));
                    }
                }
            }

            return;
        }
        catch (const fc::exception&)
        {
        }
        catch (const std::exception&)
        {
        }

        disable_inaccessible_address(address);
    }

    std::vector<std::string> get_potential_peers()
    {
        std::vector<std::string> peers;
        peers.insert(peers.end(), initial_peers.begin(), initial_peers.end());
        peers.insert(peers.end(), discovered_peers.begin(), discovered_peers.end());
        return peers;
    }

    std::vector<std::string> get_blacklisted_peers()
    {
        std::vector<std::string> peers;
        peers.insert(peers.end(), blacklisted_peers.begin(), blacklisted_peers.end());
        return peers;
    }

    bool address_is_blacklisted(const std::string& addr) const
    {
        // TODO: blacklist only ip addresses
        //       extract ip from addr and check blacklist
        return blacklisted_peers.end() != blacklisted_peers.find(addr);
    }

    void blacklist_addresses(const std::set<std::string>& addresses)
    {
        for (const std::string& address : addresses)
        {
            discovered_peers.erase(address);
            blacklisted_peers.insert(address);
            //TODO remove from disabled
        }
    }    

    std::vector<std::string> get_inaccessible_peers() const
    {
        return inaccessible_addresses.values();
    }

    std::vector<std::string> get_unsynchronized_peers() const
    {
        return unsynchronized_addresses.values();
    }

    bool address_is_disabled(const std::string& addr) const
    {
        return inaccessible_addresses.contains(addr) || unsynchronized_addresses.contains(addr);
    }

    auto get_disable_period(const std::string& address)
    {
        using namespace std::chrono;
        if (initial_peers.count(address))
        {
            return minutes{1};
        }

        return minutes{60};
    }

    void disable_unsynchronized_address(const std::string& address)
    {
        unsynchronized_addresses.insert(address, get_disable_period(address));
    }

    void disable_inaccessible_address(const std::string& address)
    {
        inaccessible_addresses.insert(address, get_disable_period(address));
    }

    void permit_address(const std::string& address)
    {
        blacklisted_peers.erase(address);
    }

protected:
    std::unique_ptr<session> connect(const std::string& address)
    {
        auto sess = std::make_unique<session>(address, chain_id, operation_timeout);
        sess->connect();

        // check first connection
        if (chain_id_type() == chain_id)
        {
            chain_id = sess->chain_id;
            reset_dynamic_peers();
        }    

        return sess;
    }

    std::vector<peers_it> shuffle_peers()
    {        
        std::mt19937 gen(std::random_device{}());
        std::vector<peers_it> peers(pre_set->size() + post_set->size());

        auto it = pre_set->begin();
        auto begin = peers.begin();
        auto end = peers.begin() + pre_set->size();
        std::generate(begin, end, [&]() mutable { return it++; });
        std::shuffle(begin, end, gen);

        it = post_set->begin();
        begin = end;
        end = peers.end();
        std::generate(begin, end, [&]() mutable { return it++; });
        std::shuffle(begin, end, gen);

        auto last = std::remove_if(peers.begin(), peers.end(), [&, this](auto addr) {
                        return address_is_disabled(*addr) || address_is_blacklisted(*addr); 
                     });

        peers.erase(last, peers.end());

        std::swap(pre_set, post_set);

        return peers;
    }

private:
    peers_set initial_peers;
    peers_set discovered_peers;
    peers_set blacklisted_peers;
    expiring_list<std::string> inaccessible_addresses;
    expiring_list<std::string> unsynchronized_addresses;

    peers_set* pre_set  = &initial_peers;
    peers_set* post_set = &discovered_peers;

    uint64_t operation_timeout;
    chain_id_type chain_id;        
};

class client_protocol::client {
private:
    struct sample_item
    {
        std::vector<block_header> headers;
        std::size_t frequency;
        std::set<std::string> peer_addresses;
    };

    using sample_of_headers = std::map<fc::sha256, sample_item>;

    std::pair<fc::sha256, std::vector<block_header>> get_headers(
            std::uint32_t first_block,
            std::uint32_t last_block)
    {
        std::string hash;

        std::vector<block_header> headers;
        headers.reserve(last_block - first_block + 1);

        for (std::uint32_t i = first_block; i <= last_block; ++i)
        {
            fc::optional<block_header> header = psession->database_api->get_block_header(i);
            if (header.valid())
            {
                hash.append(header->digest().str());
                headers.push_back(std::move(*header));
            }
        }

        return std::make_pair(fc::sha256::hash(hash), std::move(headers));
    }

    sample_of_headers get_sample_of_headers(
            std::uint32_t first_block,
            std::uint32_t last_block,
            std::size_t sample_size)
    {
        sample_of_headers sample;

        for (std::size_t processed_peers = 0; processed_peers < sample_size; ++processed_peers)
        {
            connect();

            fc::sha256 key;
            sample_item value = { { }, 1, { get_remote_endpoint() } };
            std::tie(key, value.headers) = get_headers(first_block, last_block);

            bool inserted = false;
            sample_of_headers::iterator it;
            std::tie(it, inserted) = sample.emplace(key, std::move(value));

            if (!inserted)
            {
                sample_item& item = it->second;

                ++item.frequency;
                item.peer_addresses.insert(get_remote_endpoint());
            }
        }

        return sample;
    }

    void cache_block_headers(std::uint32_t first_block, std::uint32_t last_block)
    {
        const auto validation_sample_size = validation_level;
        constexpr uint16_t validation_threshold_percent(75);

        sample_of_headers sample = get_sample_of_headers(first_block, last_block, validation_sample_size);

        auto max_it = std::max_element(
                    sample.begin(),
                    sample.end(),
                    [](const auto& lhs, const auto& rhs) {
                        return lhs.second.frequency < rhs.second.frequency;
                    });

        auto max_validation_percent = (100 * max_it->second.frequency) / validation_sample_size;

        if (max_validation_percent < validation_threshold_percent)
        {
            FC_THROW("Block validation threshold(${t}%) not reached - ${f} of ${s}(${p}%)",
                        ("t", validation_threshold_percent)
                        ("f", max_it->second.frequency)
                        ("s", validation_sample_size)
                        ("p", max_validation_percent) );
        }

        for (const block_header& header : max_it->second.headers)
        {
            block_headers[header.block_num()] = header;
        }

        for (auto it = sample.begin(); it != sample.end(); ++it)
        {
            if (it != max_it)
            {
                pnetwork->blacklist_addresses(it->second.peer_addresses);
            }
        }
    }

    template<typename Callable, typename Predicate>
    auto validate_result(Callable f, Predicate p)
    {
        std::deque<std::string> endpoints;
        std::exception_ptr eptr;
        std::uint16_t exception_count = 0;

        const std::uint16_t max_tries = validation_level;
        for (std::uint16_t i = 0; i < max_tries; ++i)
        {
            try
            {
                auto result = f();
                if (p(result))
                {
                    for (const auto& addr : endpoints)
                    {
                        pnetwork->disable_unsynchronized_address(addr);
                    }

                    return result;
                }

                endpoints.push_back(get_remote_endpoint());
                connect();
            }
            catch (const fc::exception&)
            {
                ++exception_count;
                eptr = std::current_exception();
            }
            catch (const std::exception&)
            {
                ++exception_count;
                eptr = std::current_exception();
            }
        }

        if (eptr && exception_count == max_tries)
        {
            std::rethrow_exception(eptr);
        }

        return std::result_of_t<Callable()>();
    }

    template<typename Callable>
    auto validate_optional_result(Callable f)
    {
        auto p = [](const auto& result)
        {
            return result.valid();
        };

        return validate_result(f, p);
    }

public:
    template <typename... Ts>
    using opt = fc::optional<Ts...>;
    using block_headers_t = std::map<std::uint32_t, block_header>;

    client() :
        pnetwork(new network()),
        validation_level(10)
    {
    }

    const block_headers_t& get_block_headers() const {
        return block_headers;
    }

    template<typename T>
    void set_block_headers(T&& headers) {
        block_headers = std::forward<T>(headers);
    }

    network& get_network()
    {
        return *pnetwork;
    }

    void connect()
    {
        psession = pnetwork->get_random_node_connection();
    }

    void set_validation_level(std::uint16_t n)
    {
        validation_level = n;
    }

    dynamic_global_property_object get_dynamic_global_properties()
    {
        return psession->database_api->get_dynamic_global_properties();
    }

    global_property_object get_global_properties()
    {
        return psession->database_api->get_global_properties();
    }

    std::vector<opt<account_object>> get_accounts(const std::vector<account_id_type>& account_ids)
    {
        return psession->database_api->get_accounts(account_ids);
    }

    std::vector<asset> get_account_balances(account_id_type id, const flat_set<asset_id_type>& assets)
    {
        return psession->database_api->get_account_balances(id, assets);
    }

    opt<account_object> get_account_by_name(std::string name)
    {
        return validate_optional_result([&, this]
        {
            return psession->database_api->get_account_by_name(name);
        });
    }

    cv::expiring_key get_account_pk(account_id_type id)
    {
        return psession->database_api->get_account_pk(id);
    }

    opt<pk_object> get_pk_object(account_id_type id)
    {
        return validate_optional_result([&, this]
        {
            return psession->database_api->get_pk_object(id);
        });
    }

    fc::variants get_objects(const std::vector<object_id_type>& ids)
    {
        auto f = [&ids, this]
        {
            return psession->database_api->get_objects(ids);
        };
        auto p = [&ids](const auto& result)
        {
            return ids.size() == result.size();
        };

        return validate_result(f, p);
    }

    opt<graphene::app::transaction_proof> prove_account(std::string account_name)
    {
        return validate_optional_result([&, this]
        {
            return psession->database_api->prove_account(account_name);
        });
    }

    opt<graphene::app::transaction_proof> prove_account_pk(account_id_type account)
    {
        return validate_optional_result([&, this]
        {
            return psession->database_api->prove_account_pk(account);
        });
    }

    std::vector<opt<asset_object>> lookup_asset_symbols(const std::vector<string>& symbols_or_ids)
    {
        return psession->database_api->lookup_asset_symbols(symbols_or_ids);
    }

    std::vector<asset_object> list_assets(const std::string& lower_bound_symbol, uint32_t limit)
    {
        return psession->database_api->list_assets(lower_bound_symbol, limit);
    }

    std::vector<opt<asset_object>> get_assets(const std::vector<asset_id_type>& asset_ids)
    {
        return psession->database_api->get_assets(asset_ids);
    }

    void broadcast_transaction(const signed_transaction& tx)
    {
        return psession->remote_net_broadcast->broadcast_transaction(tx);
    }

    std::uint32_t get_head_block_number()
    {
        auto dyn_props = get_dynamic_global_properties();
        return dyn_props.head_block_number;
    }

    std::string get_remote_endpoint() const
    {
        return psession->remote_endpoint;
    }

    opt<block_header> get_synchronized_block(std::uint32_t block_num)
    {
        if (!block_headers.count(block_num))
        {
            cache_block_headers(block_num, block_num);
        }

        auto it = block_headers.find(block_num);
        if (it != block_headers.end())
        {
            return {it->second};
        }

        return {};
    }

    void synchronize_block_headers(std::uint32_t first_block, std::uint32_t last_block)
    {
        const std::uint32_t num_head_block = get_head_block_number();
        last_block = std::min(last_block, num_head_block);

        if (first_block <= last_block)
        {
            cache_block_headers(first_block, last_block);
        }
    }

    void synchronize_all_block_headers()
    {
        const std::uint32_t num_head_block = get_head_block_number();
        const std::uint32_t first_block = block_headers.empty() ? 1 : block_headers.rbegin()->first;

        if (first_block <= num_head_block)
        {
            cache_block_headers(first_block, num_head_block);
        }
    }

    std::vector<operation_history_object> get_account_history(account_id_type account_id)
    {
        auto api = psession->login_api->history();

        
        auto res = std::vector<operation_history_object>{};
        while (true)
        {
            auto start = operation_history_id_type{};
            if (!res.empty())
            {
                start = res.back().id;
                start.instance = start.instance.value - 1;
                if (start == operation_history_id_type{})
                {
                    break;
                }
            }

            
            auto partial = api->get_account_history(account_id, operation_history_id_type{}, 100, start);
            res.insert(res.end(), partial.begin(), partial.end());

            if (partial.empty())
            {
                break;
            }
        }

        return res;
    }

private:
    std::unique_ptr<network> pnetwork;
    std::unique_ptr<session> psession;

    block_headers_t block_headers;

    std::uint16_t validation_level;
};

//
//
//
client_protocol::client_protocol()
    : pclient(new client())
{
}

client_protocol::~client_protocol()
{
}

/* DEPRECATED */
void client_protocol::set_remote_endpoint_from_address(const std::string& address)
{
    add_initial_peer(address);
}

client_protocol::key_data client_protocol::get_account_pk_unsecure(const std::string& account_name)
{
    auto account = pclient->get_account_by_name(account_name);

    if (account.valid())
    {
        auto id = account->get_id();
        return {pclient->get_account_pk(id)};
    }

    signal_error(error_code::ACCOUNT_NOT_REGISTERED);
    return {};
}

client_protocol::key_data client_protocol::get_account_pk(const std::string& account_name)
{
    auto account_id = get_verified_account_id(account_name);
    if (account_id == account_id_type())
    {
        return {};
    }

    auto key = get_verified_pk(account_id);
    if (key)
    {
        return *key;
    }

    return {};
}

void client_protocol::sign_transaction(signed_transaction tx, const std::string& wif_key)
{
    flat_set<account_id_type> req_active_approvals;
    flat_set<account_id_type> req_owner_approvals;
    vector<authority>         other_auths;

    tx.get_required_authorities( req_active_approvals, req_owner_approvals, other_auths );

    for( const auto& auth : other_auths )
        for( const auto& a : auth.account_auths )
            req_active_approvals.insert(a.first);

    // std::merge lets us de-duplicate account_id's that occur in both
    //   sets, and dump them into a vector (as required by remote_db api)
    //   at the same time
    vector<account_id_type> v_approving_account_ids;
    std::merge(req_active_approvals.begin(), req_active_approvals.end(),
               req_owner_approvals.begin() , req_owner_approvals.end(),
               std::back_inserter(v_approving_account_ids));

    /// TODO: fetch the accounts specified via other_auths as well.
    vector< fc::optional<account_object> > approving_account_objects =
    pclient->get_accounts( v_approving_account_ids );

    /// TODO: recursively check one layer deeper in the authority tree for keys
    if (approving_account_objects.size() != v_approving_account_ids.size())
    {
        signal_error(CREATE_TRANSACTION_ERROR);
        return;
    }

    flat_map<account_id_type, account_object*> approving_account_lut;
    size_t i = 0;
    for( fc::optional<account_object>& approving_acct : approving_account_objects )
    {
        if( !approving_acct.valid() )
        {
            i++;
            continue;
        }
        approving_account_lut[ approving_acct->id ] = &(*approving_acct);
        i++;
    }

    flat_set<public_key_type> approving_key_set;
    for( account_id_type& acct_id : req_active_approvals )
    {
        const auto it = approving_account_lut.find( acct_id );
        if( it == approving_account_lut.end() ){
            continue;
        }
        const account_object* acct = it->second;
        vector<public_key_type> v_approving_keys = acct->active.get_keys();
        for( const public_key_type& approving_key : v_approving_keys ){
            approving_key_set.insert( approving_key );
        }
    }
    for( account_id_type& acct_id : req_owner_approvals )
    {
        const auto it = approving_account_lut.find( acct_id );
        if( it == approving_account_lut.end() ){
            continue;
        }
        const account_object* acct = it->second;
        vector<public_key_type> v_approving_keys = acct->owner.get_keys();
        for( const public_key_type& approving_key : v_approving_keys ){
           approving_key_set.insert( approving_key );
        }
    }
    for( const authority& a : other_auths )
    {
        for( const auto& k : a.key_auths )
        approving_key_set.insert( k.first );
    }

    auto dyn_props = pclient->get_dynamic_global_properties();
    tx.set_reference_block( dyn_props.head_block_id );

    // first, some bookkeeping, expire old items from _recently_generated_transactions
    // since transactions include the head block id, we just need the index for keeping transactions
    // unique when there are multiple transactions in the same block.  choose a time period that
    // should be at least one block long, even in the worst case.  2 minutes ought to be plenty.
    fc::time_point_sec oldest_transaction_ids_to_track(dyn_props.time - fc::minutes(2));
    auto oldest_transaction_record_iter = _recently_generated_transactions.get<timestamp_index>().lower_bound(oldest_transaction_ids_to_track);
    auto begin_iter = _recently_generated_transactions.get<timestamp_index>().begin();
    _recently_generated_transactions.get<timestamp_index>().erase(begin_iter,
                                                                  oldest_transaction_record_iter);

    uint32_t expiration_time_offset = 0;
    for (;;)
    {
        tx.set_expiration( dyn_props.time + fc::seconds(30 + expiration_time_offset) );
        tx.signatures.clear();

        fc::optional<fc::ecc::private_key> default_priv_key =
                         graphene::utilities::wif_to_key(wif_key);
        FC_ASSERT( default_priv_key.valid(), "Malformed private key 'wif_key'" );
        tx.sign( *default_priv_key, pclient->get_network().get_chain_id() );

        graphene::chain::transaction_id_type this_transaction_id = tx.id();
        auto iter = _recently_generated_transactions.find(this_transaction_id);
        if (iter == _recently_generated_transactions.end())
        {
            //we haven't generated this transaction before, the usual case
            recently_generated_transaction_record this_transaction_record;
            this_transaction_record.generation_time = dyn_props.time;
            this_transaction_record.transaction_id = this_transaction_id;
            _recently_generated_transactions.insert(this_transaction_record);
            break;
        }
        // else we've generated a dupe, increment expiration time and re-sign it
        ++expiration_time_offset;
    }

    try
    {
        pclient->broadcast_transaction(tx);
    }
    catch (const fc::exception& e)
    {
       signal_error(error_code::BROADCAST_ERROR, e.to_string());
    }    
}

account_id_type client_protocol::get_verified_account_id(const std::string& account_name)
{
    auto err = [&](error_code code = error_code::ACCOUNT_NOT_REGISTERED)
    {
        signal_error(code);
        return account_id_type();
    };

    auto proof = pclient->prove_account(account_name);
    if (!proof)
    {
        return err();
    }

    auto account = pclient->get_account_by_name(account_name);
    if (!account)
    {
        return err();
    }

    auto block_num = account->reference_transaction.block_num;
    auto block = pclient->get_synchronized_block(block_num);
    if (!block)
    {
        return err();
    }

    if (!verify_merkle(proof->tx, proof->path, block->transaction_merkle_root))
    {
        return err(error_code::VERIFICATION_FAILURE);
    }

    return account->get_id();
}

std::vector<char> client_protocol::encrypt_permissions(const string& perm)
{
    const auto& key = pclient->get_global_properties().parameters.permissions_server_key;
    if (!key || perm.empty())
    {
        return to_bytes(perm);
    }

    return key->encrypt(to_bytes(perm));
}

fc::optional<client_protocol::key_data> client_protocol::get_verified_pk(account_id_type account_id)
{
    auto err = [&](error_code code)
    {
        signal_error(code);
        return fc::optional<key_data>{};
    };

    auto proof = pclient->prove_account_pk(account_id);
    if (!proof)
    {
        return err(error_code::PUBLIC_KEY_NOT_REGISTERED);
    }

    auto pk = pclient->get_pk_object(account_id);
    if (!pk)
    {
        return err(error_code::PUBLIC_KEY_NOT_REGISTERED);
    }

    auto ref = pk->reference_transaction;
    auto block = pclient->get_synchronized_block(ref.block_num);
    if (!block)
    {
        return err(error_code::PUBLIC_KEY_NOT_REGISTERED);
    }

    if (!verify_merkle(proof->tx, proof->path, block->transaction_merkle_root))
    {
        return err(error_code::VERIFICATION_FAILURE);
    }

    const auto& ops = proof->tx.operations;
    auto op = std::find_if(ops.rbegin(), ops.rend(), [&](auto& o)
    {
        return contains_key(o, account_id);
    });

    if (op != ops.rend())
    {
        return { key_data{key_from_operation(*op), ref.block_num, ref.trx_num } };
    }

    return err(error_code::VERIFICATION_FAILURE);
}

void client_protocol::signal_error(error_code code)
{
    signal_error(code, strerror(code));
}

void client_protocol::signal_error(error_code code, const std::string& message)
{
    last_error = { code, message };
}

void client_protocol::signal_success()
{
    last_error = { error_code::SUCCESS, std::string() };
}

template<typename Callable>
auto client_protocol::safe_execute(Callable f)
{
    try
    {
        signal_success();
        return f();
    }
    catch (run_out_of_connections_exception& e)
    {
        signal_error(error_code::CONNECTION_ERROR, e.to_string());
    }
    catch (std::exception& e)
    {
        signal_error(error_code::EXCEPTION_OCCURED, e.what());
    }
    catch (fc::exception& e)
    {
        signal_error(error_code::EXCEPTION_OCCURED, e.to_string());
    }

    return std::result_of_t<Callable()>();
}

//
// mobit network api
//
void client_protocol::set_operation_timeout(uint64_t timeout_in_seconds)
{
    return safe_execute([&,this] {
        pclient->get_network().set_operation_timeout(timeout_in_seconds);
    });
}

void client_protocol::set_initial_peers(const std::vector<std::string>& peers)
{
    return safe_execute([&,this] {
        pclient->get_network().set_initial_peers(peers);
    });
}

void client_protocol::add_initial_peer(const std::string& address)
{
    return safe_execute([&,this] {
        pclient->get_network().add_initial_peer(address);
    });
}

void client_protocol::discover_peers()
{
    return safe_execute([this] {
        pclient->get_network().discover_peers();
    });
}

std::vector<std::string> client_protocol::get_potential_addresses()
{
    return safe_execute([this] {
        return pclient->get_network().get_potential_peers();
    });
}

std::vector<std::string> client_protocol::get_blacklisted_addresses()
{
    return safe_execute([this] {
        return pclient->get_network().get_blacklisted_peers();
    });
}

std::vector<std::string> client_protocol::get_inaccessible_peers()
{
    return safe_execute([this]{
        return pclient->get_network().get_inaccessible_peers();
    });
}

std::vector<std::string> client_protocol::get_unsynchronized_peers()
{
    return safe_execute([this]{
        return pclient->get_network().get_unsynchronized_peers();
    });
}

//
// mobit api
//
void client_protocol::set_validation_level(std::uint16_t n)
{
    pclient->set_validation_level(n);
}

void client_protocol::load_from_buffer(const char* buffer, size_t n)
{
    safe_execute([&, this] {
        pclient->set_block_headers(fc::raw::unpack<client::block_headers_t>(buffer, n));
    });
}

size_t client_protocol::save_to_buffer(char* buffer, size_t n)
{
    return safe_execute([&, this] {
        std::vector<char> packed = fc::raw::pack(pclient->get_block_headers());

        if (n == 0)
        {
            return packed.size();
        }
        else
        {
            const std::size_t bytes_to_copy = std::min(n, packed.size());
            std::copy(packed.begin(), packed.begin() + bytes_to_copy, buffer);

            return bytes_to_copy;
        }
    });
}

std::string client_protocol::get_key(const std::string& account_name)
{
    return safe_execute([&, this] {
        pclient->connect();
    
        cv::expiring_key pk = get_account_pk(account_name).key;
        return pk.key;
    });
}

std::string client_protocol::get_expiration(const std::string& account_name)
{
    return safe_execute([&, this] {
        pclient->connect();

        cv::expiring_key pk = get_account_pk(account_name).key;
        return pk.expiration.to_iso_string();
    });
}

void client_protocol::subscribe_account(const std::string& account_name, const std::string& key,
                           const long subscription_pack_id, const std::string& amount,
                           const std::string& asset_symbol, const std::string& wif_key)
{
    safe_execute([&, this] {
        pclient->connect();

        auto asset = pclient->lookup_asset_symbols({asset_symbol}).front();

        if (!asset)
        {
            signal_error(error_code::INVALID_PARAM);
            return;
        }

        auto account = pclient->get_account_by_name(account_name);

        if (!account)
        {
            signal_error(error_code::INVALID_PARAM);
            return;
        }

        pk_update_operation op;
        op.account = account->id;
        op.key = key;
        op.amount = cv::impl::core_amount_from_string(*asset, amount);
        op.expiration = cv::impl::calc_pk_expiration(*pclient,
                                                     account->id, 
                                                     op.amount,
                                                     subscription_pack_id);

        // leave extensions.value.subscription_pack empty if default value used
        // this code can be executed before hardfork 346
        // old nodes will reject request due to data serialization issues
        if (cv::default_subscription_pack_id != subscription_pack_id)
        {
            op.extensions.value.subscription_pack = subscription_pack_id;
        }

        signed_transaction tx;
        tx.operations.push_back(op);

        auto fee_schedule = pclient->get_global_properties().parameters.current_fees;
        set_operation_fees(tx, *fee_schedule);

        tx.validate();
        sign_transaction(tx, wif_key);        
    });
}

void client_protocol::update_pk(const std::string& account_name, const std::string& key,
        const std::string& amount, const std::string& asset_symbol, const std::string& wif_key)
{
    subscribe_account(account_name, key, cv::default_subscription_pack_id,
                      amount, asset_symbol, wif_key);
}

std::string client_protocol::get_key_unsecure(const std::string& account_name)
{
    return safe_execute([&, this] {
        pclient->connect();

        cv::expiring_key pk = get_account_pk_unsecure(account_name).key;

        return pk.key;
    });
}

std::string client_protocol::get_expiration_unsecure(const std::string& account_name)
{
    return safe_execute([&, this] {
        pclient->connect();

        cv::expiring_key pk = get_account_pk_unsecure(account_name).key;

        return pk.expiration.to_iso_string();
    });
}

void client_protocol::synchronize_block_headers(std::uint32_t first_block, std::uint32_t last_block)
{
    safe_execute([&, this] {
        pclient->connect();
        pclient->synchronize_block_headers(first_block, last_block);
    });
}

void client_protocol::synchronize_all_block_headers()
{
    safe_execute([this] {
        pclient->connect();
        pclient->synchronize_all_block_headers();
    });
}

uint32_t client_protocol::get_number_head_block()
{
    return safe_execute([this] {
        pclient->connect();

        return pclient->get_head_block_number();
    });
}

void client_protocol::register_account(const std::string& account_name, 
                                       public_key_type account_public_key,
                                       const std::string& registrar, 
                                       const std::string& registrar_private_key)
{
    safe_execute([&, this] {
        pclient->connect();

        auto registrar_account = pclient->get_account_by_name(registrar);
        if (!registrar_account)
        {
            signal_error(error_code::INVALID_PARAM);
            return;
        }

        account_create_operation op;
        op.registrar = registrar_account->id;
        op.name = account_name;
        op.owner = authority(1, account_public_key, 1);
        op.active = authority(1, account_public_key, 1);
        op.options.memo_key = account_public_key;

        signed_transaction tx;
        tx.operations.push_back(op);

        auto fee_schedule = pclient->get_global_properties().parameters.current_fees;
        set_operation_fees(tx, *fee_schedule);

        tx.validate();
        
        sign_transaction(tx, registrar_private_key);
    });
}

void client_protocol::register_account_with_registrar_server(
        const std::string& registrar_server_address,
        const std::string& account_name,
        const std::string& account_public_key)
{
    safe_execute([&] {
        fc::rpc::request req = { {}, "register_account", { account_name, account_public_key } };

        websocket_session<websocket> socket;
        socket.connect(registrar_server_address);
        socket.write(fc::json::to_string(req));
    });
}

void client_protocol::transfer(std::string from, std::string to, std::string amount, std::string wif_key)
{
    safe_execute([&, this] {
        pclient->connect();

        auto assets = pclient->list_assets(GRAPHENE_SYMBOL, 1);
        if (assets.empty())
        {
            signal_error(error_code::INTERNAL_ERROR);
            return;
        }

        auto from_account = pclient->get_account_by_name(from);
        auto to_account = pclient->get_account_by_name(to);

        if (!from_account || !to_account)
        {
            signal_error(error_code::INVALID_PARAM);
            return;
        }

        transfer_operation op;
        op.from = from_account->id;
        op.to = to_account->id;
        op.amount = assets[0].amount_from_string(amount);

        signed_transaction tx;
        tx.operations.push_back(op);

        auto fee_schedule = pclient->get_global_properties().parameters.current_fees;
        set_operation_fees(tx, *fee_schedule);

        tx.validate();
        sign_transaction(tx, wif_key);
    });
}

brain_key_info client_protocol::suggest_brain_key() 
{
    return safe_execute([] 
    {
        return graphene::wallet::suggest_brain_key();
    });
}

fc::variant client_protocol::version() const
{
    return graphene::wallet::about();
}

void client_protocol::set_chain_id(chain_id_type chain_id)
{
    safe_execute([&, this]
    {
        pclient->get_network().set_chain_id(chain_id);
    });
}

chain_id_type client_protocol::get_chain_id()
{
    return safe_execute([this] 
    {
        return pclient->get_network().get_chain_id();
    });
}

balance client_protocol::get_balance(const std::string& account_name)
{
    return safe_execute([&, this] 
    {
        pclient->connect();

        auto account_id = get_verified_account_id(account_name);
        if (account_id == account_id_type())
        {
            return balance{};
        
        }

        auto core = graphene::chain::asset_id_type();
        auto balances = pclient->get_account_balances(account_id, {core});

        return balances.empty() ? balance{} : to_balance(balances.begin()->amount.value);
    });
}

balance client_protocol::get_total_supply()
{
    return safe_execute([&, this] 
    {
        pclient->connect();
        auto id = graphene::chain::asset_id_type{};
        auto objs = pclient->get_objects({id});

        FC_ASSERT(!objs.empty(), "Cannot find asset");
            
        auto asset = objs.front().as<graphene::chain::asset_object>();
        return to_balance(asset.options.max_supply.value);
    });
}

balance client_protocol::get_current_supply()
{
    return safe_execute([&, this] 
    {
        pclient->connect();
        auto id = graphene::chain::asset_dynamic_data_id_type{};
        auto objs = pclient->get_objects({id});

        FC_ASSERT(!objs.empty(), "Cannot find dynamic asset");
            
        auto data = objs.front().as<graphene::chain::asset_dynamic_data_object>();
        return to_balance(data.current_supply.value);
    });
}

std::string client_protocol::get_global_properties()
{
    return safe_execute([&, this] 
    {
        pclient->connect();

        auto prop = pclient->get_global_properties();

        return fc::json::to_string(prop);
    });
}

key_info client_protocol::get_key_info(const std::string& account_name)
{
    return safe_execute([&, this] 
    {
        pclient->connect();
        auto info = get_account_pk(account_name);

        if (!get_last_error())
        {
            auto block = pclient->get_synchronized_block(info.block_num);

            auto id = block->witness;
            auto objs = pclient->get_objects({id});

            FC_ASSERT(!objs.empty(), "Cannot find witness");

            auto witness = objs.front().as<graphene::chain::witness_object>(); 
            auto account_id = witness.witness_account;

            auto accounts = pclient->get_accounts({account_id});

            FC_ASSERT(!accounts.empty() && accounts.front().valid(), "Cannot find account");

            auto witness_name = accounts.front()->name;
            auto timestamp = block->timestamp.to_iso_string();

            return key_info{info.block_num, info.trx_num, witness_name, timestamp};
        }

        return key_info{};
    });
}

std::string client_protocol::get_account_history(std::string account_name)
{
    return safe_execute([&, this] 
    {
        pclient->connect();
        auto account = pclient->get_account_by_name(account_name);

        if (account)
        {
            auto history = pclient->get_account_history(account->id);
            return fc::json::to_string(history);
        }
    
        signal_error(error_code::ACCOUNT_NOT_REGISTERED);
        return std::string{};
    });
}


} // namespace light


// Define C functions for the C++ class - as ctypes can only talk to C...
// smoke tests via python
#define MOBIT_BUFSIZE 64
char mobit_buff[MOBIT_BUFSIZE] = "";
extern "C"
{
    light::client_protocol* client_new() { return new light::client_protocol(); }
    const char* get_key(light::client_protocol* client, const char* account) {
        snprintf(mobit_buff, MOBIT_BUFSIZE, "%s", client->get_key(account).c_str());
        return mobit_buff;
    }
    const char* get_exp(light::client_protocol* client, const char* account) {
        snprintf(mobit_buff, MOBIT_BUFSIZE, "%s", client->get_expiration(account).c_str());
        return mobit_buff;
    }
    void update_pk(light::client_protocol* client, const char* account_name,  const char* key,
                   const char* amount,  const char* asset_symbol,  const char* wif_key)
    {
        client->update_pk(account_name, key, amount, asset_symbol, wif_key);
    }
}
