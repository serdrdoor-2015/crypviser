Crypviser Core
==============

# Interface of lightweght client

## Build

library - build_folder/mobile/mobit/libmobit_static.a
include file - build_folder/mobile/mobit/include/mobit/mobit hpp

## Configuration

run crypviser testnet (default witness_node address - localhost:11010)


## Namespace
namespace light {
    class client_protocol
    {
        .
        .
        .
        //Public API
        std::string get_key(const std::string& account_name);
        std::string get_expiration(const std::string& account_name);
 
        void update_pk(const std::string& account_name, const std::string& key,
                       const std::string& amount, const std::string& asset_symbol,
                       const std::string& wif_key);
 
        void set_remote_endpoint(const std::string& address);
        void set_remote_endpoint(const std::string& ip, uint port);
 
        int get_last_error() const { return error; };
        .
        .
        .
    };
}

## Methods:

    get_key
    -------

    Parameters:
    	std::string account_id //testnet example "nathan"

    Return value:
    	std::string public_key //testnet example "1234567"

     

    get_expiration
    --------------

    Parameters:
    	std::string& account_id //testnet example "nathan"

    Return value:
    	std::string public_key_expiration //testnet example "2017-12-06T20:59:12"

     
    update_pk
    ---------

    Parameters:
    	std::string account_name  //testnet example "nathan"
    	std::string key           //testnet example "12345"
    	std::string amount        //testnet example "2"
    	std::string asset_symbol  //testnet example "CVT"
    	std::string wif_key       //testnet example "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"

    Return value:
    	void

     
    set_remote_endpoint
    -------------------

    Parameters:
    	std::string address   //testnet example "ws://127.0.0.1:11010"

    Return value:
    	void

     
    set_remote_endpoint
    -------------------

    Parameters:
    	const std::string ip   //testnet example "localhost" or "127.0.0.1"
    	uint port              //testnet example "11010"

    Return value:
    	void

     
    get_last_error
    --------------

    Parameters:
    	void

    Return value:
    	int error
