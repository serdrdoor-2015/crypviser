Assuming you have the testnet running (use the docker container for that, or make testnet if you're building source code yourself), you can issue the following RPC requests to emulate the Crypviser workflow

#### Register a new account 
```
curl --data '{"jsonrpc": "2.0", "method": "register_account", "params": ["new-account-name", "CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV", "CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV", "nathan", true], "id": 1}' http://localhost:8092/rpc
```
Where:
* new-account-name is a unique account name. 
* CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV - its blockchain public key (not a Crypviser public key!). 
* CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV - its blockchain master public key (AKA owner key). 
* nathan is a registrar account (the one paying for the resigration, nathan is pre-defined in the genesis). 

Note, you're not allow to re-use a name, all names should be unique, otherwise you'll get an error message

#### Check the registered account 
```
curl --data '{"jsonrpc": "2.0", "method": "get_account", "params": ["new-account-name"], "id": 1}' http://localhost:8092/rpc
```

#### Import account private key 
In order to make a blockchain transaction on behalf of the newly created user you need to import its private key. This cannot be considered secure, nor should it ever be used in production
```
curl --data '{"jsonrpc": "2.0", "method": "import_key", "params": ["new-account-name", "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"], "id": 1}' http://localhost:8092/rpc

```

#### Check Crypviser key
By Crypviser key, we mean the very key used for traffic encryption (or rather key exchange), in any event this is not a blockchain key.
```
curl --data '{"jsonrpc": "2.0", "method": "get_account_pk", "params": ["new-account-name"], "id": 1}' http://localhost:8092/rpc
```
Since we've just registered our account, you should get an empty string in the response

#### Register Crypviser key
In order to register a new Crypviser key (the one used for traffic encryption) you first need to obtain some CVT. We have a special account which serves as a bank: nathan. Just make nathan send some tokens your way
```
curl --data '{"jsonrpc": "2.0", "method": "transfer", "params": ["nathan", "new-account-name", 300000, "CVT", "", true], "id": 1}' http://localhost:8092/rpc
```
Now, finally you can register your key:
```
curl --data '{"jsonrpc": "2.0", "method": "update_account_pk", "params": ["new-account-name", "123456789abcdefghijklmnopq", "100000", "CVT", ""], "id": 1}' http://localhost:8092/rpc
```
123456789abcdefghijklmnopq is the key. Currently, it's just an unlimited string with no limit, basically you can store any data you may need. Keep in mind, that the key must be unique, you're not allowed to re-use a once-met key twice.

Check the newly uploaded key:
```
curl --data '{"jsonrpc": "2.0", "method": "get_account_pk", "params": ["new-account-name"], "id": 1}' http://localhost:8092/rpc
```



