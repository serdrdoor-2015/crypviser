Crypviser Core
==============
 
# Build Crypviser with Apple's toolchain

## Installing package manager for macOS (Brew)
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

 
## Installing xcode
xcode-select --install

## after install run command
sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer

 
## installing dependencies
brew install cmake git libtool
 

## Build & Install OpenSSL 1.0 from source code
## configure openSSL 1.0
OPENSSL10_HOME=$HOME/opt/openssl10
wget https://www.openssl.org/source/openssl-1.0.2l.tar.gz
tar zxvf openssl-1.0.2l.tar.gz
cd openssl-1.0.2l
./Configure darwin64-x86_64-cc --prefix="$OPENSSL10_HOME"
make all
make install

 
## Download, build and install the latest Boost
BOOST_ROOT=/opt/boost_1_65_1
wget -c 'http://sourceforge.net/projects/boost/files/boost/1.65.1/boost_1_65_1.tar.bz2/download' -O boost_1_65_1.tar.bz2
[ $( shasum -a256 boost_1_65_1.tar.bz2 | cut -d ' ' -f 1 ) == "9807a5d16566c57fd74fb522764e0b134a8bbe6b6e8967b83afefd30dcd3be81" ] || ( echo 'Corrupt download' ; exit 1 )
tar xjf boost_1_65_1.tar.bz2
cd boost_1_65_1/
./bootstrap.sh "--prefix=$BOOST_ROOT"
sudo ./b2 install


## Get the source code and generate xcode project
git clone https://bitbucket.aeteh.com/scm/cv/crypviser.git
cd crypviser
git submodule sync --recursive
git submodule update --init --recursive
mkdir build && cd build
cmake -DBOOST_ROOT="$BOOST_ROOT" \
      -DOPENSSL_INCLUDE_DIR="$OPENSSL10_HOME/include" \
      -DOPENSSL_SSL_LIBRARY="$OPENSSL10_HOME/lib/libssl.a" \
      -DOPENSSL_CRYPTO_LIBRARY="$OPENSSL10_HOME/lib/libcrypto.a" \
      -DCMAKE_CXX_COMPILER=/usr/bin/clang++ \
      -G Xcode ..

The project file is crypviser/build/Crypviser.xcodeproj. Open it via xCode and enjoy!))
