#!/usr/bin/env python3

import argparse
import sys
import logging
import logging.handlers
import json
import fractions
import requests
import urllib.parse


def create_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("host",
                        help="wallet's host")
    parser.add_argument("publisher",
                        help="publisher account")
    parser.add_argument("--logfile",
                        default="/tmp/feed_provider.log",
                        help="file to store logs")
    return parser


def init_logger(level, filename):
    sys.excepthook = handle_exception

    handler = logging.handlers.RotatingFileHandler(filename, maxBytes=1000000, backupCount=1)

    formatter = logging.Formatter("%(levelname)s %(asctime)s - %(message)s")
    handler.setFormatter(formatter)

    lg = logging.getLogger(__name__)
    lg.setLevel(level)
    lg.addHandler(handler)


def logger():
    return logging.getLogger(__name__)


def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger().error("uncaught exception: ", exc_info=(exc_type, exc_value, exc_traceback))


def get_eur_price(currency_id):
    base = "https://api.coinmarketcap.com/v1/ticker/"
    url = urllib.parse.urljoin(base, currency_id)

    params = {"convert": "EUR"}
    response = requests.get(url=url, params=params)

    logger().debug("got response from coinmarketcap: %s" % response.text)

    data = response.json()
    return fractions.Fraction(data[0]["price_eur"])


def update_eur_price_feed(host, publisher, price):
    cvt_id = "1.3.0"
    eur_id = "1.3.2"

    price = {
        "base": {"amount": price.numerator, "asset_id": eur_id},
        "quote": {"amount": price.denominator, "asset_id": cvt_id}
    }

    params = [
        publisher,
        eur_id,
        {"settlement_price": price, "core_exchange_rate": price},
        True
    ]

    response = requests.post(host, json={
        "jsonrpc": "2.0",
        "method": "publish_asset_feed",
        "params": params,
        "id": 1
    })

    data = response.json()
    response.close()

    if "error" in data:
        logger().error(data["error"])
    else:
        logger().debug("submitted transaction to: %s" % json.dumps(data["result"], indent=4))


def main():
    args = create_arg_parser().parse_args()
    init_logger(logging.DEBUG, args.logfile)

    logger().debug("'%s' has been started with the arguments %s" % (sys.argv[0], sys.argv[1:]))

    price_eur = get_eur_price("CVCOIN")

    precision = 10 ** 9
    update_eur_price_feed(
        args.host,
        args.publisher,
        price_eur.limit_denominator(precision))


if __name__ == "__main__":
    main()
