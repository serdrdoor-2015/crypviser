#! /bin/bash
set -eu

function send_request()
{
    local host=$1
    local method=$2
    local params=$(IFS=, ; echo "${*:3}")

    local rpc_request="{\"jsonrpc\":\"2.0\",\"method\":\"$method\",\"params\":[$params],\"id\":1}"
    local response=$(curl -sS --data "$rpc_request" $host/rpc)

    if [[ "$response" =~ .+\"error\"[[:space:]]*:.+ ]]; then
        >&2 echo "$response"
        return 1
    fi

    echo $response
}

function chain_id()
{
    send_request ${witness_host/ws/http} get_chain_properties null | sed 's/.*"chain_id":"\([[:alnum:]]*\)".*/\1/'
}

function run_wallet()
{
    local chainid=$(chain_id)

    ./bin/cli_wallet --chain-id=$chainid --server-rpc-endpoint=$witness_host --rpc-http-endpoint=$wallet_ip:$wallet_port --daemon &> /dev/null &

    wallet_pid=$!

    until fuser -n tcp $wallet_port | grep -q $wallet_pid; do
        if [ ! -e /proc/$wallet_pid ]; then
            return 1
        fi
        sleep 1
    done
}

function unlock_wallet()
{
    local password=1
    send_request "http://$wallet_ip:$wallet_port" set_password \"$password\"
    send_request "http://$wallet_ip:$wallet_port" unlock \"$password\"
}

function import_private_key()
{
    send_request "http://$wallet_ip:$wallet_port" import_key \"$publisher\" \"$publisher_private_key\"
}

function set_up_provider_cron_job()
{
    service cron start

    local provider="$PWD/feed_provider.py"
    local cron_job="*/$update_interval * * * * $provider http://$wallet_ip:$wallet_port $publisher"

    echo "$cron_job" | crontab -
}

config_file=$1
source $config_file

wallet_ip=127.0.0.1
wallet_port=13658

run_wallet
unlock_wallet
import_private_key
set_up_provider_cron_job

wait $wallet_pid

