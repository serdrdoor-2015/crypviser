### Attention

Before build, you must copy binary files(witness_node, cli_wallet) to the current folder


### Docker commands for working with the crypviser-private-box container
List of commands:
  * `$ make help`

Build image crypviser-private-box:
  * `$ make container`

Save docker image to a tar archive:
  * `$ make save`

Load docker image from file:
  * `$ make load`

Run crypviser-private-box container:
  * `$ make run`

Start already stopped crypviser-private-box container:
  * `$ make start`

Attach to crypviser-private-box container:
  * `$ make attach`

Stop crypviser-private-box container:
  * `$ make stop`

Remove crypviser-private-box container:
  * `$ make clean`


