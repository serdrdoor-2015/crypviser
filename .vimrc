" ale C++ opts
let cpp_flags  = '-std=c++14 -Wall -I/opt/boost_1_66_0/include -Ilibraries/app/include -Ilibraries/chain/include -Ilibraries/db/include -Ilibraries/net/include -Ilibraries/fc/include -Ilibraries/utilities/include -Ilibraries/wallet/include -Ilibraries/plugins/account_history/include -Ilibraries/debug_witness/include -Ilibraries/delayed_node/include -Ilibraries/witness/include'

let g:ale_cpp_clang_options = cpp_flags
let g:ale_cpp_gcc_options = cpp_flags

set path+=libraries/*/include
