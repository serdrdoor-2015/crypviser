#!/bin/sh

dir=$(dirname "$2")
mkdir -p $dir
cat "$1"/* > "$2"

echo "\n\nconst std::vector<graphene::app::hardfork_description> hardforks = {" >> "$2"

for file in "$1"/*; do
  DEFINE_STR=$(grep -oP '#define.*' $file)
  [ -z "$DEFINE_STR" ] && continue
  COMMENT=$(cat $file | grep -oP '//.*' | sed -e 's/\/\/*//g' | sed ':a;N;$!ba;s/\n/ /g')
  NAME=$(echo $DEFINE_STR | grep -oP 'HARDFORK[^[:blank:]]*')
  TIME=$(echo $DEFINE_STR | grep -oP '\d+\D+\K\d+')

  echo "   {\"$COMMENT\"", "\"$NAME\"", "fc::time_point_sec($TIME).to_iso_string()}," >> "$2"
done

echo "};" >> "$2"
