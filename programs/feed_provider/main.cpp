#include <chrono>
#include <iostream>
#include <random>
#include <thread>

#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <fc/rpc/websocket_api.hpp>

#include <graphene/app/cli.hpp>
#include <graphene/app/plugin.hpp>
#include <graphene/wallet/wallet.hpp>
#include <graphene/utilities/config_helper.hpp>

struct config
{
    std::chrono::minutes update_interval;
    std::string witness_host;
    std::string publisher;
    std::string publisher_private_key;
};

config parse_config(const std::string& config_path);
void update_price_feed_loop(const config& cfg);

int main(int argc, char** argv)
{
    try
    {
        namespace po = boost::program_options;

        po::options_description opts("Feed provider");
        opts.add_options()
            ("help,h", "Print this help message and exit.")
            ("config,c", po::value<std::string>()->default_value("config.ini"), "Configuration file path.")
            ("encrypt-private-key", "Encrypt private key in interactive mode.");

        po::variables_map options;
        po::store(po::parse_command_line(argc, argv, opts), options);

        if (options.count("help"))
        {
            std::cout << opts << '\n';
            return EXIT_SUCCESS;
        }

        const std::string config_path = (options.at("config").as<std::string>());

        if (options.count("encrypt-private-key"))
        {
            const auto salt = graphene::config_helper::get_salt(fc::path(config_path).parent_path());
            const auto key_pair = graphene::app::cli::encrypt_private_key_interactively(salt);
            graphene::config_helper::save_extra_option(config_path, "private-key", fc::json::to_string(key_pair));
            return EXIT_SUCCESS;
        }

        const config cfg = parse_config(config_path);
        update_price_feed_loop(cfg);
    }
    catch (const fc::exception& e)
    {
        std::cout << e.to_detail_string() << '\n';
        return EXIT_FAILURE;
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

config parse_config(const std::string& config_path)
{
    namespace po = boost::program_options;
	namespace app = graphene::app;

    po::options_description cfg_options;
    cfg_options.add_options()
          ("update-interval", po::value<std::int64_t>()->required())
          ("witness-host", po::value<std::string>()->required())
          ("publisher", po::value<std::string>()->required())
          ("private-key", po::value<std::vector<std::string>>()->required());

    po::variables_map options;
    po::store(po::parse_config_file<char>(config_path.c_str(), cfg_options, false), options);

    config cfg;
    cfg.update_interval = std::chrono::minutes(options.at("update-interval").as<std::int64_t>());
    cfg.witness_host = options.at("witness-host").as<std::string>();
    cfg.publisher = options.at("publisher").as<std::string>();

    const auto config_keys = options.at("private-key").as<std::vector<std::string>>();
    const auto salt = graphene::config_helper::get_salt(fc::path(config_path).parent_path());    
	const auto private_keys = app::cli::decrypt_private_keys_interactively(salt, config_keys);
	const auto key_pair = app::dejsonify<std::pair<std::string, std::string>>(private_keys[0]);
    cfg.publisher_private_key = key_pair.second;

    return cfg;
}

std::string request(const std::string& host, const std::string& service, const std::string& target)
{
    namespace asio = boost::asio;
    namespace http = boost::beast::http;

    asio::io_context io_ctx;

    asio::ip::tcp::resolver resolver(io_ctx);
    const auto results = resolver.resolve(host, service);

    asio::ssl::context ctx(asio::ssl::context::sslv23_client);
    asio::ssl::stream<asio::ip::tcp::socket> stream(io_ctx, ctx);
    asio::connect(stream.next_layer(), results.begin(), results.end());
    stream.handshake(asio::ssl::stream_base::client);

    http::request<http::empty_body> req(http::verb::get, target, 11);
    req.set(http::field::host, host);
    http::write(stream, req);

    boost::beast::flat_buffer buffer;
    http::response<http::string_body> res;
    http::read(stream, buffer, res);

    boost::system::error_code ec;
    stream.shutdown(ec);

    return res.body();
}

std::string parse_coinmarketcap_response(const std::string& response)
{
    std::istringstream stream(response);

    boost::property_tree::ptree pt;
    boost::property_tree::read_json(stream, pt);

    return pt.front().second.get<std::string>("price_eur");
}

boost::multiprecision::cpp_rational get_coinmarketcap_eur_price(const std::string& currency)
{
    std::string base = "api.coinmarketcap.com";
    std::string target = "/v1/ticker/" + currency + "/?convert=EUR";
    std::string service = "443";

    std::string response = request(base, service, target);
    std::string str_price = parse_coinmarketcap_response(response);

    boost::multiprecision::cpp_dec_float_50 float_price(str_price);
    return static_cast<boost::multiprecision::cpp_rational>(float_price);
}

std::string generate_password(std::size_t length)
{
    std::string chars(
        "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "1234567890"
        "!@#$%^&*()"
        "`~-_=+[{]{\\|;:'\",<.>/?");

    std::random_device rd;
    std::uniform_int_distribution<std::size_t> distr(0, chars.size() - 1);

    std::string password(length, 'a');
    std::generate(password.begin(), password.end(), [&](){ return chars[distr(rd)]; });
    return password;
}

void publish_eur_cvt_feed(graphene::wallet::wallet_api& wapi, const std::string& publisher)
{
    const graphene::chain::asset_id_type cvt_id(0);
    const graphene::chain::asset_id_type eur_id(2);

    const auto coinmarketcap_price = get_coinmarketcap_eur_price("CVCOIN");

    graphene::chain::asset base(numerator(coinmarketcap_price), eur_id);
    graphene::chain::asset quote(denominator(coinmarketcap_price), cvt_id);

    graphene::chain::price price(base, quote);
    graphene::chain::price_feed feed = {price, price};

    wapi.publish_asset_feed(publisher, "EUR", feed, true);
}

void update_price_feed(const config& cfg)
{
	graphene::wallet::wallet_data wdata;
    wdata.ws_server = cfg.witness_host;

    fc::http::websocket_client client;
    fc::http::websocket_connection_ptr con = client.connect(wdata.ws_server);
    auto apic = std::make_shared<fc::rpc::websocket_api_connection>(*con);

    fc::api<login_api> remote_api = apic->get_remote_api<login_api>(1);
    FC_ASSERT(remote_api->login(wdata.ws_user, wdata.ws_password));

    wdata.chain_id = remote_api->database()->get_chain_id();

    graphene::wallet::wallet_api wapi(wdata, remote_api);

    std::string password = generate_password(512);
    wapi.set_password(password);
    wapi.unlock(password);
    wapi.import_key(cfg.publisher, cfg.publisher_private_key);

	publish_eur_cvt_feed(wapi, cfg.publisher);
}

void update_price_feed_loop(const config& cfg)
{
    auto execution_time = std::chrono::system_clock::now();

    while (true)
    {
        try
        {
            update_price_feed(cfg);
        }
        catch (const fc::exception& e)
        {
            std::cout << e.to_detail_string() << '\n';
        }
        catch (const std::exception& e)
        {
            std::cout << e.what() << '\n';
        }

        execution_time += cfg.update_interval;
        std::this_thread::sleep_until(execution_time);
    }
}
