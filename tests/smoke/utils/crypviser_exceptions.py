class CrypviserBaseException(Exception):
    pass


class CrypviserRequestError(CrypviserBaseException):
    pass


class CrypviserStatusCodeError(CrypviserRequestError):
    def __init__(self, msg, *args, **kwargs):
        super(CrypviserStatusCodeError, self).__init__(msg, *args, **kwargs)
