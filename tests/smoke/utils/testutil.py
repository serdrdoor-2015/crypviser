import dateutil.parser as dt
import time
from datetime import datetime
from datetime import timedelta
import random
import string
from utils.py_logger import logger
from utils.constants import HARDFORK_346_TIME, HARDFORK_783_TIME


class TestUtils(object):
    def __init__(self, cli_wallet):
        self._cli_wallet = cli_wallet

    def get_subscription_pack(self, id):
        additional_parameters = self._cli_wallet.get_global_additional_params()
        if additional_parameters['subscription_packs']:
            for pack in additional_parameters['subscription_packs']:
                if pack['id'] == id:
                    result = pack
                    break
            else:
                logger.info('There is no "%s" pack' % id)
                result = None
        else:
            logger.info('There are no any packs in subscription_packs')
            result = None
        return result

    def get_min_cost(self, pack_id):
        pack = self.get_subscription_pack(pack_id)
        if pack is not None:
            return pack['min_cost']

    def get_next_maintenance_time(self):
        prop = self._cli_wallet.get_dynamic_global_properties()
        return dt.parse(prop["next_maintenance_time"])

    def get_current_supply(self):
        asset_data = self._cli_wallet.get_object('1.3.0')
        dynamic_asset_data_id = asset_data[0]['dynamic_asset_data_id']
        dynamic_asset_data = self._cli_wallet.get_object(dynamic_asset_data_id)
        current_supply = dynamic_asset_data[0]['current_supply']
        return current_supply

    def get_last_operation_id(self, account="init0"):
        account = self._cli_wallet.get_account(account)
        stats = self._cli_wallet.get_object(account["statistics"])
        most_recent_op = self._cli_wallet.get_object(stats[0]["most_recent_op"])
        return most_recent_op[0]["operation_id"]

    def get_last_operation(self, account):
        operation = self._cli_wallet.get_object(self.get_last_operation_id(account))
        return operation[0]

    def get_exchange_rate(self, asset):
        rate = self._cli_wallet.get_object(asset)[0]["options"]["core_exchange_rate"]

        #
        # rate is formatted like this
        #
        #     {"base": {"amount": 20, "asset_id": "1.3.1"},
        #     "quote": {"amount": 1,  "asset_id": "1.3.0"}}
        #

        base = int(rate["base"]["amount"])
        quote = int(rate["quote"]["amount"])

        if rate["base"]["asset_id"] == asset:
            return base / quote
        elif rate["quote"]["asset_id"] == asset:
            return quote / base

    def set_subscription_plan(self, plan):
        self._cli_wallet.transfer("nathan", "1.2.0", 1)
        self._cli_wallet.transfer("nathan", "init0", 30)
        self._cli_wallet.try_send_request("vote_for_committee_member",
                                          ["nathan", "init0", False, True])
        self._cli_wallet.send_request("vote_for_committee_member",
                                      ["nathan", "init0", True, True])

        prev_op = self.get_last_operation_id("init0")
        expiration = self.get_expiration_time(
            8 * self._cli_wallet.get_block_interval())

        new_params = self._cli_wallet.get_global_additional_params()
        new_params["subscription_plan"] = plan

        self._cli_wallet.propose_parameter_change(
            ["init0", expiration, {"additional_parameters": new_params}, True])

        self.wait_operation_processed("init0", prev_op)

        operation = self.get_last_operation("init0")
        proposal_id = operation["result"][1]

        self._cli_wallet.approve_proposal("init0", proposal_id)
        self.wait_proposal_processed(proposal_id)

        self._cli_wallet.send_request("vote_for_committee_member",
                                ["nathan", "init0", False, True])

        additional_parameters = self._cli_wallet.get_global_additional_params()
        assert additional_parameters["subscription_plan"] == plan

    def is_before_hf346(self):
        now = dt.parse(self._cli_wallet.get_dynamic_global_properties()["time"])
        return now < HARDFORK_346_TIME

    def is_before_hf783(self):
        now = dt.parse(self._cli_wallet.get_dynamic_global_properties()["time"])
        return now < HARDFORK_783_TIME

    def is_between_hf346_and_hf783(self):
        now = dt.parse(self._cli_wallet.get_dynamic_global_properties()["time"])
        return (HARDFORK_346_TIME <= now < HARDFORK_783_TIME)

    def get_expiration_time(self, lifetime_seconds):
        now = dt.parse(self._cli_wallet.get_dynamic_global_properties()["time"])
        expiration = now + timedelta(seconds=lifetime_seconds)
        expiration_iso = expiration.replace(microsecond=0).isoformat()
        return expiration_iso

    def wait_for_maintenance_after(self, timestamp):
        while True:
            prop = self._cli_wallet.get_dynamic_global_properties()
            now = dt.parse(prop["time"])
            next_maintenance_time = dt.parse(prop["next_maintenance_time"])

            if now < timestamp:
                time.sleep(1)
            else:
                if timestamp < next_maintenance_time:
                    timestamp = next_maintenance_time
                break
        self.wait_until(timestamp)

    def choose_new_min_cost(self, pack_id):
        current_min_cost = self.get_min_cost(pack_id)
        while True:
            new_min_cost = random.randint(1, 10)
            if new_min_cost != current_min_cost:
                return new_min_cost

    def create_account_with_balance(self, balance=10):
        account_name = generate_random_name()
        self._cli_wallet.register_account(account_name)
        self.wait_blocks(1)
        self._cli_wallet.transfer("nathan", account_name, balance)
        self.wait_blocks(1)
        return account_name

    def wait_until_maintenance_finished(self):
        maintenance_time = self.get_next_maintenance_time()
        while True:
            if maintenance_time == self.get_next_maintenance_time():
                time.sleep(1)
            else:
                break

    def wait_blocks(self, num_blocks=1):
        block = self._cli_wallet.get_head_block_number()
        while self._cli_wallet.get_head_block_number() - block < num_blocks:
            time.sleep(1)

    def wait_next_irreversible_block(self):
        currentBlock = self._cli_wallet.get_dynamic_global_properties()[
            "last_irreversible_block_num"]
        while currentBlock == self._cli_wallet.get_dynamic_global_properties()[
            "last_irreversible_block_num"]:
            time.sleep(1)

    def wait_until(self, timestamp):
        while timestamp > dt.parse(
                self._cli_wallet.get_dynamic_global_properties()["time"]):
            time.sleep(1)

    def wait_proposal_processed(self, proposal_id):
        expiration_time = dt.parse(
            self._cli_wallet.get_object(proposal_id)[0]["expiration_time"])
        next_maintenance_time = self.get_next_maintenance_time()

        if expiration_time > next_maintenance_time:
            self.wait_until(expiration_time)
            self.wait_until_maintenance_finished()
        else:
            self.wait_until_maintenance_finished()

        self.wait_blocks(1)

    def wait_operation_processed(self, account, operation_id):
        while operation_id == self.get_last_operation_id(account):
            time.sleep(1)

    def wait_transfer(self, from_account, to_account, amount):
        prev = self._cli_wallet.get_account_balance(to_account)
        self._cli_wallet.transfer(from_account, to_account, amount)

        while self._cli_wallet.get_account_balance(to_account) - prev < amount:
            time.sleep(1)

    def choose_new_block_interval(self):
        current = self._cli_wallet.get_block_interval()
        maint_interval = self._cli_wallet.get_maintenance_interval()

        for i in range(1, maint_interval):
            if 0 == maint_interval % i and i != current:
                return i

        assert False

    def wait_key_updated(self, account, key):
        while key != self._cli_wallet.get_account_pk(account)["key"]:
            time.sleep(1)


def get_timestamp():
    now = datetime.now()
    return str(int(now.strftime("%s")) * 1000)


def generate_random_string(len):
    gen = lambda n: ''.join(
        [random.choice(string.lowercase) for i in xrange(n)])
    return gen(len)


def generate_random_key():
    ts = get_timestamp()
    key = ts + generate_random_string(10)
    return key


def generate_random_name():
    ts = get_timestamp()
    name = generate_random_string(10) + ts
    return name


def prepare_subscription_pack(id, min_cost, price_per_month, name=''):
    pack = {
        'id': id,
        'min_cost': min_cost,
        'price_per_month': price_per_month,
        'name': name
    }
    return pack
