import dateutil.parser as dt
from utils.py_logger import logger
from utils.testutil import TestUtils
from copy import deepcopy


class Committee(object):
    def __init__(self, cli_wallet):
        self._cli_wallet = cli_wallet
        self._utils = TestUtils(self._cli_wallet)

    def increaseBalance(self, fromAccount, amount):
        self._cli_wallet.transfer(fromAccount, "committee-account", amount)

    def addMember(self, account, member):
        self._cli_wallet.try_send_request("vote_for_committee_member",
                                          [account, member, False, True])
        self._cli_wallet.send_request("vote_for_committee_member",
                                      [account, member, True, True])

    def deleteMember(self, account, member):
        self._cli_wallet.send_request("vote_for_committee_member",
                                      [account, member, False, True])

    def members(self):
        obj = self._cli_wallet.get_object("1.2.0")
        return obj[0]["active"]["account_auths"]

    def update_global_property(self, member1, member2, field_name, value):
        logger.info('Updating %s to "%s"...' % (field_name, value))
        expiration = self._utils.get_expiration_time(8 * self._cli_wallet.get_block_interval())

        self._cli_wallet.propose_parameter_change(
            [member1, expiration, {field_name: value}, True])

        self._utils.wait_blocks(1)

        operation = self._utils.get_last_operation(member1)

        proposal_id = operation["result"][1]

        self._cli_wallet.approve_proposal(member1, proposal_id)
        self._cli_wallet.approve_proposal(member2, proposal_id)

        self._utils.wait_for_maintenance_after(dt.parse(expiration))

    def check_committee_preconditions(self):
        # pre-condition 1: the committee member must be voted in (init0,
        # init1 and init3)
        # precondition 2: 1 < get_maintenance_interval()
        # precondition 3: the committee account should not have empty balance
        assert 3 == len(self.members())
        assert 1 < self._cli_wallet.get_maintenance_interval()

    def set_subscription_packs(self, *packs):
        current_params = self._cli_wallet.get_global_additional_params()
        new_params = deepcopy(current_params)
        print 'Given packs'
        print packs
        subscription_packs = [] if packs == (None,) else list(packs)
        new_params['subscription_packs'] = subscription_packs
        self.update_global_property("init0", "init1", "additional_parameters",
                                    new_params)

    def set_plan(self, *plans):
        current_params = self._cli_wallet.get_global_additional_params()
        new_params = deepcopy(current_params)
        subscription_plan = [] if plans == (None,) else list(plans)
        new_params['subscription_plan'] = subscription_plan
        self.update_global_property("init0", "init1", "additional_parameters",
                                    new_params)
