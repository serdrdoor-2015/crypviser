from datetime import datetime


REQ_ID = 0
PRIMARY_KEY = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"
DEFAULT_CORE_ASSET = 'CVT'

HARDFORK_346_TIME = datetime(2018, 4, 3, 0, 0)
HARDFORK_783_TIME = datetime(2019, 3, 14, 12, 0)
