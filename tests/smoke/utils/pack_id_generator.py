class PackIdGenerator(object):

    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(PackIdGenerator, cls).__new__(
                cls, *args, **kwargs)
            cls._instance._id_number = 0
        return cls._instance

    def increment(self):
        self._id_number += 1
        return self._id_number


def get_next_pack_id():
    id_generator = PackIdGenerator()
    id_number = id_generator.increment()
    return id_number
