from connection import JsonRpc
from constants import DEFAULT_CORE_ASSET
from utils.py_logger import logger


class CliWallet(object):
    def __init__(self, uri):
        self.rpc = JsonRpc(uri)

    def send_request(self, method, *arguments, **kwargs):
        args_param = arguments[0] if arguments else []
        cmd = 'curl --data \'{"jsonrpc": "2.0", "method": "%s", ' \
              '"params": %s, "id": 1}\' %s' % \
              (method, args_param, self.rpc.uri)
        logger.debug('Execute: %s' % cmd)
        response = self.rpc.send_request(method, *arguments, **kwargs)
        logger.debug('%s\n' % response)

        return response

    def try_send_request(self, method, *arguments, **kwargs):
        kwargs.update(expected_code=None)
        logger.debug('Try send "%s" request...' % method)
        result = self.send_request(method, *arguments, **kwargs)
        return result

    def get_account_balance(self, account):
        result = self.send_request("list_account_balances",
                                   [account])["result"]
        if 0 < len(result):
            balance = int(result[0]["amount"])
        else:
            balance = 0
        logger.info('Balance of %s: %s' % (account, balance))
        return balance

    def get_account(self, account):
        return self.send_request("get_account", [account])["result"]

    def list_assets(self):
        return self.send_request("list_assets", ['', '100'])["result"]

    def get_object(self, obj):
        return self.send_request("get_object", [obj])["result"]

    def get_dynamic_global_properties(self):
        return self.send_request("get_dynamic_global_properties")["result"]

    def get_head_block_number(self):
        return self.get_dynamic_global_properties()["head_block_number"]

    def get_global_properties(self):
        return self.send_request("get_global_properties")["result"]

    def get_global_additional_params(self):
        return self.get_global_properties()['parameters']['additional_parameters']

    def get_global_parameters(self):
        return self.get_global_properties()["parameters"]

    def get_subscription_packs(self):
        """Function works only after hardfork 783"""
        return self.get_global_additional_params()['subscription_packs']

    def get_subscription_plan(self):
        """Function works only between hardforks 346 and 783"""
        return self.get_global_additional_params()['subscription_plan']

    def get_block_interval(self):
        return self.get_global_parameters()["block_interval"]

    def get_maintenance_interval(self):
        return self.get_global_parameters()["maintenance_interval"]

    def propose_parameter_change(self, *args):
        self.send_request("propose_parameter_change", *args)

    def approve_proposal(self, account, proposal):
        args = [account, proposal, {"active_approvals_to_add": [account]},
                True]
        self.send_request("approve_proposal", args)

    def transfer(self, from_account, to_account, amount,
                 asset=DEFAULT_CORE_ASSET):
        response = self.send_request(
            "transfer", [from_account, to_account, amount, asset, "", True])
        return response

    def get_account_pk(self, account):
        return self.send_request("get_account_pk", [account])["result"]

    def update_account_pk(self, account, key, amount, currency):
        self.send_request("update_account_pk",
                          [account, key, amount, currency, ""])

    def try_update_account_pk(self, account, key, amount, currency):
        self.try_send_request("update_account_pk",
                              [account, key, amount, currency, ""])

    def subscribe_account(self, account, key, pack_id, amount, currency):
        self.send_request("subscribe_account",
                                   [account, key, pack_id, amount, currency,
                                    ""])

    def publish_asset_feed(self, account, asset, feed):
        self.send_request("publish_asset_feed",
                                   [account, asset, feed, True])

    def create_account_with_private_key(self, key, name, registrar):
        self.send_request("create_account_with_private_key",
                                   [key, name, registrar, True])

    def register_account(self, name):
        logger.info('Register new account: %s' % name)
        self.send_request("register_account",
                          [name,
                           "CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV",
                           "CV6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV",
                           "nathan", True])

    def import_key(self, key, account):
        self.send_request("import_key", [key, account])

    def list_witnesses(self):
        return self.send_request("list_witnesses", ["", 1000])[
            "result"]

    def get_witness(self, witness):
        return self.send_request("get_witness", [witness])["result"]

    def vote_for_witness(self, voting_account, witness):
        self.send_request("vote_for_witness",
                          [voting_account, witness, True, True])

    def try_vote_for_witness(self, voting_account, witness):
        self.try_send_request("vote_for_witness",
                              [voting_account, witness, True, True])

    def reserve_asset(self, account, amount, symbol='CVT'):
        response = self.send_request("reserve_asset",
                                     [account, amount, symbol, True])
        return response['result']

    def get_active_witnesses(self):
        result = self.get_global_properties()["active_witnesses"]
        return result


host_port = 'localhost', 8092
uri = 'http://%s:%s/' % host_port
CLI_WALLET = CliWallet(uri)
