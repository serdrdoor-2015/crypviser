import pytest
from utils.py_logger import logger, log_step
from utils.crypviser_exceptions import CrypviserStatusCodeError
from utils.testutil import generate_random_name
from conftest import get_test_utils


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
@pytest.mark.parametrize('account_balance', [30, 31])
def test_reserve_asset_and_check_balance(account_balance, cli_wallet, utils):
    amount_to_reserve = 10
    reserve_asset_fee = 20

    log_step('Create account with %s CVT balance' % account_balance)
    account = utils.create_account_with_balance(account_balance)

    log_step('Reserve %s CVT by account' % amount_to_reserve)
    cli_wallet.reserve_asset(account, amount_to_reserve)

    log_step('Check that account balance is correct')
    balance = cli_wallet.get_account_balance(account)

    assert balance == (account_balance - amount_to_reserve - reserve_asset_fee)*1e5


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
def test_reserve_asset_and_check_current_supply(cli_wallet, utils):
    # reserve_asset_fee == 20 CVT
    account_balance = 30
    amount_to_reserve = 10

    log_step('Create account with %s CVT balance' % account_balance)
    account = utils.create_account_with_balance(account_balance)

    log_step('Get current supply before')
    supply_before = utils.get_current_supply()

    log_step('Reserve %s CVT by account' % amount_to_reserve)
    cli_wallet.reserve_asset(account, amount_to_reserve)

    log_step('Check that current supply after is correct')
    supply_after = utils.get_current_supply()
    assert int(supply_before) - int(supply_after) == amount_to_reserve * 1e5


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
@pytest.mark.parametrize('account_balance', [1, 10, 29])
def test_try_to_reserve_asset_more_than_account_has(account_balance, cli_wallet, utils):
    # reserve_asset_fee == 20 CVT
    amount_to_reserve = 10

    log_step('Create account with %s CVT balance' % account_balance)
    account = utils.create_account_with_balance(account_balance)

    log_step('Try to reserve %s CVT by account' % amount_to_reserve)
    try:
        cli_wallet.reserve_asset(account, amount_to_reserve)
    except CrypviserStatusCodeError:
        logger.info('Expected error was got')


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
def test_try_to_reserve_asset_when_account_has_empty_balance(cli_wallet):
    # reserve_asset_fee == 20 CVT
    amount_to_reserve = 10

    log_step('Create account with 0 CVT balance')
    account_name = generate_random_name()
    cli_wallet.register_account(account_name)

    log_step('Try to reserve %s CVT by account' % amount_to_reserve)
    try:
        cli_wallet.reserve_asset(account_name, amount_to_reserve)
    except CrypviserStatusCodeError:
        logger.info('Expected error was got')
