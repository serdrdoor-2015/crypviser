import pytest
import dateutil.parser as dt
from datetime import timedelta
from copy import deepcopy
from utils.py_logger import logger, log_step
from utils.pack_id_generator import get_next_pack_id
from utils.crypviser_exceptions import (CrypviserBaseException,
                                        CrypviserStatusCodeError)
from utils.testutil import prepare_subscription_pack, generate_random_key
from conftest import get_test_utils


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
def test_subscribe_account_using_subscription_packs(committee, cli_wallet, utils):
    pack_id = get_next_pack_id()
    min_cost = 3
    price_per_month = 3 * 100000

    block_interval = cli_wallet.get_global_parameters()['block_interval']

    log_step('Set new subscription pack')
    new_pack = prepare_subscription_pack(pack_id, min_cost, price_per_month)
    committee.set_subscription_packs(new_pack)

    log_step('Subscribe account')
    acc = utils.create_account_with_balance()
    key = generate_random_key()
    cli_wallet.subscribe_account(acc, key, pack_id, 6, "CVT")
    log_step('Get current time for check if subscription period is correct')
    current_time = dt.parse(cli_wallet.get_dynamic_global_properties()['time'])

    utils.wait_key_updated(acc, key)

    log_step('Check that account is subscribed correctly')
    exp_key = cli_wallet.get_account_pk(acc)
    assert key == exp_key["key"]
    assert pack_id == exp_key["subscription_pack_id"]

    log_step('Check that subscription is 60 days (2 month)')
    expiration = dt.parse(exp_key['expiration'])
    subscription = expiration - current_time

    assert timedelta(days=60) - subscription <= timedelta(seconds=block_interval)


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
@pytest.mark.timeout(timeout=100)
def test_change_subscription_packs(committee, cli_wallet):
    committee.check_committee_preconditions()
    pack_id_1 = get_next_pack_id()
    pack_id_2 = get_next_pack_id()

    min_cost_1 = 3
    min_cost_2 = min_cost_1 * 2

    price_per_month_1 = 3 * 100000
    price_per_month_2 = price_per_month_1 * 2

    log_step('Add first pack')
    pack_1 = prepare_subscription_pack(pack_id_1, min_cost_1,
                                       price_per_month_1)
    committee.set_subscription_packs(pack_1)

    log_step('Modify subscription_packs. Add second pack')
    pack_2 = prepare_subscription_pack(pack_id_2, min_cost_2,
                                       price_per_month_2)
    committee.set_subscription_packs(pack_1, pack_2)

    current_packs = cli_wallet.get_subscription_packs()
    assert current_packs == [pack_1, pack_2]

    log_step('Modify subscription_packs. Update second pack')
    updated_pack_2 = prepare_subscription_pack(pack_id_2, min_cost_2,
                                               price_per_month_2*2)

    committee.set_subscription_packs(pack_1, updated_pack_2)

    current_packs = cli_wallet.get_subscription_packs()
    assert current_packs == [pack_1, updated_pack_2]

    log_step('Delete subscription packs')
    committee.set_subscription_packs(None)

    current_packs = cli_wallet.get_subscription_packs()
    assert current_packs == []


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
def test_set_subscription_pack_without_name(committee, cli_wallet):
    committee.check_committee_preconditions()
    pack_id = get_next_pack_id()

    log_step('Prepare subscription pack without name')
    pack_without_name = {
        'id': pack_id,
        'min_cost': 2,
        'price_per_month': 3 * 100000
    }

    log_step('Set subscription pack')
    committee.set_subscription_packs(pack_without_name)

    log_step('Check that pack is created correctly and has empty name field')
    pack_without_name['name'] = ''
    current_packs = cli_wallet.get_subscription_packs()
    assert current_packs == [pack_without_name]


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
def test_try_subscribe_account_using_subscription_packs_with_amount_less_than_min_cost(committee, cli_wallet, utils):
    pack_id = get_next_pack_id()
    min_cost = 3
    price_per_month = 3 * 100000

    log_step('Set new subscription pack')
    new_pack = prepare_subscription_pack(pack_id, min_cost, price_per_month)
    committee.set_subscription_packs(new_pack)

    acc = utils.create_account_with_balance()
    key = generate_random_key()
    try:
        logger.info('Try to subscribe account with amount less than min_cost')
        cli_wallet.subscribe_account(acc, key, pack_id, 2, "CVT")
        raise CrypviserBaseException()
    except CrypviserStatusCodeError:
        logger.info('Expected error was got')


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
@pytest.mark.timeout(timeout=100)
def test_set_package_min_cost(committee, utils):
    committee.check_committee_preconditions()
    pack_id = get_next_pack_id()
    min_cost = 2
    price_per_month = 3 * 100000

    log_step('Set new subscription pack')
    new_pack = prepare_subscription_pack(pack_id, min_cost, price_per_month)
    committee.set_subscription_packs(new_pack)

    log_step('Check that subscription pack min_cost is correct')
    assert min_cost == utils.get_min_cost(pack_id)


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
@pytest.mark.timeout(timeout=100)
def test_try_to_set_wrong_value_to_min_package_cost(committee, cli_wallet):
    committee.check_committee_preconditions()
    pack_id = get_next_pack_id()
    price_per_month = 3 * 100000
    cases = (-1, 0, 0.12, 'str_value')
    current_params = cli_wallet.get_global_additional_params()
    new_params = deepcopy(current_params)
    for case in cases:
        log_step('Prepare subscription pack with "%s" min_cost value' % case)
        new_pack = prepare_subscription_pack(pack_id, case,
                                             price_per_month)
        new_params['subscription_packs'] = [new_pack]
        try:
            log_step('Try to set subscription pack with "%s" min_cost' % case)
            committee.update_global_property("init0", "init1",
                                             "additional_parameters",
                                             new_params)
            raise CrypviserBaseException()
        except CrypviserStatusCodeError:
            logger.info('Expected error was got')


@pytest.mark.skipif(get_test_utils().is_before_hf783(),
                    reason="This test should pass after hardfork 783")
def test_max_subscription_cost_less_than_package_min_cost(committee, cli_wallet, utils):
    pack_id = get_next_pack_id()

    #  max_subscription_cost hardcoded as 5 CVT
    min_cost = 10
    price_per_month = 10 * 100000

    log_step('Prepare and set subscription_pack')
    new_pack = prepare_subscription_pack(pack_id, min_cost, price_per_month)
    committee.set_subscription_packs(new_pack)

    acc = utils.create_account_with_balance(15)
    key = generate_random_key()

    log_step('Subscribe account')
    cli_wallet.subscribe_account(acc, key, pack_id, 10, "CVT")

    log_step('Get current time for check if subscription period is correct')
    current_time = dt.parse(cli_wallet.get_dynamic_global_properties()['time'])

    utils.wait_key_updated(acc, key)

    log_step('Check that subscription is 60 days (2 month)')
    exp_key = cli_wallet.get_account_pk(acc)
    expiration = dt.parse(exp_key['expiration'])
    subscription = expiration - current_time

    assert subscription.days == 60
