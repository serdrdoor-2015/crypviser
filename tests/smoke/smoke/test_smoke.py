# dependencies: pip install pytest requests dateutil

import pytest
import dateutil.parser as dt
from utils.py_logger import log_step
from utils.pack_id_generator import get_next_pack_id
from utils.testutil import generate_random_name, generate_random_key
from conftest import get_test_utils
from copy import deepcopy


@pytest.mark.timeout(timeout=10)
def test_witness_node_request(witness_node):
    response = witness_node.send_request('get_chain_properties')
    assert response["result"]


@pytest.mark.timeout(timeout=10)
@pytest.mark.parametrize('params', [[["1.2.0"]], [["1.2.1"]]])
def test_witness_node_request_with_params(witness_node, params):
    response = witness_node.send_request('get_accounts', params)
    assert response["result"]


@pytest.mark.timeout(timeout=10)
@pytest.mark.parametrize('params', [["init1"], ["init2"], ["init3"], ["init4"], ["init5"], ["init6"]])
@pytest.mark.parametrize('method_name', ['list_account_balances'])
def test_cli_wallet_empty_balances(method_name, params, cli_wallet):
    response = cli_wallet.send_request(method_name, params)
    #check empty balance
    assert response["result"] != None


@pytest.mark.timeout(timeout=10)
@pytest.mark.parametrize('params', ["init1", "init2", "init3", "init4", "init5", "init6"])
@pytest.mark.parametrize('method_name', ['transfer'])
def test_cli_wallet_transfer(method_name, params, cli_wallet):
    prev_balance = cli_wallet.get_account_balance(params)

    sum = "1.234"
    response = cli_wallet.send_request(method_name, ["nathan", params, sum, "CVT", "memo", True])
    assert response["result"]

    transfered_amount = cli_wallet.get_account_balance(params) - prev_balance
    assert transfered_amount == int(float(sum) * 100000)


@pytest.mark.timeout(timeout=10)
def test_update_public_key(cli_wallet):
    accnt = generate_random_name()
    cli_wallet.register_account(accnt)
    cli_wallet.transfer("nathan", accnt, 100)

    key = generate_random_key()
    cli_wallet.update_account_pk(accnt, key, 20, "CVT")

    exp_key = cli_wallet.get_account_pk(accnt)
    assert key == exp_key["key"]


@pytest.mark.timeout(timeout=40)
def test_vote_for_committee_member(cli_wallet, utils):
    cli_wallet.transfer("nathan", "init0", "100000")
    cli_wallet.send_request("vote_for_committee_member", ["nathan", "init0", True, True])
    utils.wait_until_maintenance_finished()

    commitee = cli_wallet.get_object("1.2.0")
    assert 0 < len(commitee[0]["active"]["account_auths"])

    cli_wallet.send_request("vote_for_committee_member", ["nathan", "init0", False, True])
    utils.wait_until_maintenance_finished()
    

@pytest.mark.skipif(not get_test_utils().is_between_hf346_and_hf783(),
                    reason="This test should pass after hardfork 346 and before hardfork 783")
def test_change_block_interval(committee, cli_wallet, utils):
    committee.check_committee_preconditions()
    new_block_interval = utils.choose_new_block_interval()
    committee.update_global_property("init0", "init1", "block_interval",
                                     new_block_interval)
    assert new_block_interval == cli_wallet.get_block_interval()


@pytest.mark.skipif(not get_test_utils().is_between_hf346_and_hf783(),
                    reason="This test should pass after hardfork 346 and before hardfork 783")
def test_change_referral_percent(committee, cli_wallet):
    expected_value = cli_wallet.get_global_parameters()["referral_percent"]

    if len(expected_value):
        del expected_value[0]
    else:
        expected_value = [[0, 1]]

    committee.update_global_property("init0", "init1", "referral_percent",
                                     expected_value)
    newValue = cli_wallet.get_global_parameters()["referral_percent"]
    assert expected_value == newValue


# This test should pass after Thursday, April 12, 2018 6:07:00 AM
# HARDFORK_346_TIME
@pytest.mark.skipif(not get_test_utils().is_between_hf346_and_hf783(),
                    reason="This test should pass after hardfork 346 and before hardfork 783")
def test_additional_properties_change_subscription_plan(committee, cli_wallet):
    committee.check_committee_preconditions()
    plan_id_1 = get_next_pack_id()
    plan_id_2 = get_next_pack_id()

    price_per_month_1 = 2
    price_per_month_2 = 3

    plan_1 = [plan_id_1, price_per_month_1]
    plan_2 = [plan_id_2, price_per_month_2]

    log_step('Add first plan')
    committee.set_plan(plan_1)

    log_step('Modify subscription_plan. Add second plan')
    committee.set_plan(plan_1, plan_2)

    current_plan = cli_wallet.get_subscription_plan()
    assert current_plan == [plan_1, plan_2]

    log_step('Modify subscription_plan. Update second plan')
    updated_plan_2 = deepcopy(plan_2)
    updated_plan_2[1] = price_per_month_2 + 1

    committee.set_plan(plan_1, updated_plan_2)

    current_plan = cli_wallet.get_subscription_plan()
    assert current_plan == [plan_1, updated_plan_2]

    log_step('Delete subscription plan')
    committee.set_plan(plan_1)

    current_plan = cli_wallet.get_subscription_plan()
    assert current_plan == [plan_1]


@pytest.mark.timeout(timeout=10)
def test_create_usd_asset(cli_wallet):
    usd = cli_wallet.get_object("1.3.1")[0]
    assert "USD" == usd["symbol"]
    assert 0 == usd["options"]["max_supply"]


@pytest.mark.timeout(timeout=10)
def test_create_account(cli_wallet, utils):
    name = generate_random_name()
    key = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"

    cli_wallet.create_account_with_private_key(key, name, "nathan")
    utils.wait_blocks(1)
    account = cli_wallet.get_account(name)

    registrar_id = account["registrar"]
    registrar = cli_wallet.get_object(registrar_id)

    assert "nathan" == registrar[0]["name"]


@pytest.mark.timeout(timeout=10)
def test_create_eur_asset(cli_wallet):
    eur = cli_wallet.get_object("1.3.2")[0]
    assert "EUR" == eur["symbol"]
    assert 0 == eur["options"]["max_supply"]


@pytest.mark.timeout(timeout=10)
def test_spend_coins_when_updating_the_key(cli_wallet, utils):
    name = generate_random_name()
    account_key = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"
    cli_wallet.create_account_with_private_key(account_key, name, "nathan")
    cli_wallet.transfer("nathan", name, 30)
    utils.wait_blocks(1)

    key = generate_random_key()
    prev_balance = cli_wallet.get_account_balance(name)

    cli_wallet.update_account_pk(name, key, 5, "CVT")
    utils.wait_key_updated(name, key)

    new_balance = cli_wallet.get_account_balance(name)

    # FIXME: 
    # 700000 == 5 CVT (pk fee) + 2 CVT (tx fee)
    # there is another fee here: price_per_kbyte
    # it depends on the key length, and it's quite difficult to calculate
    assert 500000 < prev_balance - new_balance
    assert 700000 > prev_balance - new_balance


@pytest.mark.timeout(timeout=10)
def test_provide_price_feed(cli_wallet, utils):
    # pre-condition 1: the committee member must be voted in
    obj = cli_wallet.get_object("1.2.0")
    assert 0 < len(obj[0]["active"]["account_auths"])

    rate = {"base": {"amount": 20, "asset_id": "1.3.1"},
            "quote": {"amount": 1,"asset_id": "1.3.0"}}

    feed = { "core_exchange_rate": rate, "settlement_price": rate }

    cli_wallet.publish_asset_feed("init0", "USD", feed)
    utils.wait_blocks(1)

    assert rate == cli_wallet.get_object("1.3.1")[0]["options"]["core_exchange_rate"]


@pytest.mark.timeout(timeout=10)
def test_convert_usd_to_core_before_charging_and_charge_core(cli_wallet, utils):
    # pre-condition: price_feed should be uploaded
    # see test_provide_price_feed 
    rate = cli_wallet.get_object("1.3.1")[0]["options"]["core_exchange_rate"]
    assert rate["base"] != rate["quote"]

    name = generate_random_name()
    account_key = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"
    cli_wallet.create_account_with_private_key(account_key, name, "nathan")
    cli_wallet.transfer("nathan", name, 300)
    utils.wait_blocks(1)

    rate = utils.get_exchange_rate("1.3.1")

    key = generate_random_key()
    prev_balance = cli_wallet.get_account_balance(name)

    # core to usd rate is 20, see test_provide_price_feed
    # monthly_crypviser_fee = 5
    to_pay = 20 * 5

    cli_wallet.update_account_pk(name, key, to_pay, "USD")
    utils.wait_key_updated(name, key)

    new_balance = cli_wallet.get_account_balance(name)
    charged_core = prev_balance - new_balance

    # FIXME:
    # there is another fee here: price_per_kbyte
    # it depends on the key length, and it's quite difficult to calculate
    fee = 100000 # 1 CVT
    cvcoin = 100000 # precision
    assert cvcoin * (to_pay / rate) + fee < charged_core
    assert cvcoin * (to_pay / rate) + 2 * fee > charged_core


@pytest.mark.skipif(not get_test_utils().is_between_hf346_and_hf783(),
                    reason="This test should pass after hardfork 346 and before hardfork 783")
def test_subscribe_account_using_subscription_plan(cli_wallet, utils):
    log_step('Set new subscription pack')
    sub = get_next_pack_id()
    new_subscription_plan = [[sub, (3*100000)]]
    utils.set_subscription_plan(new_subscription_plan)

    log_step('Subscribe account')
    acc = utils.create_account_with_balance()
    key = generate_random_key()
    cli_wallet.subscribe_account(acc, key, sub, 6, "CVT")

    log_step('Get current time for check if subscription period is correct')
    current_time = dt.parse(cli_wallet.get_dynamic_global_properties()['time'])

    utils.wait_key_updated(acc, key)

    log_step('Check that account is subscribed correctly')
    exp_key = cli_wallet.get_account_pk(acc)
    assert key == exp_key["key"]
    assert sub == exp_key["subscription_pack_id"]

    log_step('Check that subscription is 60 days (2 month)')
    expiration = dt.parse(exp_key['expiration'])
    subscription = expiration - current_time

    assert subscription.days == 60


@pytest.mark.timeout(timeout=300)
def test_all_active_witnesses_participate_in_block_production(cli_wallet, utils):
    log_step('pre-condition: we need more than one active witness')
    witnesses = cli_wallet.list_witnesses()
    assert 1 < len(witnesses)

    log_step('vote in the witnesses')
    for witness in witnesses:
        witness_account = witness[0]
        utils.wait_transfer("nathan", witness_account, 100000)
        cli_wallet.try_vote_for_witness(witness_account, witness_account)

    utils.wait_blocks(1)
    utils.wait_until_maintenance_finished()

    log_step('ensure that we have all witnesses voted in')
    active_witnesses = cli_wallet.get_active_witnesses()
    assert 3 <= len(active_witnesses)

    log_step('collect last signed block for every witness')
    confirmed_blocks = {}
    for witness in active_witnesses:
        block = cli_wallet.get_witness(witness)["last_confirmed_block_num"]
        confirmed_blocks[witness] = block

    log_step('give a chance to every witness to sign a block')
    for i in xrange(10):
        cli_wallet.transfer("nathan", "init0", 1)
        utils.wait_blocks(1)

    log_step('Check that active witnesses were not changed')
    current_active_witnesses = cli_wallet.get_active_witnesses()
    assert current_active_witnesses == active_witnesses

    log_step('verify that the last signed block for every witness does change')
    for witness in active_witnesses:
        block = cli_wallet.get_witness(witness)["last_confirmed_block_num"]
        assert block > confirmed_blocks[witness]
