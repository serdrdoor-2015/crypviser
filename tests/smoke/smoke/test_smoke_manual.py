import pytest
from datetime import datetime
from utils.py_logger import log_step
from utils.pack_id_generator import get_next_pack_id
from utils.testutil import generate_random_key, prepare_subscription_pack


@pytest.mark.skip("Test should be run only manual and between "
                  "hardfork 346 and 783. Set correct hardfork 783 time,"
                  "comment this skip fixture and run test")
def test_subscription_plan_is_converted_to_subscription_packs_after_hf_783(cli_wallet, utils):
    sub_id_1 = get_next_pack_id()
    sub_id_2 = get_next_pack_id()

    sub_1_min_cost = 3 * 100000
    sub_2_min_cost = 4 * 100000

    account_1 = utils.create_account_with_balance()
    account_2 = utils.create_account_with_balance()

    log_step('Set 2 new subscription plans')
    subscription_plan = [[sub_id_1, sub_1_min_cost],
                         [sub_id_2, sub_2_min_cost]]
    utils.set_subscription_plan(subscription_plan)

    log_step('Subscribe accounts')
    key_1 = generate_random_key()
    cli_wallet.subscribe_account(account_1, key_1, sub_id_1, 6, "CVT")
    utils.wait_key_updated(account_1, key_1)

    key_2 = generate_random_key()
    cli_wallet.subscribe_account(account_2, key_2, sub_id_2, 6, "CVT")
    utils.wait_key_updated(account_2, key_2)

    log_step('Waiting for hardfork 783')
    hardfork_783_time = datetime(2018, 12, 26, 7, 44)
    utils.wait_until(hardfork_783_time)

    log_step('Check that subscription_plan was removed from additional_parameters')
    global_additional_params = cli_wallet.get_global_additional_params()
    assert 'subscription_plan' not in global_additional_params

    log_step('Check that subscription plans are converted to subscription packs')
    expected_pack_1 = prepare_subscription_pack(sub_id_1, sub_1_min_cost,
                                                sub_1_min_cost)
    expected_pack_2 = prepare_subscription_pack(sub_id_2, sub_2_min_cost,
                                                sub_2_min_cost)
    subscription_packs = cli_wallet.get_subscription_packs()

    assert len(subscription_packs) == 2

    assert expected_pack_1 in subscription_packs
    assert expected_pack_2 in subscription_packs

    log_step('Check that account_1 still subscribed to the pack_1')
    exp_key = cli_wallet.get_account_pk(account_1)
    assert key_1 == exp_key["key"]
    assert sub_id_1 == exp_key["subscription_pack_id"]

    log_step('Check that account_2 still subscribed to the pack_2')
    exp_key = cli_wallet.get_account_pk(account_2)
    assert key_2 == exp_key["key"]
    assert sub_id_2 == exp_key["subscription_pack_id"]
