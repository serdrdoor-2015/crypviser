import pytest
import socket as s
from utils.testutil import TestUtils, generate_random_name
from utils.committee import Committee
from utils.cli_wallet import CliWallet
from utils.witness_node import WitnessNode
from utils.py_logger import logger, PrettyFormatter
from utils.step_generator import StepGenerator


def pytest_logger_config(logger_config):
    logger_config.add_loggers(['pytest_logger'], stdout_level='info')
    logger_config.set_log_option_default('pytest_logger')
    logger_config.set_formatter_class(PrettyFormatter)


@pytest.fixture(scope="function", autouse=True)
def reset_step_generator():
    step_generator = StepGenerator()
    step_generator.reset()


@pytest.yield_fixture
def socket():
    _socket = s.socket(s.AF_INET, s.SOCK_STREAM)
    yield _socket
    _socket.close()


@pytest.fixture(scope='session')
def cli_wallet():
    host_port = 'localhost', 8092
    uri = 'http://%s:%s/' % host_port
    return CliWallet(uri)


@pytest.fixture(scope='session')
def witness_node():
    host_port = 'localhost', 11010
    uri = 'http://%s:%s/' % host_port
    return WitnessNode(uri)


@pytest.fixture(scope='session')
def utils(cli_wallet):
    return TestUtils(cli_wallet)


def get_test_utils():
    return TestUtils(cli_wallet())


@pytest.fixture(scope='session', autouse=True)
def set_empty_subscription_packs(cli_wallet, utils):
    if not utils.is_before_hf783():
        accountName = generate_random_name()
        registrarName = "nathan"
        cli_wallet.register_account(accountName)
        utils.wait_blocks(1)
        cli_wallet.transfer(registrarName, accountName, 1000000)

        committeeAccounts = ["init0", "init1", "init2"]

        committee = Committee(cli_wallet)

        committee.increaseBalance(accountName, 5)

        for member in committeeAccounts:
            cli_wallet.transfer(accountName, member, 100000)
            utils.wait_blocks(1)
            committee.addMember(accountName, member)

        utils.wait_until_maintenance_finished()

        params = cli_wallet.get_global_additional_params()

        params['subscription_packs'] = []
        committee.update_global_property("init0", "init1",
                                         "additional_parameters",
                                         params)


@pytest.fixture(scope="function")
def committee(cli_wallet, utils):
    # create new accont (testAccount)
    # transfer to testAccount CVT
    # transfer to commitee CVT from testAccount balance
    # transfer to commitee members CVT from testAccount balance
    # testAccount vote for commitee members

    accountName = generate_random_name()
    registrarName = "nathan"
    cli_wallet.register_account(accountName)
    utils.wait_blocks(1)
    cli_wallet.transfer(registrarName, accountName, 1000000)

    committeeAccounts = ["init0", "init1", "init2"]

    committee = Committee(cli_wallet)

    # there are 5 operations (interval, referral_percent, subscription_plan x3)
    # that should be applied
    # it costs committee 5 tokens to make voting effective
    committee.increaseBalance(accountName, 5)

    for member in committeeAccounts:
        cli_wallet.transfer(accountName, member, 100000)
        utils.wait_blocks(1)
        committee.addMember(accountName, member)

    utils.wait_until_maintenance_finished()

    previous_global_params = cli_wallet.get_global_parameters()

    yield committee

    logger.info('committee fixture tearDown')
    current_global_params = cli_wallet.get_global_parameters()
    param_value = dict()
    for key in previous_global_params.keys():
        if isinstance(previous_global_params[key], int) or \
                key == 'additional_parameters':
            if current_global_params[key] != previous_global_params[key]:
                param_value[key] = current_global_params[key]
                logger.info(
                    'current %s:%s' % (key, current_global_params[key]))
                logger.info(
                    'previous %s:%s' % (key, previous_global_params[key]))
                committee.update_global_property("init0", "init1", key,
                                                 previous_global_params[key])

    for member in committeeAccounts:
        committee.deleteMember(accountName, member)
        utils.wait_blocks(1)

    utils.wait_until_maintenance_finished()
    if param_value:
        param = param_value.keys()[0]
        logger.info('Check that %s was changed back' % param)
        current_global_params = cli_wallet.get_global_parameters()
        assert current_global_params[param] == previous_global_params[param]
