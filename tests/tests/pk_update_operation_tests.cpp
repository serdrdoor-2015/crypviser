
#include <boost/test/unit_test.hpp>
#include <boost/core/ignore_unused.hpp>
#include <chrono>

#include <graphene/chain/database.hpp>
#include <graphene/app/database_api.hpp>
#include <graphene/chain/exceptions.hpp>
#include <graphene/chain/pk_update_calc.hpp>

#include <fc/crypto/pke.hpp>

#include "../common/database_fixture.hpp"

using namespace graphene::chain;
using namespace graphene::chain::test;

namespace graphene::chain {
    bool operator == (const subscription_pack& lhs, const subscription_pack& rhs)
    {
        return (lhs.id == rhs.id) && (lhs.min_cost == rhs.min_cost) && (lhs.price_per_month == rhs.price_per_month);
    }
}

namespace std::chrono {
   template<typename Ch, typename Rep, typename Period>
   basic_ostream<Ch>& operator<<(basic_ostream<Ch>& os,  const duration<Rep, Period> &d)
   {
      os << duration_cast<microseconds>(d).count() << " us";
      return os;
   }
}

using public_key = cv::public_key;
using expiring_key = cv::expiring_key;
using subscription_plan_type = fc::flat_map<int64_t, share_type>;
using subscription_plan_hf_783_type = additional_chain_parameters::subscription_packs_t;

struct pk_update_operation_fixture : database_fixture
{
    pk_update_operation_fixture()
        : database_fixture(HARDFORK_346_TIME - 100),
          api(db)
    {
        create_test_accounts();
        set_price_feed();
    }

    auto create_account_key(std::string name, 
                            account_id_type registrar = account_id_type(),
                            uint64_t init_balance = 1e12)
    {
        auto key = fc::ecc::private_key::generate();
        const auto& registrar_obj = db.get(registrar);
        const auto& account = create_account(name, registrar_obj, key.get_public_key());
        auto funding = asset_id_type()(db).amount(init_balance);
        int64_t balance = fund(account, funding);

        account_keys[account.id] = key;
        return std::make_tuple(key, account.id, balance);
    }

    void create_test_accounts()
    {
        auto account0 = (create_account_key("registrar"));
        std::tie(registrar_key, registrar, initial_balance) = account0;

        auto account1 = create_account_key("test", registrar);
        std::tie(test_key, test_account, initial_balance) = account1;

        auto account2 = create_account_key("test2", registrar, 1000);
        std::tie(test_key_2, test_account_2, low_balance) = account2;
    }

    ~pk_update_operation_fixture()
    {
    }

    template <typename Operation>
    void push_operation(Operation op)
    {
        push_signed_operation(op, test_key);
    }

    template <typename Operation>
    void push_signed_operation(Operation op, fc::ecc::private_key key)
    {
        set_expiration(db, trx);
        trx.clear();        
        trx.operations.push_back(op);
        sign(trx, key);
        trx.validate();
        db.push_transaction(trx);
    }

    template <typename Operation>
    void push_unsigned_operation(Operation op)
    {
        trx.clear();
        trx.operations.push_back(op);
        trx.validate();
        db.push_transaction(trx);
    }

    auto push_pk()
    {
        push_operation(pk_update);
    }

    const auto& core_token()
    {
        return asset_id_type()(db);
    }

    auto reserved()
    {
        return core_token().reserved(db);
    }

    auto core_token(share_type amount)
    {
        return core_token().amount(amount);
    }

    const auto& usd_token()
    {
        return asset_id_type(1)(db);
    }

    auto usd_token(share_type amount)
    {
        return usd_token().amount(amount);
    }

    const auto& euro_token()
    {
        return asset_id_type(2)(db);
    }

    auto euro_token(share_type amount)
    {
        return euro_token().amount(amount);
    }

    auto balance(account_id_type account)
    {
        return get_balance(db.get(account), core_token());
    }

    auto get_account_pk(account_id_type account)
    {
        return api.get_account_pk(account).key;
    }

    auto get_account_expiration(account_id_type account)
    {
        return api.get_account_pk(account).expiration;
    }

    auto get_account_permissions(account_id_type account)
    {
        return api.get_account_pk(account).permissions;
    }

    auto get_account_subscription_id(account_id_type account)
    {
        return api.get_account_pk(account).subscription_pack_id;
    }

    auto now()
    {
        return db.head_block_time();
    }

    auto get_monthly_fee_in_euro()
    {
        return euro_token(db.get_global_properties().parameters.monthly_crypviser_fee);
    }

    void set_max_monthly_subscription_price(share_type price)
    {
        db.modify(db.get_global_properties(), [price](global_property_object& prop)
        {
            *prop.parameters.additional_parameters.value.max_monthly_subscription_price = price;
        });
    }

    auto get_monthly_fee_in_core()
    {
        auto euro = euro_token(db.get_global_properties().parameters.monthly_crypviser_fee);
        return convert_to_core(db, euro).amount.value;
    }

    void set_registrar_percent(uint16_t level, uint16_t percent)
    {
        db.modify(db.get_global_properties(), [level, percent](global_property_object& prop)
        {
            prop.parameters.referral_percent[level] = percent;
        });
    }

    void set_subscription_plan(const subscription_plan_type &plan)
    {
        db.modify(db.get_global_properties(), [&plan](global_property_object& prop)
        {
            prop.parameters.additional_parameters.value.subscription_plan = plan;
        });
    }

    void set_subscription_plan_hf_783(const subscription_plan_hf_783_type &plans)
    {
        db.modify(db.get_global_properties(), [&plans](global_property_object& prop)
        {
            prop.parameters.additional_parameters.value.subscription_packs = plans;
        });
    }

    void update_expiration_by_amount(pk_update_operation &op, asset amount)
    {
        op.amount = amount;
        auto pack_id = get_subscription_pack_id(now(), op);
        auto pack = get_subscription_pack(db, pack_id);
        auto monthly = get_monthly(db, pack);
        op.expiration = calc_expiration_by_amount(op.expiration, now(), amount, monthly);
    }

    int64_t calc_expected_pay_value(const pk_update_operation &op)
    {
        using namespace std::chrono;
        auto current_time = now();
        auto duration = op.expiration - current_time;
        auto pay = calc_to_pay(core_token(get_monthly_fee_in_core()), microseconds{duration.count()});
        return pay.amount.value;
    }

    void subscribe_account(account_id_type account, asset fee)
    {
        auto key = account_keys[account];
        auto subscription_key = std::string("test-account-") + std::to_string(++accounts_subscribed);

        pk_update.expiration = get_account_expiration(account);
        pk_update.key = public_key{subscription_key};
        pk_update.account = account;
        update_expiration_by_amount(pk_update, fee); // prime number
        push_signed_operation(pk_update, account_keys[account]);
    }
    
    void subscribe_account(account_id_type account)
    {
        subscribe_account(account, core_token(13177));
    }

    void subscribe_account_hf346(account_id_type account, int64_t subscription_pack_id, asset fee)
    {
        auto key = account_keys[account];
        auto subscription_key = std::string("test-account-") + std::to_string(++accounts_subscribed);

        pk_update.expiration = get_account_expiration(account);
        pk_update.key = public_key{subscription_key};
        pk_update.extensions.value.subscription_pack = subscription_pack_id;
        pk_update.account = account;
        update_expiration_by_amount(pk_update, fee); // prime number
        push_signed_operation(pk_update, account_keys[account]);
    }

    void set_price_feed()
    {
        generate_block();

        GET_ACTOR(init0);

        db.modify(GRAPHENE_COMMITTEE_ACCOUNT(db), [&](account_object& a){
            a.active.add_authority( init0_id, weight_type( 2 ) );
        });

        auto account_auths = db.get(GRAPHENE_COMMITTEE_ACCOUNT).active.account_auths;
        BOOST_REQUIRE(account_auths.size() > 0);

        price_feed feed;
        asset euro = euro_token(1);
        asset core = core_token(100);
        feed.settlement_price = price( euro, core );
        publish_feed( euro_token(), init0, feed );
    }

    asset core_to_euro(asset amount)
    {
        FC_ASSERT(asset_id_type() == amount.asset_id);

        asset euro = euro_token(1);
        asset core = core_token(100);
        const auto result = amount * price(core, euro);
        return result;
    }

    pk_update_operation pk_update;
    graphene::app::database_api api;
    int64_t initial_balance;
    int64_t low_balance;

    fc::ecc::private_key test_key;
    account_id_type test_account;

    fc::ecc::private_key test_key_2;
    account_id_type test_account_2;

    fc::ecc::private_key registrar_key;
    account_id_type registrar;

    account_id_type null_account;

    int accounts_subscribed = 0;
    std::map<account_id_type, fc::ecc::private_key> account_keys;
};

BOOST_FIXTURE_TEST_SUITE( pk_update_operation_tests, pk_update_operation_fixture )

BOOST_AUTO_TEST_CASE(creates_public_key_when_called_first_time)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(10));

    push_pk();

    auto from_database = get_account_pk(test_account);
    BOOST_CHECK(public_key{"1234"} == from_database);
}

BOOST_AUTO_TEST_CASE(updates_public_key_when_called_again)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(10));
    push_pk();

    pk_update.key = public_key{"5678"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(10));
    push_pk();

    auto from_database = get_account_pk(test_account);
    BOOST_CHECK(public_key{"5678"} == from_database);
}

BOOST_AUTO_TEST_CASE(does_not_allow_unsigned_operation)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(10));

    BOOST_CHECK_THROW(push_unsigned_operation(pk_update), fc::exception);
}

BOOST_AUTO_TEST_CASE(throws_if_signed_with_wrong_key)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(10));

    auto wrong_key = fc::ecc::private_key::generate();
    BOOST_CHECK_THROW(push_signed_operation(pk_update, wrong_key), fc::exception);
}

BOOST_AUTO_TEST_CASE(throws_if_duplicate_key)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(10));
    push_pk();

    pk_update.key = public_key{"1234"};
    pk_update.account = test_account_2; 
    update_expiration_by_amount(pk_update, core_token(10));

    BOOST_CHECK_THROW(push_signed_operation(pk_update, test_key_2), fc::exception);
}

BOOST_AUTO_TEST_CASE(throws_if_empty_key)
{
    pk_update.key = public_key{""};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(10));

    BOOST_CHECK_THROW(push_pk(), fc::exception);
}

BOOST_AUTO_TEST_CASE(throws_if_insufficient_balance)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account_2; // account with low initial balance
    update_expiration_by_amount(pk_update, core_token(1 + initial_balance));

    BOOST_CHECK_THROW(push_pk(), fc::exception);
}

BOOST_AUTO_TEST_CASE(spends_funds_sent_with_the_operation)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(10000));

    push_pk();

    BOOST_CHECK_EQUAL(initial_balance - 10000, balance(test_account));
}

BOOST_AUTO_TEST_CASE(does_not_spend_any_money_since_account_does_not_have_registrar)
{
    subscribe_account(registrar);

    BOOST_CHECK_EQUAL(initial_balance, balance(registrar));
}

BOOST_AUTO_TEST_CASE(sends_everything_to_reserve_fund)
{
    auto prev_balance = balance(registrar);

    set_registrar_percent(0, 0);
    subscribe_account(registrar);

    auto reserve_fund = reserved().value;

    BOOST_CHECK_GT(reserve_fund, 0);
    BOOST_CHECK_EQUAL(prev_balance, reserve_fund + balance(registrar));
}

BOOST_AUTO_TEST_CASE(a_new_account_has_no_registered_key)
{
    auto from_database = get_account_pk(test_account);
    BOOST_CHECK(public_key{""} == from_database);
}

BOOST_AUTO_TEST_CASE(a_new_account_has_long_expired_key)
{
    auto from_database = get_account_expiration(test_account);

    BOOST_CHECK(from_database < now());
    BOOST_CHECK(fc::time_point_sec{} == from_database);
}

BOOST_AUTO_TEST_CASE(sets_initial_key_expiration_date)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(get_monthly_fee_in_core()));
    push_pk();

    auto from_database = get_account_expiration(test_account);

    BOOST_CHECK(from_database > now());
}

BOOST_AUTO_TEST_CASE(updates_key_expiration_date)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(get_monthly_fee_in_core()));

    push_pk();

    auto expiration_1 = get_account_expiration(test_account);

    pk_update.key = public_key{"5678"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(get_monthly_fee_in_core()));
    
    push_pk();

    auto expiration_2 = get_account_expiration(test_account);
    BOOST_CHECK(expiration_2 > expiration_1);
}

BOOST_AUTO_TEST_CASE(pays_monthly_fee)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(get_monthly_fee_in_core()));

    push_pk();

    auto from_database = get_account_expiration(test_account);
    auto duration = from_database - now();

    using namespace std::chrono;
    auto expected = microseconds{month()};
    auto actual = microseconds{duration.count()};
    BOOST_CHECK_EQUAL(expected.count(), actual.count());
}

BOOST_AUTO_TEST_CASE(pays_monthly_fee_twice)
{
    subscribe_account(test_account, core_token(get_monthly_fee_in_core()));
    subscribe_account(test_account, core_token(get_monthly_fee_in_core()));

    auto delta = initial_balance - balance(test_account);
    // delta mightly be slightly less than twice monthly fee due to timing issues
    // but it must be just slightly less, i'm using 2 qis here
    BOOST_CHECK_GE(delta + 2, 2 * get_monthly_fee_in_core());
    BOOST_CHECK_LT(delta, 3 * get_monthly_fee_in_core());
}

BOOST_AUTO_TEST_CASE(pays_monthly_fee_thrice)
{
    subscribe_account(test_account, core_token(get_monthly_fee_in_core()));
    subscribe_account(test_account, core_token(get_monthly_fee_in_core()));
    subscribe_account(test_account, core_token(get_monthly_fee_in_core()));

    auto delta = initial_balance - balance(test_account);
    // delta mightly be slightly less than twice monthly fee due to timing issues
    // but it must be just slightly less, i'm using 3 satoshis here
    BOOST_CHECK_GE(delta + 3, 3 * get_monthly_fee_in_core());
    BOOST_CHECK_LT(delta, 4 * get_monthly_fee_in_core());
}

BOOST_AUTO_TEST_CASE(user_is_prohibited_from_paying_for_more_than_one_year_ahead)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(initial_balance));

    push_pk();

    auto from_database = get_account_expiration(test_account);
    auto duration = from_database - now();

    using namespace std::chrono;
    auto expected = microseconds{year()};
    auto actual = microseconds{duration.count()};
    BOOST_CHECK_EQUAL(expected.count(), actual.count());
}

BOOST_AUTO_TEST_CASE(user_cant_pay_for_more_than_a_year_even_if_pay_is_spread_across_many_transactions)
{
    using namespace std::chrono;

    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    pk_update.amount = core_token(get_monthly_fee_in_core() * 5);
    pk_update.expiration = now() + fc::microseconds{5 * microseconds{month()}.count()};
    push_pk();

    pk_update.key = public_key{"45678"};
    pk_update.account = test_account;
    pk_update.amount = core_token(get_monthly_fee_in_core() * 8);
    pk_update.expiration = pk_update.expiration + fc::microseconds{8 * microseconds{month()}.count()};

    BOOST_CHECK_THROW(push_pk(), fc::exception);
}

BOOST_AUTO_TEST_CASE(user_receives_change_if_pays_for_more_than_a_year)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(initial_balance));

    push_pk();

    auto paid = initial_balance - balance(test_account);
    BOOST_CHECK_LT(paid, 13 * get_monthly_fee_in_core());
    BOOST_CHECK_GE(paid, 12 * get_monthly_fee_in_core());
}

BOOST_AUTO_TEST_CASE(sends_all_money_to_registrar)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(10000));
    
    auto initial_balance = balance(registrar); 
    push_pk(); 

    BOOST_CHECK_EQUAL(initial_balance + 10000, balance(registrar));
}

BOOST_AUTO_TEST_CASE(root_registrar_may_update_its_key_for_free)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = registrar; 
    update_expiration_by_amount(pk_update, core_token(10000));
    
    push_signed_operation(pk_update, registrar_key);

    BOOST_CHECK_EQUAL(initial_balance, balance(registrar));
}

BOOST_AUTO_TEST_CASE(graphene_temp_account_does_not_receive_money_via_registrar_program)
{
    auto obj = create_account_key("temp-registrar", GRAPHENE_TEMP_ACCOUNT);
    auto key = std::get<0>(obj);
    auto account = std::get<1>(obj);
    auto initial_balance = std::get<2>(obj);

    pk_update.key = public_key{"1234"};
    pk_update.account = account; 
    update_expiration_by_amount(pk_update, core_token(10000));
    
    push_signed_operation(pk_update, key);

    BOOST_CHECK_EQUAL(initial_balance, balance(account));
}

BOOST_AUTO_TEST_CASE(splits_pay_between_all_registrars)
{
    // split pay between root referral and level 1 referral
    set_registrar_percent(0, 50 * GRAPHENE_1_PERCENT); // root referral percent
    set_registrar_percent(1, 50 * GRAPHENE_1_PERCENT); // level 1 referral percent

    // registrar is required to be subscribed, otherwise it's not goint to get its percent
    subscribe_account(test_account);

    // register new account
    auto obj = create_account_key("foobar", test_account);
    auto key = std::get<0>(obj);
    auto account = std::get<1>(obj);
    auto initial_balance = std::get<2>(obj);

    boost::ignore_unused(initial_balance);
    boost::ignore_unused(key);

    auto root_registrar_balance = balance(registrar);
    auto lvl1_registrar_balance = balance(test_account);

    //  update new account key, pays 13177 core
    subscribe_account(account);

    auto root_registrar_pay = balance(registrar) - root_registrar_balance;
    auto lvl1_registrar_pay = balance(test_account) - lvl1_registrar_balance;

    BOOST_CHECK_GT(root_registrar_pay, 0u); // root registrar gets paid
    BOOST_CHECK_GT(lvl1_registrar_pay, 0u); // level 1 registrar gest paid

    // the total pay mightly be slightly more than the sum transferred
    // since it also includes the transaction fees
    // on the other hand it might be slightly less, because the expiration date
    // may be computed in such a way, that the paying account receives some change
    int64_t actual_pay = root_registrar_pay + lvl1_registrar_pay;
    auto expected_pay = 13177ll;
    BOOST_CHECK_LE(abs(expected_pay - actual_pay), 10); 
}

BOOST_AUTO_TEST_CASE(pays_everything_to_root_registrar)
{
    set_registrar_percent(0, GRAPHENE_100_PERCENT);

    const auto obj = create_account_key("foobar", test_account);
    const auto key = std::get<0>(obj);
    const auto foobar = std::get<1>(obj);
    const auto foobar_initial_balance = std::get<2>(obj);
    const auto root_registrar_initial_balance = balance(registrar);

    pk_update.key = public_key{"1234"};
    pk_update.account = foobar;

    const auto to_pay = core_token(13177);
    update_expiration_by_amount(pk_update, to_pay); // prime number
    push_signed_operation(pk_update, key);

    // delta mightly be slightly less than monthly fee due to timing or rounding issues issues
    const auto delta = foobar_initial_balance - balance(foobar);
    BOOST_CHECK_EQUAL(delta + 1, to_pay.amount.value );

    const auto expected = calc_expected_pay_value(pk_update);
    const auto root_registrar_balance = balance(registrar);

    BOOST_CHECK_EQUAL(root_registrar_initial_balance + expected, root_registrar_balance);
    BOOST_CHECK_EQUAL(initial_balance, balance(test_account));
}

BOOST_AUTO_TEST_CASE(adjusts_balances_between_payer_and_all_its_registrars)
{
    set_registrar_percent(0, 50 * GRAPHENE_1_PERCENT); // root referral percent
    set_registrar_percent(1, 50 * GRAPHENE_1_PERCENT); // level 1 referral percent

    // test_acount serves as a registrar of foobar, 
    // test_account is a registrar of foobar
    auto obj = create_account_key("foobar", test_account);
    auto account = std::get<1>(obj);
    auto initial_balance = std::get<2>(obj);

    // registrar is required to be subscribed, otherwise it's not goint to get its percent
    subscribe_account(test_account);
    
    auto prev_registrar_balance = balance(registrar);
    transfer_to_referrals(db, account, core_token(13177)); // prime number

    BOOST_CHECK_EQUAL(initial_balance - 13177, balance(account));

    int64_t expect_root_fee = std::ceil(13177 / 2.); // root registrar takes 50%
    BOOST_CHECK_EQUAL(prev_registrar_balance + expect_root_fee, balance(registrar));

    BOOST_CHECK_EQUAL(initial_balance * 3, balance(registrar) +
                                           balance(test_account) +
                                           balance(account));
}

BOOST_AUTO_TEST_CASE(ignores_unsubscribed_account_when_distributing_subscription_fee)
{
    set_registrar_percent(0, 50 * GRAPHENE_1_PERCENT); // root referral percent
    set_registrar_percent(1, 50 * GRAPHENE_1_PERCENT); // level 1 referral percent

    // test_acount serves as a registrar of foobar, 
    // test_account is a registrar of foobar
    auto obj = create_account_key("foobar", test_account);
    auto account = std::get<1>(obj);

    auto prev_balance = balance(registrar);

    transfer_to_referrals(db, account, core_token(13177)); // prime number

    BOOST_CHECK_EQUAL(prev_balance + 13177, balance(registrar));
}

BOOST_AUTO_TEST_CASE(pays_only_to_immediate_registrar)
{
    set_registrar_percent(0, 0); // root referral percent
    set_registrar_percent(1, GRAPHENE_100_PERCENT); // level 1 referral percent

    subscribe_account(test_account);
    auto foo = std::get<1>(create_account_key("foo", test_account));
    
    subscribe_account(foo);
    auto bar = std::get<1>(create_account_key("bar", foo));

    subscribe_account(bar);

    auto prev_balance = balance(foo);

    transfer_to_referrals(db, bar, core_token(13177)); // prime number

    BOOST_CHECK_EQUAL(prev_balance + 13177, balance(foo));
}

BOOST_AUTO_TEST_CASE(splits_pay_according_to_configuration)
{
    set_registrar_percent(0, 30 * GRAPHENE_1_PERCENT); // root referral percent
    set_registrar_percent(1, 70 * GRAPHENE_1_PERCENT); // level 1 referral percent

    subscribe_account(test_account); // test_account needs to be subscribed in order to get paid

    auto obj = create_account_key("foobar", test_account);
    auto account = std::get<1>(obj);

    auto prev_root_balance = balance(registrar);
    auto prev_lvl1_balance = balance(test_account);

    transfer_to_referrals(db, account, core_token(13177)); // prime number

    auto root_paid = balance(registrar) - prev_root_balance;
    auto lvl1_paid = balance(test_account) - prev_lvl1_balance;

    BOOST_CHECK_GT(lvl1_paid, 2 * root_paid);
}

BOOST_AUTO_TEST_CASE(splits_pay_between_all_15_registrars_plus_root_registrar)
{
    // 100% = 15 * 6% + 10%
    set_registrar_percent(0, 10 * GRAPHENE_1_PERCENT); // root referral percent
    for (int i = 1; i <= 15; ++i)
    {
        set_registrar_percent(i, 6 * GRAPHENE_1_PERCENT); 
    }

    auto register_accounts = [&](auto& accounts)
    {
        std::string name("a-foobar");
        auto gen_next_name = [&]
        {
            name[0] += 1;
        };

        account_id_type account_registrar = registrar;
        for (int i = 0; i < 15; ++i)
        {
            accounts.push_back(account_registrar);
            auto obj = create_account_key(name, account_registrar);
            auto account = std::get<1>(obj);

            subscribe_account(account);
            account_registrar = account;
            gen_next_name();
        }

        accounts.push_back(account_registrar);
    };
    
    auto get_balances = [&](auto& accounts, auto& balances)
    {
        for (auto a : accounts)
        {
            balances.push_back(balance(a));
        }
    };

    auto calc_pay = [&](auto& balances, auto& new_balances)
    {
        assert(balances.size() == new_balances.size());

        std::vector<uint64_t> pay;
        for (auto i = 0u; i < balances.size(); ++i)
        {
            pay.push_back(new_balances[i] - balances[i]);
        }

        return pay;
    };

    std::vector<account_id_type> accounts;
    register_accounts(accounts);

    auto paying_account = accounts.back();
    accounts.pop_back();

    std::vector<uint64_t> balances;
    get_balances(accounts, balances);

    transfer_to_referrals(db, paying_account, core_token(13177)); 

    std::vector<uint64_t> new_balances;
    get_balances(accounts, new_balances);

    auto pay = calc_pay(balances, new_balances);
    for (auto p : pay) // every registrar gets its cut
    {
        BOOST_CHECK_GT(p, 0u);
        BOOST_CHECK_LT(p, 13177u);
    }

    uint64_t sum = std::accumulate(pay.begin(), pay.end(), 0u);
    BOOST_CHECK_EQUAL(13177u, sum);
}


BOOST_AUTO_TEST_CASE(creates_permissions)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    pk_update.permissions = {'a', 'u', 'd', 'i', 'o'};
    update_expiration_by_amount(pk_update, core_token(10));

    push_pk();

    auto from_database = get_account_permissions(test_account);
    BOOST_CHECK_EQUAL("audio", std::string(from_database.begin(), from_database.end()));
}

BOOST_AUTO_TEST_CASE(updates_permissions)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(10));
    push_pk();

    pk_update.key = public_key{"5678"};
    pk_update.account = test_account; 
    pk_update.permissions = {'a', 'u', 'd', 'i', 'o'};
    update_expiration_by_amount(pk_update, core_token(10));
    push_pk();

    auto from_database = get_account_permissions(test_account);
    BOOST_CHECK_EQUAL("audio", std::string(from_database.begin(), from_database.end()));
}

BOOST_AUTO_TEST_CASE(proves_account_public_key)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    update_expiration_by_amount(pk_update, core_token(10));
    push_pk();
    generate_block();

    auto proof = *api.prove_account_pk(test_account);
    auto block = *api.get_block(db.head_block_num());

    BOOST_CHECK(verify_merkle(proof.tx, proof.path, block.transaction_merkle_root));
}

BOOST_AUTO_TEST_CASE(user_receives_change_if_pays_for_more_than_calculated_period)
{
    using namespace std::chrono;
    // remember the current block time
    const auto transaction_create_timepoint = now();

    // generate some blocks to move forward the time
    generate_block();
    generate_block();

    const auto monthly_euro = get_monthly_fee_in_euro();
    const auto to_pay = convert_to_core( db, monthly_euro );

    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    pk_update.amount = to_pay;
    // set expiration as transaction_create_timepoint + month
    // so user should pay only for duration: month - (now - transaction_create_timepoint)
    pk_update.expiration = transaction_create_timepoint 
                            + fc::microseconds{microseconds{month()}.count()};

    push_pk();

    const auto expected = calc_expected_pay_value(pk_update);
    const auto paid = initial_balance - balance(test_account);

    BOOST_CHECK_LT(paid, to_pay.amount.value);
    BOOST_CHECK_EQUAL(paid, expected);
}

BOOST_AUTO_TEST_CASE(throws_if_user_sends_not_enough_money)
{
    using namespace std::chrono;
    
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    pk_update.amount = core_token(get_monthly_fee_in_core() / 2);
    pk_update.expiration = now() + fc::microseconds{microseconds{month()}.count()};

    BOOST_CHECK_THROW(push_pk(), fc::exception);
}

BOOST_AUTO_TEST_CASE(throws_if_set_expiration_date_in_the_past)
{
    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    pk_update.expiration = now() - 1;
    BOOST_CHECK_THROW(push_pk(), fc::exception);
}

BOOST_AUTO_TEST_CASE(bug_before_hf346_throws_when_extend_key_into_the_future_if_already_expired)
{
    subscribe_account(test_account, core_token(3));

    // expire subscription
    generate_blocks(pk_update.expiration + 20);

    BOOST_CHECK_THROW(subscribe_account(test_account, core_token(10)), fc::exception);
}

BOOST_AUTO_TEST_CASE(bug_before_hf346_user_can_reduce_or_cancel_subscription_and_get_refund)
{
    subscribe_account(test_account, core_token(get_monthly_fee_in_core()));

    auto after_subscribe = balance(test_account);
    auto after_subscribe_expiration = get_account_expiration(test_account);

    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    pk_update.expiration = now() + 1;

    push_pk();

    // get refund after last update
    BOOST_CHECK_LT(after_subscribe, balance(test_account));

    // reduce key expiration
    BOOST_CHECK(after_subscribe_expiration > get_account_expiration(test_account));
}

BOOST_AUTO_TEST_CASE(extend_subscription_into_the_future_even_if_expired_long_ago)
{
    generate_blocks( HARDFORK_346_TIME );
    generate_block();

    const auto monthly = euro_token(5000);
    const auto to_pay = convert_to_core(db, monthly);
    set_subscription_plan({{1, monthly.amount}});

    subscribe_account_hf346(test_account, 1, to_pay);
    generate_blocks(pk_update.expiration + 20);
    subscribe_account_hf346(test_account, 1, to_pay);

    const auto from_database = get_account_expiration(test_account);
    BOOST_CHECK(from_database == pk_update.expiration);
}

BOOST_AUTO_TEST_CASE(throws_if_reduce_or_cancel_subscription_after_346_hardfork)
{
    generate_blocks( HARDFORK_346_TIME );
    generate_block();

    const auto monthly = euro_token(5000);

    set_subscription_plan({{1, monthly.amount}});
    
    subscribe_account_hf346(test_account, 1, core_token(get_monthly_fee_in_core()));

    pk_update.key = public_key{"1234"};
    pk_update.account = test_account;
    pk_update.expiration = now() + 1;

    BOOST_CHECK_THROW(push_pk(), fc::exception);
}

BOOST_AUTO_TEST_CASE(throws_if_subscription_extension_used_before_346_hardfork)
{
    BOOST_CHECK_THROW(
        subscribe_account_hf346(test_account, 1, core_token(get_monthly_fee_in_core())),
        fc::exception);
}

BOOST_AUTO_TEST_CASE(throws_if_subscripted_less_than_one_month_after_346_hardfork)
{
    generate_blocks( HARDFORK_346_TIME );
    generate_block();

    const auto monthly = euro_token(5000);
    set_subscription_plan({{1, monthly.amount}});

    const auto to_pay = convert_to_core(db, monthly).amount / 2;
    BOOST_CHECK_THROW(
        subscribe_account_hf346(test_account, 1, core_token(to_pay)), 
        fc::exception);
}

BOOST_AUTO_TEST_CASE(user_cant_change_active_subscription_type)
{
    generate_blocks( HARDFORK_346_TIME );
    generate_block();

    const auto monthly_1_plan = euro_token(5000);
    const auto monthly_2_plan = euro_token(3000);
    set_subscription_plan({{1, monthly_1_plan.amount},{2, monthly_2_plan.amount}});

    {
        const auto to_pay = convert_to_core(db, monthly_1_plan);
        subscribe_account_hf346(test_account, 1, to_pay);

        // delta mightly be slightly less than monthly fee due to timing or rounding issues issues
        auto delta = initial_balance - balance(test_account);
        BOOST_CHECK_EQUAL(delta + 1, to_pay.amount.value );
    }
    {
        const auto to_pay = convert_to_core(db, monthly_2_plan);
        BOOST_CHECK_THROW(subscribe_account_hf346(test_account, 2, to_pay), fc::exception);
    }
}

BOOST_AUTO_TEST_CASE(user_pays_for_subscription_according_to_subscription_type)
{
    generate_blocks( HARDFORK_346_TIME );
    generate_block();

    const auto monthly = euro_token(5000);
    set_subscription_plan({{1, monthly.amount}});

    const auto to_pay = convert_to_core(db, monthly);
    subscribe_account_hf346(test_account, 1, to_pay );

    // delta mightly be slightly less than monthly fee due to timing or rounding issues issues
    auto delta = initial_balance - balance(test_account);
    BOOST_CHECK_EQUAL(delta + 1, to_pay.amount.value );

    auto foo = std::get<1>(create_account_key("foo", test_account));
    subscribe_account_hf346(foo, 0, core_token(get_monthly_fee_in_core()));

    delta = initial_balance - balance(foo);
    // delta mightly be slightly less than monthly fee due to timing or rounding issues issues
    BOOST_CHECK_GE(delta + 1, get_monthly_fee_in_core());
}

BOOST_AUTO_TEST_CASE(throws_if_subscription_id_not_exists)
{
    generate_blocks( HARDFORK_346_TIME );
    generate_block();

    BOOST_CHECK_THROW(
        subscribe_account_hf346(test_account, 1, core_token(get_monthly_fee_in_core())),
        fc::exception);
}

BOOST_AUTO_TEST_CASE(user_subscribes_for_minmal_period_even_in_case_of_network_latency)
{
    generate_blocks( HARDFORK_346_TIME );
    generate_block();

    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(get_monthly_fee_in_core()));

    using namespace std::chrono;
    const auto delay = fc::microseconds{cv::update_operation_ttl_ms.count() - 5000000};
    generate_blocks( now() + delay );

    push_pk();

    auto from_database = get_account_expiration(test_account);
    BOOST_CHECK(from_database == pk_update.expiration);
}

BOOST_AUTO_TEST_CASE(throws_if_update_pk_transaction_delayed_for_a_long_period)
{
    generate_blocks( HARDFORK_346_TIME );
    generate_block();

    pk_update.key = public_key{"1234"};
    pk_update.account = test_account; 
    update_expiration_by_amount(pk_update, core_token(get_monthly_fee_in_core()));

    using namespace std::chrono;
    const auto delay = fc::microseconds{cv::update_operation_ttl_ms.count() + 5000000};
    generate_blocks( now() + delay );
    
    BOOST_CHECK_THROW(push_pk(), fc::exception);
}

BOOST_AUTO_TEST_CASE(user_is_able_to_pay_per_month_more_than_max_limit_before_783_hardfork)
{
    const auto monthly = euro_token(10 * GRAPHENE_BLOCKCHAIN_PRECISION);

    generate_blocks(HARDFORK_346_TIME);
    generate_block();

    set_subscription_plan({{1, monthly.amount}});

    const auto to_pay = convert_to_core(db, monthly);
    subscribe_account_hf346(test_account, 1, to_pay);

    const auto payed = initial_balance - balance(test_account);
    BOOST_CHECK_EQUAL(payed, to_pay.amount.value);

    const auto from_database = get_account_expiration(test_account);
    const auto duration = from_database - now();

    using namespace std::chrono;
    const auto expected = microseconds{ month() };
    const auto actual = microseconds{duration.count()};
    BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_CASE(user_does_not_pay_per_month_more_than_max_limit_after_783_hardfork)
{
    auto monthly_limit = core_token(30 * GRAPHENE_BLOCKCHAIN_PRECISION);
    auto monthly = core_to_euro(2 * monthly_limit.amount);

    generate_block();

    set_subscription_plan({{1, monthly.amount}});

    generate_blocks(HARDFORK_783_TIME);

    set_max_monthly_subscription_price(monthly_limit.amount);

    auto to_pay = convert_to_core(db, monthly);
    subscribe_account_hf346(test_account, 1, to_pay);

    // delta mightly be slightly less than monthly fee due to timing or rounding issues
    auto delta = initial_balance - balance(test_account);
    BOOST_CHECK_EQUAL(delta, to_pay.amount.value );

    const auto from_database = get_account_expiration(test_account);
    const auto duration = from_database - now();

    using namespace std::chrono;

    const auto expected = microseconds{month() * to_pay.amount.value / monthly_limit.amount.value};
    const auto actual = microseconds{duration.count()};

    BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_CASE(user_is_able_to_pay_less_than_max_limit_after_783_hardfork)
{
    const auto monthly_limit = core_token(30 * GRAPHENE_BLOCKCHAIN_PRECISION);
    const auto monthly = core_to_euro(monthly_limit.amount / 2);

    generate_block();

    set_subscription_plan({{1, monthly.amount}});

    generate_blocks(HARDFORK_783_TIME);

    set_max_monthly_subscription_price(monthly_limit.amount);

    const auto to_pay = convert_to_core(db, monthly);
    subscribe_account_hf346(test_account, 1, to_pay);

    const auto payed = initial_balance - balance(test_account);
    // delta mightly be slightly less than twice monthly fee due to timing issues
    BOOST_CHECK_EQUAL(payed, to_pay.amount.value);

    const auto from_database = get_account_expiration(test_account);
    const auto duration = from_database - now();

    using namespace std::chrono;
    const auto expected = microseconds{ month() };
    const auto actual = microseconds{ duration.count() };
    BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_CASE(convert_subscription_plans_to_new_format_after_783_hardfork)
{
    generate_block();

    set_subscription_plan({{1, 100}, {2, 200}, {3, 400}});
    BOOST_CHECK(!db.get_global_properties().parameters.additional_parameters.value.subscription_packs.valid());

    generate_blocks(HARDFORK_783_TIME);

    BOOST_CHECK(!db.get_global_properties().parameters.additional_parameters.value.subscription_plan.valid());
    const additional_chain_parameters::subscription_packs_t expected = {{1, 100, 100, ""}, {2, 200, 200, ""}, {3, 400, 400, ""}};
    BOOST_CHECK(*db.get_global_properties().parameters.additional_parameters.value.subscription_packs == expected);
}

BOOST_AUTO_TEST_CASE(set_subscription_plan_after_hardfork_783)
{
    generate_blocks( HARDFORK_783_TIME );

    const additional_chain_parameters::subscription_packs_t expected = {{1, 3000, 1500, "plan one"}, {2, 4000, 1000, "plan two"}};
    set_subscription_plan_hf_783(expected);
    generate_block();

    BOOST_CHECK(!db.get_global_properties().parameters.additional_parameters.value.subscription_plan.valid());
    BOOST_CHECK(*db.get_global_properties().parameters.additional_parameters.value.subscription_packs == expected);
}

BOOST_AUTO_TEST_CASE(user_should_pay_at_least_min_subscription_cost_after_hf_783)
{
    generate_blocks(HARDFORK_783_TIME);

    const auto min_cost = euro_token(3000);
    const auto per_mounth = euro_token(1500);
    set_subscription_plan_hf_783({{1, min_cost.amount, per_mounth.amount}});

    {
        // pay less then min_cost
        const auto to_pay = convert_to_core(db, per_mounth);
        BOOST_CHECK_THROW(subscribe_account_hf346(test_account, 1, to_pay), fc::exception);
    }
    {
        // pay min_cost
        const auto to_pay = convert_to_core(db, min_cost);
        subscribe_account_hf346(test_account, 1, to_pay);
        BOOST_CHECK_EQUAL(initial_balance - to_pay.amount.value, balance(test_account));
    }
}

BOOST_AUTO_TEST_CASE(user_pay_more_then_min_subscription_cost_after_hf_783)
{
    generate_blocks(HARDFORK_783_TIME);

    const auto min_cost = euro_token(3000);
    const auto per_mounth = euro_token(1500);
    set_subscription_plan_hf_783({{1, min_cost.amount, per_mounth.amount}});

    const auto to_pay = convert_to_core(db, euro_token(per_mounth.amount.value * 3));
    subscribe_account_hf346(test_account, 1, to_pay);

    auto delta = initial_balance - balance(test_account);
    BOOST_CHECK_EQUAL(delta, to_pay.amount.value );

    auto from_database = get_account_expiration(test_account);
    auto duration = from_database - now();

    using namespace std::chrono;
    auto expected = microseconds{ month() * 3};
    auto actual = microseconds{duration.count()};
    BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_SUITE_END()

