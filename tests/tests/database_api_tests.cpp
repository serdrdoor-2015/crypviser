/*
 * Copyright (c) 2017 Cryptonomex, Inc., and contributors.
 *
 * The MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <boost/test/unit_test.hpp>

#include <graphene/app/database_api.hpp>

#include "../common/database_fixture.hpp"

using namespace graphene::chain;
using namespace graphene::chain::test;

struct database_api_fixture : database_fixture
{
  database_api_fixture()
    : api(db)
  {
  }

  graphene::app::database_api api;
};

BOOST_FIXTURE_TEST_SUITE(database_api_tests, database_api_fixture)

BOOST_AUTO_TEST_CASE(is_registered) 
{
  auto is_registered = [this](auto key) -> bool
  {
    public_key_type pk = key.get_public_key();
    return this->api.is_public_key_registered((string)pk);
  };

  auto bar = generate_private_key("bar");
  auto foo = generate_private_key("foo");
  auto unregistered = generate_private_key("unregistered");

  create_account("foo", foo.get_public_key());
  create_account("bar", bar.get_public_key());

  BOOST_CHECK(is_registered(bar));
  BOOST_CHECK(is_registered(foo));
  BOOST_CHECK(!is_registered(unregistered));
}

BOOST_AUTO_TEST_CASE(no_blocks_in_the_empty_database)
{
  // i suppose first block is the genesis
  BOOST_CHECK_EQUAL(1u, db.head_block_num());
}

BOOST_AUTO_TEST_CASE(generates_one_block_and_increases_head_block_num)
{
  auto key = generate_private_key("foo");
  create_account("foo", key.get_public_key());
  generate_block();

  BOOST_CHECK_EQUAL(2u, db.head_block_num());
}

BOOST_AUTO_TEST_CASE(fails_to_retrieve_a_non_esistent_block)
{
  BOOST_CHECK(!api.get_block(2));
}

BOOST_AUTO_TEST_CASE(retrieves_generated_block)
{
  auto key = generate_private_key("foo");
  create_account("foo", key.get_public_key());
  generate_block();

  BOOST_CHECK(!!api.get_block(2));
}

BOOST_AUTO_TEST_CASE(retrieves_transaction_from_block)
{
  auto key = generate_private_key("foo");
  create_account("foo", key.get_public_key());
  generate_block();

  auto tx = api.get_transaction(2, 0);
  const auto& op = tx.operations[0].get<account_create_operation>();

  BOOST_CHECK_EQUAL("foo", op.name);
}

BOOST_AUTO_TEST_CASE(fails_to_retrieve_transaction_merkle_proof)
{
  auto key = generate_private_key("foo");
  create_account("foo", key.get_public_key());
  generate_block();

  BOOST_CHECK(!api.prove_transaction(2, 123)); // invalid transaction number
  BOOST_CHECK(!api.prove_transaction(123, 0)); // invalid block number
}

BOOST_AUTO_TEST_CASE(verifies_transaction_using_merkle_path)
{
  auto key = generate_private_key("foo");
  create_account("foo", key.get_public_key());
  generate_block();

  auto block = *api.get_block(2);
  auto proof = *api.prove_transaction(2, 0);

  BOOST_CHECK(verify_merkle(proof.tx, proof.path, block.transaction_merkle_root));
}

BOOST_AUTO_TEST_CASE(retrieves_block_header)
{
  auto key = generate_private_key("foo");
  create_account("foo", key.get_public_key());
  generate_block();

  auto header = *api.get_block_header(2);
  auto block = *api.get_block(2);

  BOOST_CHECK(header == block_header(block));
}

BOOST_AUTO_TEST_CASE(proves_account_existance)
{
  auto key = generate_private_key("foo");
  create_account("foo", key.get_public_key());
  generate_block();

  auto proof = *api.prove_account("foo");
  auto account = *api.get_account_by_name("foo");
  auto block = *api.get_block(account.reference_transaction.block_num);

  BOOST_CHECK(verify_merkle(proof.tx, proof.path, block.transaction_merkle_root));
}

BOOST_AUTO_TEST_CASE(finds_block_containing_correct_transaction)
{
  auto generate_block_with_account = [&](string name)
  {
    auto key = generate_private_key(name);
    create_account(name, key.get_public_key());
    generate_block();
  };

  generate_block_with_account("foo");
  generate_block_with_account("bar");
  generate_block_with_account("baz");
  generate_block_with_account("cuz");

  auto proof = *api.prove_account("baz");
  BOOST_REQUIRE_EQUAL(1u, proof.tx.operations.size());

  auto& op = proof.tx.operations[0].get<account_create_operation>();
  BOOST_CHECK_EQUAL("baz", op.name);
}

BOOST_AUTO_TEST_CASE(generates_exactly_one_block)
{
  auto create_account_transaction = [&](string name)
  {
    auto key = generate_private_key(name);
    create_account(name, key.get_public_key());
  };

  create_account_transaction("foo");
  create_account_transaction("bar");
  create_account_transaction("baz");
  create_account_transaction("cuz");

  auto prev_block = db.head_block_num();
  generate_block();

  BOOST_CHECK_EQUAL(1u, db.head_block_num() - prev_block);
}

BOOST_AUTO_TEST_CASE(finds_correct_transaction_within_a_block)
{
  auto create_account_transaction = [&](string name)
  {
    auto key = generate_private_key(name);
    create_account(name, key.get_public_key());
  };

  create_account_transaction("foo");
  create_account_transaction("bar");
  create_account_transaction("baz");
  create_account_transaction("cuz");
  generate_block();

  auto proof = *api.prove_account("baz");
  BOOST_REQUIRE_EQUAL(1u, proof.tx.operations.size());

  auto& op = proof.tx.operations[0].get<account_create_operation>();
  BOOST_CHECK_EQUAL("baz", op.name);
}

BOOST_AUTO_TEST_CASE(verify_transaction_if_a_block_of_many)
{
  auto create_account_transaction = [&](string name)
  {
    auto key = generate_private_key(name);
    create_account(name, key.get_public_key());
  };

  create_account_transaction("foo");
  create_account_transaction("bar");
  create_account_transaction("baz");
  create_account_transaction("cuz");
  generate_block();

  auto proof = *api.prove_account("baz");
  auto block = *api.get_block(db.head_block_num());
  BOOST_CHECK(verify_merkle(proof.tx, proof.path, block.transaction_merkle_root));
}

BOOST_AUTO_TEST_SUITE_END()
