/*
 * Copyright (c) 2015 Cryptonomex, Inc., and contributors.
 *
 * The MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <boost/test/unit_test.hpp>

#include <graphene/chain/database.hpp>
#include <graphene/chain/exceptions.hpp>
#include <graphene/chain/hardfork.hpp>

#include <graphene/chain/account_object.hpp>
#include <graphene/chain/asset_object.hpp>
#include <graphene/chain/committee_member_object.hpp>
#include <graphene/chain/vesting_balance_object.hpp>
#include <graphene/chain/withdraw_permission_object.hpp>
#include <graphene/chain/witness_object.hpp>

#include <fc/crypto/digest.hpp>

#include "../common/database_fixture.hpp"

using namespace graphene::chain;
using namespace graphene::chain::test;

struct operation_tests_fixture : database_fixture
{
    auto create_account_key(std::string name, 
                            account_id_type registrar = account_id_type(),
                            uint64_t init_balance = 1e12)
    {
        auto key = fc::ecc::private_key::generate();
        const auto& registrar_obj = db.get(registrar);
        const auto& account = create_account(name, registrar_obj, key.get_public_key());

        uint64_t balance = 0;
        if (init_balance > 0)
        {
           auto funding = asset_id_type()(db).amount(init_balance);
           balance = fund(account, funding);
        }

        keys[name] = key;
        return std::make_tuple(key, account.id, balance);
    }

    auto create_account_key(std::string name, std::string registrar, uint64_t init_balance)
    {
       if (!registrar.empty())
       {
          auto registrar_account = get_account(registrar).id;
          return create_account_key(name, registrar_account, init_balance);
       }
       else
       {
          return create_account_key(name, account_id_type(), init_balance);
       }
    }
    
    void set_registrar_percent(uint16_t level, uint16_t percent)
    {
        db.modify(db.get_global_properties(), [level, percent](global_property_object& prop)
        {
            prop.parameters.referral_percent[level] = percent;
        });
    }    

    std::map<std::string, fc::ecc::private_key> keys;
};

BOOST_FIXTURE_TEST_SUITE( operation_tests, operation_tests_fixture )

BOOST_AUTO_TEST_CASE( feed_limit_logic_test )
{
   try {
      asset usd(1000,asset_id_type(1));
      asset core(1000,asset_id_type(0));
      price_feed feed;
      feed.settlement_price = usd / core;

      // require 3x min collateral
      auto swanp = usd / core;
      auto callp = ~price::call_price( usd, core, 1750 );
      // 1:1 collateral
//      wdump((callp.to_real())(callp));
//      wdump((swanp.to_real())(swanp));
      FC_ASSERT( callp.to_real() > swanp.to_real() );

      /*
      wdump((feed.settlement_price.to_real()));
      wdump((feed.maintenance_price().to_real()));
      wdump((feed.max_short_squeeze_price().to_real()));

      BOOST_CHECK( usd * feed.settlement_price < usd * feed.maintenance_price() );
      BOOST_CHECK( usd * feed.maintenance_price() < usd * feed.max_short_squeeze_price() );
      */

   } catch (fc::exception& e) {
      edump((e.to_detail_string()));
      throw;
   }
}

BOOST_AUTO_TEST_CASE( create_account_test )
{
   try {
      trx.operations.push_back(make_account());
      account_create_operation op = trx.operations.back().get<account_create_operation>();

      REQUIRE_THROW_WITH_VALUE(op, registrar, account_id_type(9999999));
      REQUIRE_THROW_WITH_VALUE(op, fee, asset(-1));
      REQUIRE_THROW_WITH_VALUE(op, name, "!");
      REQUIRE_THROW_WITH_VALUE(op, name, "Sam");
      REQUIRE_THROW_WITH_VALUE(op, name, "saM");
      REQUIRE_THROW_WITH_VALUE(op, name, "sAm");
      REQUIRE_THROW_WITH_VALUE(op, name, "6j");
      REQUIRE_THROW_WITH_VALUE(op, name, "j-");
      REQUIRE_THROW_WITH_VALUE(op, name, "-j");

      REQUIRE_THROW_WITH_VALUE(op, name, 
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

      REQUIRE_THROW_WITH_VALUE(op, name, "aaaa.");
      REQUIRE_THROW_WITH_VALUE(op, name, ".aaaa");
      REQUIRE_THROW_WITH_VALUE(op, options.voting_account, account_id_type(999999999));

      auto auth_bak = op.owner;
      op.owner.add_authority(account_id_type(9999999999), 10);
      trx.operations.back() = op;
      op.owner = auth_bak;
      GRAPHENE_REQUIRE_THROW(PUSH_TX( db, trx, ~0 ), fc::exception);
      op.owner = auth_bak;

      trx.operations.back() = op;
      sign( trx,  init_account_priv_key );
      trx.validate();
      PUSH_TX( db, trx, ~0 );

      const account_object& nathan_account = *db.get_index_type<account_index>().indices().get<by_name>().find("nathan");
      BOOST_CHECK(nathan_account.id.space() == protocol_ids);
      BOOST_CHECK(nathan_account.id.type() == account_object_type);
      BOOST_CHECK(nathan_account.name == "nathan");

      BOOST_REQUIRE(nathan_account.owner.num_auths() == 1);
      BOOST_CHECK(nathan_account.owner.key_auths.at(committee_key) == 123);
      BOOST_REQUIRE(nathan_account.active.num_auths() == 1);
      BOOST_CHECK(nathan_account.active.key_auths.at(committee_key) == 321);
      BOOST_CHECK(nathan_account.options.voting_account == GRAPHENE_PROXY_TO_SELF_ACCOUNT);
      BOOST_CHECK(nathan_account.options.memo_key == committee_key);

      const account_statistics_object& statistics = nathan_account.statistics(db);
      BOOST_CHECK(statistics.id.space() == implementation_ids);
      BOOST_CHECK(statistics.id.type() == impl_account_statistics_object_type);
   } catch (fc::exception& e) {
      edump((e.to_detail_string()));
      throw;
   }
}

BOOST_AUTO_TEST_CASE( update_account )
{
   try {
      const account_object& nathan = create_account("nathan", init_account_pub_key);
      const fc::ecc::private_key nathan_new_key = fc::ecc::private_key::generate();
      const public_key_type key_id = nathan_new_key.get_public_key();
      const auto& active_committee_members = db.get_global_properties().active_committee_members;

      transfer(account_id_type()(db), nathan, asset(1000000000));

      trx.operations.clear();
      account_update_operation op;
      op.account = nathan.id;
      op.owner = authority(2, key_id, 1, init_account_pub_key, 1);
      op.active = authority(2, key_id, 1, init_account_pub_key, 1);
      op.new_options = nathan.options;
      op.new_options->votes = flat_set<vote_id_type>({active_committee_members[0](db).vote_id, active_committee_members[5](db).vote_id});
      op.new_options->num_committee = 2;
      trx.operations.push_back(op);
      BOOST_TEST_MESSAGE( "Updating account" );
      PUSH_TX( db, trx, ~0 );

      BOOST_CHECK(nathan.options.memo_key == init_account_pub_key);
      BOOST_CHECK(nathan.active.weight_threshold == 2);
      BOOST_CHECK(nathan.active.num_auths() == 2);
      BOOST_CHECK(nathan.active.key_auths.at(key_id) == 1);
      BOOST_CHECK(nathan.active.key_auths.at(init_account_pub_key) == 1);
      BOOST_CHECK(nathan.owner.weight_threshold == 2);
      BOOST_CHECK(nathan.owner.num_auths() == 2);
      BOOST_CHECK(nathan.owner.key_auths.at(key_id) == 1);
      BOOST_CHECK(nathan.owner.key_auths.at(init_account_pub_key) == 1);
      BOOST_CHECK(nathan.options.votes.size() == 2);

   } catch (fc::exception& e) {
      edump((e.to_detail_string()));
      throw;
   }
}

BOOST_AUTO_TEST_CASE( transfer_core_asset )
{
   try {
      INVOKE(create_account_test);

      account_id_type committee_account;
      asset committee_balance = db.get_balance(account_id_type(), asset_id_type());

      const account_object& nathan_account = *db.get_index_type<account_index>().indices().get<by_name>().find("nathan");
      transfer_operation top;
      top.from = committee_account;
      top.to = nathan_account.id;
      top.amount = asset( 10000);
      trx.operations.push_back(top);
      for( auto& op : trx.operations ) db.current_fee_schedule().set_fee(op);

      asset fee = trx.operations.front().get<transfer_operation>().fee;
      trx.validate();
      PUSH_TX( db, trx, ~0 );

      BOOST_CHECK_EQUAL(get_balance(account_id_type()(db), asset_id_type()(db)),
                        (committee_balance.amount - 10000 - fee.amount).value);
      committee_balance = db.get_balance(account_id_type(), asset_id_type());

      BOOST_CHECK_EQUAL(get_balance(nathan_account, asset_id_type()(db)), 10000);

      trx = signed_transaction();
      top.from = nathan_account.id;
      top.to = committee_account;
      top.amount = asset(2000);
      trx.operations.push_back(top);

      for( auto& op : trx.operations ) db.current_fee_schedule().set_fee(op);

      fee = trx.operations.front().get<transfer_operation>().fee;
      set_expiration( db, trx );
      trx.validate();
      PUSH_TX( db, trx, ~0 );

      BOOST_CHECK_EQUAL(get_balance(nathan_account, asset_id_type()(db)), 8000 - fee.amount.value);
      BOOST_CHECK_EQUAL(get_balance(account_id_type()(db), asset_id_type()(db)), committee_balance.amount.value + 2000);

   } catch (fc::exception& e) {
      edump((e.to_detail_string()));
      throw;
   }
}

BOOST_AUTO_TEST_CASE( create_committee_member )
{
   try {
      committee_member_create_operation op;
      op.committee_member_account = account_id_type();
      op.fee = asset();
      trx.operations.push_back(op);

      REQUIRE_THROW_WITH_VALUE(op, committee_member_account, account_id_type(99999999));
      REQUIRE_THROW_WITH_VALUE(op, fee, asset(-600));
      trx.operations.back() = op;

      committee_member_id_type committee_member_id = db.get_index_type<committee_member_index>().get_next_id();
      PUSH_TX( db, trx, ~0 );
      const committee_member_object& d = committee_member_id(db);

      BOOST_CHECK(d.committee_member_account == account_id_type());
   } catch (fc::exception& e) {
      edump((e.to_detail_string()));
      throw;
   }
}


BOOST_AUTO_TEST_CASE( witness_pay_test )
{ try {
   const share_type prec = asset::scaled_precision( asset_id_type()(db).precision );
   set_registrar_percent(0, 0); // 100% of fees go to reserve fund

   // there is an immediate maintenance interval in the first block
   //   which will initialize last_budget_time
   generate_block();

   const auto* core = &asset_id_type()(db);

   create_account_key("registrar");
   create_account_key("nathan", "registrar", 0u);

   transfer(account_id_type()(db), get_account("nathan"), asset(20000*prec));
   transfer(account_id_type()(db), get_account("init3"), asset(20*prec));

   generate_block();

   auto last_witness_vbo_balance = [&]() -> share_type
   {
      const witness_object& wit = db.fetch_block_by_number(db.head_block_num())->witness(db);
      if( !wit.pay_vb.valid() )
      {
         return 0;
      }
      return (*wit.pay_vb)(db).balance.amount;
   };

   const auto block_interval = db.get_global_properties().parameters.block_interval;
   const account_object* nathan = &get_account("nathan");
   enable_fees();

   auto operation_fee = db.current_fee_schedule().get<account_create_operation>().basic_fee;
   BOOST_CHECK_GT(operation_fee, 0u);

   // Based on the size of the reserve fund later in the test, the witness budget will be set to this value
   const uint64_t ref_budget =
      ((uint64_t(operation_fee)
         * GRAPHENE_CORE_ASSET_CYCLE_RATE * 30
         * block_interval
       ) + ((uint64_t(1) << GRAPHENE_CORE_ASSET_CYCLE_RATE_BITS)-1)
      ) >> GRAPHENE_CORE_ASSET_CYCLE_RATE_BITS
      ;

   // change this if ref_budget changes
   BOOST_CHECK_EQUAL( ref_budget, 119u );

   const uint64_t witness_ppb = ref_budget * 10 / 23 + 1;
   // change this if ref_budget changes
   BOOST_CHECK_EQUAL( witness_ppb, 52u );

   // following two inequalities need to hold for maximal code coverage
   BOOST_CHECK_LT( witness_ppb * 2, ref_budget );
   BOOST_CHECK_GT( witness_ppb * 3, ref_budget );

   db.modify( db.get_global_properties(), [&]( global_property_object& _gpo )
   {
      _gpo.parameters.witness_pay_per_block = witness_ppb;
   } );

   BOOST_CHECK_EQUAL(core->dynamic_asset_data_id(db).accumulated_fees.value, 0);
   BOOST_TEST_MESSAGE( "Creating account" );

   account_create_operation create_account;
   const fc::ecc::private_key anton_sk = fc::ecc::private_key::generate();
   const public_key_type anton_pk = anton_sk.get_public_key();
   create_account.registrar = nathan->id;
   create_account.name = "anton";
   create_account.owner = authority(2, anton_pk, 1, init_account_pub_key, 1);
   create_account.active = authority(2, anton_pk, 1, init_account_pub_key, 1);
   create_account.options.memo_key = anton_pk;
   create_account.options.voting_account = GRAPHENE_PROXY_TO_SELF_ACCOUNT;
   auto create_account_fee = db.current_fee_schedule().calculate_fee( create_account );
   create_account.fee = create_account_fee;

   set_expiration(db, trx);
   trx.operations.push_back(create_account);
   for( auto& op : trx.operations ) db.current_fee_schedule().set_fee(op);
   trx.validate();
   sign( trx, keys["nathan"] );

   auto prev_balance = get_balance(*nathan, *core);

   BOOST_CHECK_EQUAL( core->reserved(db).value, 0); 

   PUSH_TX( db, trx );
   auto pay_fee_time = db.head_block_time().sec_since_epoch();
   trx.clear();

   // account_create_operation fills the reserve fund
   BOOST_CHECK_EQUAL( core->reserved(db).value, 200020605); 

   // ensure than nathan pays account registration fee
   auto cur_balance = get_balance(*nathan, *core);
   BOOST_CHECK(prev_balance > cur_balance);
   BOOST_CHECK( get_balance(*nathan, *core) == 20000*prec - create_account_fee.amount );

   generate_block();
   nathan = &get_account("nathan");
   core = &asset_id_type()(db);
   BOOST_CHECK_EQUAL( last_witness_vbo_balance().value, 0 );

   auto schedule_maint = [&]()
   {
      // now we do maintenance
      db.modify( db.get_dynamic_global_properties(), [&]( dynamic_global_property_object& _dpo )
      {
         _dpo.next_maintenance_time = db.head_block_time() + 1;
      } );
   };
   BOOST_TEST_MESSAGE( "Generating some blocks" );

   // generate some blocks
   while( db.head_block_time().sec_since_epoch() - pay_fee_time < 24 * block_interval )
   {
      generate_block();
      BOOST_CHECK_EQUAL( last_witness_vbo_balance().value, 0 );
   }
   BOOST_CHECK_EQUAL( db.head_block_time().sec_since_epoch() - pay_fee_time, 24u * block_interval );

   BOOST_CHECK_EQUAL( uint64_t(db.get_dynamic_global_properties().witness_budget.value), 0u );

   schedule_maint();
   generate_block();

   // the reserve fund gets spent during maintenance interval to reward witnesses
   // initially the reselve fund value was 40004121
   BOOST_CHECK_EQUAL( core->reserved(db).value, 200020486);  

   BOOST_CHECK_EQUAL( uint64_t(db.get_dynamic_global_properties().witness_budget.value), ref_budget );

   // first witness paid from old budget (so no pay)
   BOOST_CHECK_EQUAL( last_witness_vbo_balance().value, 0 );
   // second witness finally gets paid!
   generate_block();
   BOOST_CHECK_EQUAL( uint64_t(last_witness_vbo_balance().value), witness_ppb );
   BOOST_CHECK_EQUAL( uint64_t(db.get_dynamic_global_properties().witness_budget.value), ref_budget - witness_ppb );

   generate_block();
   BOOST_CHECK_EQUAL( uint64_t(last_witness_vbo_balance().value), witness_ppb );
   BOOST_CHECK_EQUAL( uint64_t(db.get_dynamic_global_properties().witness_budget.value), ref_budget - 2 * witness_ppb );

   generate_block();
   BOOST_CHECK_LT( uint64_t(last_witness_vbo_balance().value), witness_ppb );
   BOOST_CHECK_EQUAL( uint64_t(last_witness_vbo_balance().value), ref_budget - 2 * witness_ppb );
   BOOST_CHECK_EQUAL( db.get_dynamic_global_properties().witness_budget.value, 0u );

   generate_block();
   BOOST_CHECK_EQUAL( last_witness_vbo_balance().value, 0 );
   BOOST_CHECK_EQUAL( db.get_dynamic_global_properties().witness_budget.value, 0u );

   // the reserve fund does not change because there was no maintenance interval
   // and we didn't pay the witnesses
   BOOST_CHECK_EQUAL(core->reserved(db).value, 200020486 );

} FC_LOG_AND_RETHROW() }


BOOST_AUTO_TEST_CASE( reserve_asset_test )
{ try {
   ACTORS( (alice) );
   const auto& core_asset = asset_id_type()(db);

   auto reserve_asset = [&]( account_id_type payer, asset amount_to_reserve )
   {
       asset_reserve_operation op;
       op.payer = payer;
       op.amount_to_reserve = amount_to_reserve;
       signed_transaction tx;
       tx.operations.push_back( op );
       set_expiration( db, tx );
       tx.validate();
       db.push_transaction( tx, ~0 );
   } ;

   int64_t alice_init_balance = 10000;
   int64_t reserve_amount = 3000;
   share_type initial_reserve;

   generate_blocks(HARDFORK_783_TIME);
   generate_block();

   BOOST_TEST_MESSAGE( "Test reserve operation on core asset" );
   transfer( committee_account, alice_id, core_asset.amount( alice_init_balance ) );

   initial_reserve = core_asset.reserved( db );
   reserve_asset( alice_id, core_asset.amount( reserve_amount  ) );
   BOOST_CHECK_EQUAL( get_balance( alice, core_asset ), alice_init_balance - reserve_amount );
   BOOST_CHECK_EQUAL( (core_asset.reserved( db ) - initial_reserve).value, reserve_amount );
   verify_asset_supplies(db);

} FC_LOG_AND_RETHROW() }

// TODO:  Write linear VBO tests

BOOST_AUTO_TEST_SUITE_END()
