#include <boost/test/unit_test.hpp>

#include <graphene/chain/database.hpp>
#include <graphene/chain/exceptions.hpp>
#include <graphene/chain/hardfork.hpp>

#include "../common/database_fixture.hpp"

using namespace graphene::chain;
using namespace graphene::chain::test;

struct transfer_operation_fixture : database_fixture
{
    template <typename Operation>
    void push_operation(Operation op)
    {
        push_signed_operation(op, test_key);
    }

    template <typename Operation>
    void push_signed_operation(Operation op, fc::ecc::private_key key)
    {
        trx.clear();
        trx.operations.push_back(op);
        sign(trx, key);
        trx.validate();
        db.push_transaction(trx);
    }

    template <typename Operation>
    void push_unsigned_operation(Operation op)
    {
        trx.clear();
        trx.operations.push_back(op);
        trx.validate();
        db.push_transaction(trx);
    }

    transfer_operation_fixture()
    {
        test_key= fc::ecc::private_key::generate();

        const auto& account = create_account("test", test_key.get_public_key());
        test_account = account.id;
        
        initial_balance = fund(account);
    }

    const auto& core_token()
    {
        return asset_id_type()(db);
    }

    uint64_t balance()
    {
        return get_balance(db.get(test_account), core_token());
    }

    fc::ecc::private_key test_key;
    account_id_type test_account;
    account_id_type null_account;
    uint64_t initial_balance;
};

BOOST_FIXTURE_TEST_SUITE(transfer_operation_tests, transfer_operation_fixture) 

BOOST_AUTO_TEST_CASE(transfers_tokens)
{
    transfer_operation op;
    op.from = test_account;
    op.to = null_account;
    op.amount = core_token().amount(500);

    push_operation(op);

    BOOST_CHECK_EQUAL(500u, initial_balance - balance());
}

BOOST_AUTO_TEST_CASE(unable_to_transfer_tokens_if_transaction_is_not_signed)
{
    transfer_operation op;
    op.from = test_account;
    op.to = null_account;
    op.amount = core_token().amount(500);

    BOOST_CHECK_THROW(push_unsigned_operation(op), fc::exception);
}

BOOST_AUTO_TEST_CASE(unable_to_transfer_if_signed_with_wrong_key)
{
    transfer_operation op;
    op.from = test_account;
    op.to = null_account;
    op.amount = core_token().amount(500);

    auto wrong_key = fc::ecc::private_key::generate();

    BOOST_CHECK_THROW(push_signed_operation(op, wrong_key), fc::exception);
}

BOOST_AUTO_TEST_SUITE_END()
