#include <boost/test/unit_test.hpp>
#include <boost/core/ignore_unused.hpp>
#include <chrono>

#include <graphene/chain/database.hpp>
#include <graphene/app/database_api.hpp>
#include <graphene/chain/exceptions.hpp>
#include <graphene/chain/hardfork.hpp>

#include <fc/crypto/pke.hpp>

#include "../common/database_fixture.hpp"

using namespace graphene::chain;
using namespace graphene::chain::test;

struct chain_additional_parameters_fixture : database_fixture
{
   void set_chain_additional_parameters(const committee_member_update_global_parameters_operation& op)
   {
      db.modify(db.get_global_properties(), [](global_property_object& p) {
         p.parameters.committee_proposal_review_period = fc::hours(1).to_seconds();
      });

      BOOST_TEST_MESSAGE( "Creating a proposal to change the additional parameters" );
      {
         proposal_create_operation cop = proposal_create_operation::committee_proposal(
               db.get_global_properties().parameters, db.head_block_time());

         cop.fee_paying_account = GRAPHENE_TEMP_ACCOUNT;
         cop.expiration_time = db.head_block_time() + *cop.review_period_seconds + 10;
         cop.proposed_ops.emplace_back(op);
         
         set_expiration(db, trx);
         trx.operations.push_back(cop);
         db.push_transaction(trx);
      }

      BOOST_TEST_MESSAGE( "Updating proposal by signing with the committee_member private key" );
      {
         proposal_update_operation uop;
         uop.fee_paying_account = GRAPHENE_TEMP_ACCOUNT;
         uop.active_approvals_to_add = {get_account("init0").get_id(), get_account("init1").get_id(),
                                       get_account("init2").get_id(), get_account("init3").get_id(),
                                       get_account("init4").get_id(), get_account("init5").get_id(),
                                       get_account("init6").get_id(), get_account("init7").get_id()};
         trx.clear();
         trx.operations.push_back(uop);
         sign( trx, init_account_priv_key );
         db.push_transaction(trx);
         BOOST_CHECK(proposal_id_type()(db).is_authorized_to_execute(db));
      }
   }
};

BOOST_AUTO_TEST_SUITE(chain_additional_parameters_tests)

BOOST_FIXTURE_TEST_CASE( change_parameters, chain_additional_parameters_fixture )
{ try {

   if (db.head_block_time() < HARDFORK_346_TIME)
      generate_blocks(HARDFORK_346_TIME);

   generate_block();

   auto initialValue = db.get_global_properties().parameters.additional_parameters.value.subscription_plan;
   const additional_chain_parameters::subscription_plan_t expectedValue = { {1, 1}, {2,2}, {3,3} };

   committee_member_update_global_parameters_operation op;
   op.new_parameters.additional_parameters.value.subscription_plan = expectedValue;
   set_chain_additional_parameters(op);

   BOOST_TEST_MESSAGE( "Verifying that the additional parameters didn't change immediately" );

   BOOST_CHECK(db.get_global_properties().parameters.additional_parameters.value.subscription_plan == initialValue);

   BOOST_TEST_MESSAGE( "Generating blocks until proposal expires" );
   generate_blocks(proposal_id_type()(db).expiration_time + 5);
   BOOST_TEST_MESSAGE( "Verify that the additional properties is still were not changed" );
   BOOST_CHECK(db.get_global_properties().parameters.additional_parameters.value.subscription_plan == initialValue);

   BOOST_TEST_MESSAGE( "Generating blocks until next maintenance interval" );
   generate_blocks(db.get_dynamic_global_properties().next_maintenance_time);
   generate_block();   // get the maintenance skip slots out of the way

   BOOST_TEST_MESSAGE( "Verify that the additional parameters were changed" );
   BOOST_CHECK(db.get_global_properties().parameters.additional_parameters.value.subscription_plan == expectedValue);

} FC_LOG_AND_RETHROW() }


BOOST_FIXTURE_TEST_CASE( change_parameters_before_hardfork_346, chain_additional_parameters_fixture )
{ 
      if (db.head_block_time() >= HARDFORK_346_TIME)
      {
         BOOST_TEST_MESSAGE( "Obsolete test case. It makes sense to run it before HARDFORK_346_TIME" );
         return;
      }

      generate_block();

      db.modify(db.get_global_properties(), [](global_property_object& p) {
         p.parameters.committee_proposal_review_period = fc::hours(1).to_seconds();
      });

      const additional_chain_parameters::subscription_plan_t expectedValue = { {1, 1}, {2,2}, {3,3} };

      BOOST_TEST_MESSAGE( "Creating a proposal to change the additional parameters before HARDFORK_346_TIME" );
      proposal_create_operation cop = proposal_create_operation::committee_proposal(
            db.get_global_properties().parameters, db.head_block_time());

      cop.fee_paying_account = GRAPHENE_TEMP_ACCOUNT;
      cop.expiration_time = db.head_block_time() + *cop.review_period_seconds + 10;
      committee_member_update_global_parameters_operation uop;
      uop.new_parameters.additional_parameters.value.subscription_plan = expectedValue;
      cop.proposed_ops.emplace_back(uop);

      trx.operations.push_back(cop);
      db.push_transaction(trx);
   
      BOOST_CHECK_THROW (db.push_proposal(proposal_id_type()(db)), fc::assert_exception);
}

BOOST_FIXTURE_TEST_CASE( change_parameters_negative, chain_additional_parameters_fixture )
{
   BOOST_TEST_MESSAGE( "Creating a proposal to change the additional parameters with wrong values" );
   const std::vector<additional_chain_parameters::subscription_plan_t> wrongValues = { { {0, 1} }, // ID should be > 0
    {{-1, 1}}, // ID should be > 0
    {{1, 0}}, // fee should be > 0
    {{1, -1}} // fee should be > 0
     };
   
      proposal_create_operation cop = proposal_create_operation::committee_proposal(
            db.get_global_properties().parameters, db.head_block_time());

      committee_member_update_global_parameters_operation uop;
      cop.fee_paying_account = GRAPHENE_TEMP_ACCOUNT;
      cop.expiration_time = db.head_block_time() + *cop.review_period_seconds + 10;
   
   for (const auto& value : wrongValues)
   {
      uop.new_parameters.additional_parameters.value.subscription_plan = value;
      cop.proposed_ops.emplace_back(uop);
      trx.operations.push_back(cop);
      BOOST_CHECK_THROW (db.push_transaction(trx), fc::assert_exception);
   }
}

BOOST_FIXTURE_TEST_CASE( change_subscription_plans_new_format_validation_test, chain_additional_parameters_fixture )
{
    BOOST_TEST_MESSAGE( "Creating a proposal to change the additional parameters with wrong values" );
    const std::vector<additional_chain_parameters::subscription_packs_t> wrongValues = { {{0, 1, 1}}, // ID should be > 0
        {{-1, 1, 1}}, // ID should be > 0
        {{1, 0, 1}},  // min cost should be > 0
        {{1, -1, 1}}, // min cost should be > 0
        {{1, 1, 0}},  // price per month > 0
        {{1, 1, -1}}  // price per month > 0
    };

    proposal_create_operation cop = proposal_create_operation::committee_proposal(
        db.get_global_properties().parameters, db.head_block_time());

    committee_member_update_global_parameters_operation uop;
    cop.fee_paying_account = GRAPHENE_TEMP_ACCOUNT;
    cop.expiration_time = db.head_block_time() + *cop.review_period_seconds + 10;

   for (const auto& value : wrongValues)
   {
        uop.new_parameters.additional_parameters.value.subscription_packs = value;
        cop.proposed_ops.emplace_back(uop);
        trx.operations.push_back(cop);
        BOOST_CHECK_THROW (db.push_transaction(trx), fc::assert_exception);
   }
}

BOOST_FIXTURE_TEST_CASE( set_new_subscription_plans_before_hardfork_346, chain_additional_parameters_fixture )
{
   generate_block();

   additional_chain_parameters::subscription_packs_t packs = { {1, 1, 1} };
   committee_member_update_global_parameters_operation op;
   op.new_parameters.additional_parameters.value.subscription_packs = packs;

   set_chain_additional_parameters(op);

   BOOST_TEST_MESSAGE( "Generating blocks until proposal expires" );
   generate_blocks(proposal_id_type()(db).expiration_time + 5);

   generate_blocks(db.get_dynamic_global_properties().next_maintenance_time);
   generate_block();
   BOOST_CHECK(!db.get_global_properties().parameters.additional_parameters.value.subscription_packs.valid());
}

BOOST_FIXTURE_TEST_CASE( set_new_subscription_plans_after_346_before_783_hardfork, chain_additional_parameters_fixture )
{
   generate_blocks(HARDFORK_346_TIME);
   generate_block();

   additional_chain_parameters::subscription_packs_t packs = { {1, 1, 1} };
   committee_member_update_global_parameters_operation op;
   op.new_parameters.additional_parameters.value.subscription_packs = packs;

   set_chain_additional_parameters(op);

   BOOST_TEST_MESSAGE( "Generating blocks until proposal expires" );
   generate_blocks(proposal_id_type()(db).expiration_time + 5);

   generate_blocks(db.get_dynamic_global_properties().next_maintenance_time);
   generate_block();
   BOOST_CHECK(!db.get_global_properties().parameters.additional_parameters.value.subscription_packs.valid());
}

BOOST_FIXTURE_TEST_CASE( set_new_subscription_plans_after_783, chain_additional_parameters_fixture )
{
   generate_blocks(HARDFORK_783_TIME);

   additional_chain_parameters::subscription_packs_t packs = { {1, 1, 1} };
   committee_member_update_global_parameters_operation op;
   op.new_parameters = db.get_global_properties().parameters;
   op.new_parameters.additional_parameters.value.subscription_packs = packs;

   set_chain_additional_parameters(op);

   BOOST_TEST_MESSAGE( "Generating blocks until proposal expires" );
   generate_blocks(proposal_id_type()(db).expiration_time + 5);

   generate_blocks(db.get_dynamic_global_properties().next_maintenance_time);
   generate_block();
   BOOST_CHECK(db.get_global_properties().parameters.additional_parameters.value.subscription_packs.valid());
}

BOOST_FIXTURE_TEST_CASE( set_new_max_monthly_subscription_price_after_783, chain_additional_parameters_fixture )
{
   generate_blocks(HARDFORK_783_TIME);

   const auto price = 21 * GRAPHENE_BLOCKCHAIN_PRECISION;

   additional_chain_parameters::subscription_packs_t packs = { {1, 1, 1} };
   committee_member_update_global_parameters_operation op;
   op.new_parameters = db.get_global_properties().parameters;
   op.new_parameters.additional_parameters.value.subscription_packs = packs;
   op.new_parameters.additional_parameters.value.max_monthly_subscription_price = price;

   set_chain_additional_parameters(op);

   BOOST_TEST_MESSAGE( "Generating blocks until proposal expires" );
   generate_blocks(proposal_id_type()(db).expiration_time + 5);

   generate_blocks(db.get_dynamic_global_properties().next_maintenance_time);
   generate_block();

   const auto new_additional_parameters = db.get_global_properties().parameters.additional_parameters;

   BOOST_CHECK(new_additional_parameters.value.max_monthly_subscription_price.valid());
   BOOST_CHECK(new_additional_parameters.value.max_monthly_subscription_price == price);
}

BOOST_AUTO_TEST_SUITE_END()
